/*
 *  Copyright (C) 2009 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This schema was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "visualSonar.h"

int visualSonar_brothers[MAX_SCHEMAS];
arbitration visualSonar_callforarbitration;

int DEBUG = 1;
int debug = 1;
// Control estático de los movimientos de traslación/rotación del robot
int moveAndFlash = 0;
int traslacion = 0, rotacion = 0;
int flashImage = TRUE;

// Variables para guardar las matrices K, R y T de nuestra cámara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* para ver el entorno circundante al robot */
TPinHoleCamera roboticLabCam0; // cámaras del techo del laboratorio
TPinHoleCamera roboticLabCam1; //
TPinHoleCamera roboticLabCam2; //
TPinHoleCamera roboticLabCam3; //
TPinHoleCamera ceilLabCam;
TPinHoleCamera robotCamera;
TPinHoleCamera robotCamera2;
TPinHoleCamera *myCamera; // puntero temporal
HPoint2D pixel_camA;
HPoint3D myfloor[18*2]; /* mm */
int myfloor_lines=0;

HPoint2D actualPosition;
float coveredDistance;

static SofReference mypioneer;

int actualCameraView; // identificador de la cámara de la escena actual

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

char *configFile;

HPoint2D myActualPoint2D; // variables para el cálculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myStartPoint2D;
HPoint2D myEndPoint2D;
HPoint3D myStartPoint3D;
HPoint3D myEndPoint3D;
HPoint3D cameraPos3D;
HPoint2D myPoint2D;
HPoint3D myPoint3D;
HPoint3D intersectionPoint;
HPoint3D intersectionStartPoint;
HPoint3D intersectionEndPoint;

Segment2D groundSegments2D [MAX_LINES_TO_DETECT];
Segment3D groundSegments3D [MAX_LINES_IN_MEMORY];
SemiArrow3D semiArrows3D [MAX_LINES_IN_MEMORY/2];
Parallelogram3D groundParallelograms3D [MAX_PARALLELOGRAMS_IN_MEMORY];
int numSegments, incNumSegments, numParallelograms, incNumParallelograms, numFaces, incNumFaces;

int primerPunto = FALSE, segundoPunto = FALSE, tercerPunto = FALSE, cuartoPunto = FALSE;
float distanciaPlanoImagen, incremento;
int numIterations = 0;

HPoint2D mouse_on_minicanvas, mouse_on_visualSonarcanvas;

pthread_mutex_t main_mutex;
/*GTK variables*/
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkWidget *visualSonarCanvas, *floorCanvas;
GtkWidget *miniCanvas;

GtkImage *originalImage; // Widgets de las imágenes que se obtienen del Window1 (GTK)
GtkImage *filteredImage;
GtkImage *groundedImage;
GtkImage *borderImage;

float sliderPANTILT_BASE_HEIGHT, sliderISIGHT_OPTICAL_CENTER, sliderTILT_HEIGHT, sliderCAMERA_TILT_HEIGHT, sliderPANTILT_BASE_X, sliderThreshold;

struct image_struct *imageA = NULL; // Imágenes en crudo
struct image_struct *imageAfiltered = NULL;
struct image_struct *imageAgrounded = NULL;
struct image_struct *imageAbordered = NULL;

struct image_struct *originalImageRGB = NULL; // Imágenes preparadas para ser pintadas (cambio de bits)
struct image_struct *filteredImageRGB = NULL;
struct image_struct *groundedImageRGB = NULL;
struct image_struct *borderImageRGB = NULL;

/*Some control variable*/
static int vmode;
static XImage *imagenA, *imagenFiltrada, *imagenSuelo;
static char *imagenA_buf, *imagenFiltrada_buf, *imagenSuelo_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */
char *image;
char *contourImage;
static long int tabla[256]; 
/* tabla con la traduccion de niveles de gris a numero de pixel en Pseudocolor-8bpp. Cada numero de pixel apunta al valor adecuado del ColorMap, con el color adecuado instalado */
static int pixel8bpp_rojo, pixel8bpp_blanco, pixel8bpp_amarillo;

int floorView = FALSE, worldView = TRUE, whatWorld = 0;

int visualSonar_id=0; 
int visualSonar_brothers[MAX_SCHEMAS];
arbitration visualSonar_callforarbitration;
int visualSonar_cycle=1000; /* ms */

char **myActualCamera;

int *mycolorAwidth		= NULL;
int *mycolorAheight		= NULL;
char **mycolorA;
float *myencoders=NULL;
float *myv=NULL;
float *myw=NULL;
runFn colorArun, encodersrun, motorsrun;
stopFn colorAstop, encodersstop, motorsstop;
float *mypan_angle=NULL, *mytilt_angle=NULL;  /* degs */
float *mylongitude=NULL; /* degs, pan angle */
float *mylatitude=NULL; /* degs, tilt angle */
float *mylongitude_speed=NULL;
float *mylatitude_speed=NULL;
float *max_pan=NULL;
float *min_pan=NULL;
float *max_tilt=NULL;
float *min_tilt=NULL;
runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;

int botonPulsado;

float old_x=0., old_y=0.;
float longi_foa = 0.0;
float lati_foa = 0.0;
float foax = 0.0;
float foay = 0.0;
float foaz = 0.0;
float xcam = -1.0;
float ycam = -1.0;
float zcam = -1.0;
float lati = 0.0;
float longi = 0.0;
int foa_mode, cam_mode;
float radius = 500;
float radius_old;
int centrado = 0;
float t = 0.5; // lambda
float phi = 0., theta = 0.;
int boton_pulsado;
float tiltAngle, panAngle;
float speed_y, speed_x;
int completedMovement;

int pantiltStill;
int robotStill;
int comeFromCenter;
int last_movement;
int last_full_movement;

int numFlashes;

double actualInstant;
double stopInstant;

// Variables relativas a la atención visual
static CvMemStorage* storage = 0;
static CvMemStorage* storageTmp = 0;
static CvMemStorage* storageAux = 0;
static CvHaarClassifierCascade* cascade = 0;
const char* cascade_name = "haarcascade_frontalface_alt.xml";
IplImage *imgLocal = NULL;
CvMat img; // Envoltura a mycolorA
CvSeq *facesTmp, *facesAux;
HPoint2D faceCenter;
double systemInstant;
double timeForced;
double timeToForcedSearch;
double startToFollowTime, actualFollowTime;
int completedSearch;
int analizedImage;
int completedTrack;
int isForcedSearch;
int myActualState;
struct elementStruct* myElements; // apuntará al inicio de la lista de elementos
struct elementStruct* myMaxSaliency; // apuntará al elemento de mayor saliencia
struct elementStruct *myActualElement;
int elementsStructCounter;
int randomPosition;
int nextLatitude;
int nextLongitude;
Face3D groundFaces3D [MAX_FACES_IN_MEMORY];

void printCameraInformation (TPinHoleCamera* actualCamera) {
	printf ("CAMERA INFORMATION\n");
	printf ("==================\n");
	printf ("Position = [%f, %f, %f]\n", actualCamera->position.X, actualCamera->position.Y, actualCamera->position.Z);
	printf ("FOA = [%f, %f, %f]\n", actualCamera->foa.X, actualCamera->foa.Y, actualCamera->foa.Z);
	printf ("fdist = [%f, %f], u0 = %f, v0 = %f, roll = %f\n", actualCamera->fdistx, actualCamera->fdisty, actualCamera->u0, actualCamera->v0, actualCamera->roll);
	printf ("K matrix:\n");
	printf ("%f %f %f %f\n", actualCamera->k11, actualCamera->k12, actualCamera->k13, actualCamera->k14);
	printf ("%f %f %f %f\n", actualCamera->k21, actualCamera->k22, actualCamera->k23, actualCamera->k24);
	printf ("%f %f %f %f\n", actualCamera->k31, actualCamera->k32, actualCamera->k33, actualCamera->k34);
	printf ("RT matrix:\n");
	printf ("%f %f %f %f\n", actualCamera->rt11, actualCamera->rt12, actualCamera->rt13, actualCamera->rt14);
	printf ("%f %f %f %f\n", actualCamera->rt21, actualCamera->rt22, actualCamera->rt23, actualCamera->rt24);
	printf ("%f %f %f %f\n", actualCamera->rt31, actualCamera->rt32, actualCamera->rt33, actualCamera->rt34);
	printf ("%f %f %f %f\n", actualCamera->rt41, actualCamera->rt42, actualCamera->rt43, actualCamera->rt44);
}

inline void drawCross(struct image_struct *image, HPoint2D p2d, int side,	unsigned char r, unsigned char g, unsigned char b) {
		int i, offset;

		// linea vertical
		for (i=-side; i<side+1; i++) {
			offset = image->width * ((int)p2d.y+i) * image->bpp + ((int)p2d.x * image->bpp);
			image->image[offset + 0] = b;
			image->image[offset + 1] = g;
			image->image[offset + 2] = r;
		}

		// linea horizontal
		for (i=-side; i<side+1; i++) {
			offset = image->width * (int)p2d.y * image->bpp + (((int)p2d.x+i) * image->bpp);
			image->image[offset + 0] = b;
			image->image[offset + 1] = g;
			image->image[offset + 2] = r;
		}
}

/*CALLBACKS*/
void floorButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myViewButton;

	myViewButton = (GtkToggleButton *)glade_xml_get_widget(xml, "worldButton");
	gtk_toggle_button_set_active (myViewButton, FALSE);
	printf ("entramos en pressed\n");
  floorView = TRUE;
	worldView = FALSE;
}

void floorButton_released (GtkButton *button, gpointer user_data) {}

void worldButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myViewButton;

	myViewButton = (GtkToggleButton *)glade_xml_get_widget(xml, "floorButton");
	gtk_toggle_button_set_active (myViewButton, FALSE);

  worldView = TRUE;
  floorView = FALSE;
}

void worldButton_released (GtkButton *button, gpointer user_data) {}

void go1Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 1000;
}

void go1Button_released (GtkButton *button, gpointer user_data) {}

void go50Button_pressed (GtkButton *button, gpointer user_data) {
	traslacion += 500;
}

void go50Button_released (GtkButton *button, gpointer user_data) {}

void turn45RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 45;
}

void turn45RButton_released (GtkButton *button, gpointer user_data) {}

void turn45LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 45;
}

void turn45LButton_released (GtkButton *button, gpointer user_data) {}

void turn90RButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion += 90;
}

void turn90RButton_released (GtkButton *button, gpointer user_data) {}

void turn90LButton_pressed (GtkButton *button, gpointer user_data) {
	rotacion -= 90;
}

void turn90LButton_released (GtkButton *button, gpointer user_data) {}

void flashButton_pressed (GtkCheckMenuItem *menu_item, gpointer user_data) {
	moveAndFlash = 1;
}

void flashButton_released (GtkButton *button, gpointer user_data) {}

void camera1Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 1;
}

void camera1Button_released (GtkButton *button, gpointer user_data) {}

void camera2Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 2;
}

void camera2Button_released (GtkButton *button, gpointer user_data) {}

void camera3Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 3;
}

void camera3Button_released (GtkButton *button, gpointer user_data) {}

void camera4Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 4;
}

void camera4Button_released (GtkButton *button, gpointer user_data) {}

void ceilCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 5;
}

void ceilCameraButton_released (GtkButton *button, gpointer user_data) {}

void userCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 0;
}

void userCameraButton_released (GtkButton *button, gpointer user_data) {}

inline static gboolean button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  event = (GdkEventButton*)event;
  old_x=event->x;
  old_y=event->y;

  if (2 == event->button){
  	boton_pulsado=2;
  	flashImage = TRUE;
 	  return TRUE;
  } else {
  	flashImage = FALSE;
  	if (1 == event->button){
  	  boton_pulsado=1;
  	  cam_mode = 1;
   	  foa_mode = 0;
	  } else if (3 == event->button) {
	    boton_pulsado=3;
	    foa_mode = 1;
	    cam_mode = 0;
	  }
		return TRUE;
  }
}

inline static gboolean motion_notify_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
   float x=event->x;
   float y=event->y;
  
	event = (GdkEventButton*)event;

	if (2==boton_pulsado) {
		flashImage = TRUE;
	  return TRUE;
	}	

	mouse_on_visualSonarcanvas.x = ((event->x*360/visualSonarCanvas->allocation.width)-180);
	mouse_on_visualSonarcanvas.y = ((event->y*-180/visualSonarCanvas->allocation.height)+90);
	//printf ("Valores de mouse_on_canvas: %f y %f \n",mouse_on_visualSonarcanvas.x,mouse_on_visualSonarcanvas.y);
 
	if (GDK_BUTTON1_MASK){ /*Si está pulsado el botón 1*/
      theta -= x - old_x;
      phi -= y - old_y;
      gtk_widget_queue_draw (widget);
      old_x=x;
      old_y=y;
   }
 return TRUE;
}

inline static gboolean scroll_event (GtkRange *range, GdkEventScroll *event, gpointer data){
	if (event->direction == GDK_SCROLL_DOWN){
	if (radius > MIN_RADIUS_VALUE)
   	radius-=WHEEL_DELTA;
   }
   if (event->direction == GDK_SCROLL_UP){
     if (radius<MAX_RADIUS_VALUE){
      radius+=WHEEL_DELTA;
       }
   }
   if (radius < 0.5) radius = 0.5;
   gtk_widget_queue_draw(GTK_WIDGET((GtkWidget *)data));
   
   return TRUE;
}

inline struct image_struct *create_image(int width, int height, int bpp) {
	struct image_struct *w;

	w = (struct image_struct*) malloc(sizeof(struct image_struct));
	w->width = width;
	w->height = height;
	w->bpp = bpp;
	w->image = (char*) malloc(width * height * bpp);

	return w;
}

inline void remove_image(struct image_struct *w) {
	free(w->image);
	free(w);
}

/** prepare2draw ************************************************************************
* Prepare an image to draw it with a GtkImage in BGR format with 3 pixels per byte.	*
*	src: source image								*
*	dest: destiny image. It has to have the same size as the source image and	*
*	      3 bytes per pixel.
*****************************************************************************************/
inline void prepare2draw (struct image_struct *src, struct image_struct *dest) {
	int i;

	for (i=0; i<src->width*src->height; i++) {
		dest->image[i*dest->bpp+0] = src->image[i*src->bpp+2];
		dest->image[i*dest->bpp+1] = src->image[i*src->bpp+1];
		dest->image[i*dest->bpp+2] = src->image[i*src->bpp+0];
	}
}

inline unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

inline void drawMyLines () {
  glLineWidth(1.5f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);

	// Borde exterior del recinto
  glBegin(GL_LINES);
  v3f(0., 0., 0.000000);
  v3f(2600., 0., 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2600., 0., 0.000000);
  v3f(2600., 1400., 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2600., 1400., 0.000000);
  v3f(0., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(0., 1400., 0.000000);
  v3f(0., 0., 0.000000);
	glEnd();

	// Líneas transversales
  glBegin(GL_LINES);
  v3f(500., 0., 0.000000);
  v3f(500., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1000., 0., 0.000000);
  v3f(1000., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1500., 0., 0.000000);
  v3f(1500., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(2000., 0., 0.000000);
  v3f(2000., 1400., 0.000000);
	glEnd();

	// Triángulos rectos
	// 1
  glBegin(GL_LINES);
  v3f(800., 100., 0.000000);
  v3f(500., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(800., 750., 0.000000);
  v3f(500., 400., 0.000000);
	glEnd();

	// 2
  glBegin(GL_LINES);
  v3f(1350., 85., 0.000000);
  v3f(1000., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1350., 800., 0.000000);
  v3f(1000., 400., 0.000000);
	glEnd();

	// 3
  glBegin(GL_LINES);
  v3f(1850., 75., 0.000000);
  v3f(1500., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1900., 800., 0.000000);
  v3f(1500., 400., 0.000000);
	glEnd();

	// 4
  glBegin(GL_LINES);
  v3f(2350., 85., 0.000000);
  v3f(2000., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(2400., 850., 0.000000);
  v3f(2000., 400., 0.000000);
	glEnd();
}

/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
inline void linePlaneIntersection (HPoint3D A, HPoint3D B) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	A.X = A.X;
	A.Y = A.Y;
	A.Z = A.Z;

	B.X = B.X;
	B.Y = B.Y;
	B.Z = B.Z;

	v.X = (B.X - A.X);
	v.Y = (B.Y - A.Y);
	v.Z = (B.Z - A.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint.Z = 0; // intersectionPoint.Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint.X = A.X + (t*v.X);
	intersectionPoint.Y = A.Y + (t*v.Y);
}

inline float distanceBetweenPoints (HPoint3D p1, HPoint3D p2) {
  float dist;

  dist = abs(sqrt(pow(p2.X-p1.X,2) + pow(p2.Y-p1.Y,2) + pow(p2.Z-p1.Z,2)));
  return dist;
}

inline void getMaximizedSegment (HPoint3D *seg1Start, HPoint3D *seg1End, HPoint3D *seg2Start, HPoint3D *seg2End, HPoint3D *startPoint, HPoint3D *endPoint) {

	HPoint3D* myArray[4] = {seg1Start, seg1End, seg2Start, seg2End};
	float max = 0;
	int i, j;
	float value;

	for (i = 0; i < 4; i ++) {
		for (j = i+1; j < 4; j ++) {
			value = abs(distanceBetweenPoints (*myArray[i], *myArray[j]));
			if (value > max) {
				max = value;
				*startPoint = *myArray[i];
				*endPoint = *myArray[j];
			}
		}
	}
	// Finally, we'll get two points in order to maximize distance between them
}

inline int overlapping (int i, HPoint3Dinfo proy1, HPoint3Dinfo proy2, int ubi1, int ubi2, float dist1, float dist2) {
	int found = 1;
	float d1, d2;
	HPoint3D startPoint, endPoint;

	if ((abs(dist1)<MAX_PAR) && (abs(dist2)<MAX_PAR)) { // They're parallel segments. Now we check if they're in distance
		// Posible cases: 1-Two intermediate points; 2-One intermediate point; 3-Any intermediate point
		getMaximizedSegment (&groundSegments3D[i].start.position, &groundSegments3D[i].end.position, &proy1.position, &proy2.position, &startPoint, &endPoint);

		if ((ubi1==0)&&(ubi2==0)) { // 1-Two intermediate points. We don't do anything
			found = 1;
		} else { // 2-One intermediate point
			if ((ubi1==0)||(ubi2==0)) {
				if ((ubi1==0)&&(ubi2==1)) {
					groundSegments3D[i].end = proy2;
				} else {
					if ((ubi1==0)&&(ubi2==-1)) {
						groundSegments3D[i].start = proy2; 
					} else {
						if ((ubi1==1)&&(ubi2==0)) {
							groundSegments3D[i].end = proy1; 
						} else {
							if ((ubi1==-1)&&(ubi2==0)) {
								groundSegments3D[i].start = proy1; 
							}
						}
					}
				}
			} else { // 3-Any intermediate point
				if ((ubi1==-1)&&(ubi2==1)) { // One on the right side and the other one on the left side
					groundSegments3D[i].start = proy1;
					groundSegments3D[i].end = proy2;
				} else {
					if ((ubi1==1)&&(ubi2==-1)) {
						groundSegments3D[i].start = proy2;
						groundSegments3D[i].end = proy1;
					} else {
						if (ubi1==1) { // segment is on the right side
							d1 = distanceBetweenPoints(proy1.position, groundSegments3D[i].end.position);
							d2 = distanceBetweenPoints(proy2.position, groundSegments3D[i].end.position);

							if ((abs(d1)<MAX_DIST)||(abs(d2)<MAX_DIST)) {
								if (d1<d2) {
									groundSegments3D[i].end = proy2; 
								} else {
									groundSegments3D[i].end = proy1;
								}
							} else { // They don't fusion themselves
								found = 0;
							}
						} else {    
							d1 = distanceBetweenPoints(proy1.position, groundSegments3D[i].start.position);
							d2 = distanceBetweenPoints(proy2.position, groundSegments3D[i].start.position); 

							if ((abs(d1)<MAX_DIST)||(abs(d2)<MAX_DIST)) {
								if (d1<d2) {
									groundSegments3D[i].start = proy2; 
								} else {
									groundSegments3D[i].start = proy1;
								}
							} else { // They don't fusion themselves
								found = 0;
							}
						} 
					}
				} 
			}
		}
		groundSegments3D[i].start.position = startPoint;
		groundSegments3D[i].end.position = endPoint;
	} else { // They aren't parallel lines
		found = 0;
	}

	return found;
}

inline float segmentMagnitude (Segment3D segment) {
	HPoint3D vector;

	vector.X = segment.start.position.X - segment.end.position.X;
	vector.Y = segment.start.position.Y - segment.end.position.Y;
	vector.Z = segment.start.position.Z - segment.end.position.Z;
	vector.H = 1.;

	return (float) (sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z));
}

inline int distancePointLine (HPoint3Dinfo Point, Segment3D segment, HPoint3Dinfo *Intersection, float *Distance) {
	float LineMag;
	float U;
	int res;

	LineMag = segmentMagnitude (segment);

	U = ( ( ( Point.position.X - segment.start.position.X ) * ( segment.end.position.X - segment.start.position.X ) ) +
		    ( ( Point.position.Y - segment.start.position.Y ) * ( segment.end.position.Y - segment.start.position.Y ) ) +
		    ( ( Point.position.Z - segment.start.position.Z ) * ( segment.end.position.Z - segment.start.position.Z ) ) ) /
			  ( LineMag * LineMag );

	Intersection->position.X = segment.start.position.X + U * ( segment.end.position.X - segment.start.position.X );
	Intersection->position.Y = segment.start.position.Y + U * ( segment.end.position.Y - segment.start.position.Y );
	Intersection->position.Z = segment.start.position.Z + U * ( segment.end.position.Z - segment.start.position.Z );

	if( U >= 0.0f || U <= 1.0f ) {
		res = 0;
	} else {
		if (U < 0.) { // Intersection will be after segment
			res = -1;
		} else { // Intersection will be before segment
			res = +1;
		}
	}

	*Distance = distanceBetweenPoints(Point.position, Intersection->position);
	return res;
}

inline int areTheSameSegment (Segment3D s1, Segment3D s2) {
	int areTheSame = 0;
	if ((((float)s1.start.position.X==(float)s2.start.position.X) && 
			 ((float)s1.start.position.Y==(float)s2.start.position.Y) && 
			 ((float)s1.start.position.Z==(float)s2.start.position.Z) && 
			 ((float)s1.end.position.X==(float)s2.end.position.X) &&
			 ((float)s1.end.position.Y==(float)s2.end.position.Y) && 
			 ((float)s1.end.position.Z==(float)s2.end.position.Z))	|| 
			(((float)s1.start.position.X==(float)s2.end.position.X) && 
			 ((float)s1.start.position.Y==(float)s2.end.position.Y) && 
			 ((float)s1.start.position.Z==(float)s2.end.position.Z) && 
			 ((float)s1.end.position.X==(float)s2.start.position.X) &&
			 ((float)s1.end.position.Y==(float)s2.start.position.Y) && 
			 ((float)s1.end.position.Z==(float)s2.start.position.Z))) { // they're the same segment
		areTheSame = 1;
	}

	return (areTheSame);
}

inline int mergeSegment (Segment3D segment) {
	int i=0,found=0;
	HPoint3Dinfo proy1, proy2;
	int ubi1, ubi2;
	float dist1, dist2;

	while((i < MAX_LINES_IN_MEMORY) && (!found))	{
		if (groundSegments3D[i].isValid == 1) {
			if (areTheSameSegment (groundSegments3D[i], segment)) {
				found = 1;
			} else {
				// Check every segment in memory, with segment. Calculate intersection point between segment i and segment.start perpendicular
				ubi1 = distancePointLine (segment.start, groundSegments3D[i], &proy1, &dist1);
				ubi2 = distancePointLine (segment.end, groundSegments3D[i], &proy2, &dist2);

				found = overlapping (i, proy1, proy2, ubi1, ubi2, dist1, dist2); // If they're parallel lines -> we finish checking
			}
		}
		i ++;
	}

	return found;
}

inline void printPoint (HPoint3D point) {
	printf ("(%f, %f, %f)", point.X, point.Y, point.Z);
}

inline void printParallelogram (Parallelogram3D par1) {
	HPoint3D* myArray1[4] = {&(par1.p1.position), &(par1.p2.position), &(par1.p3.position), &(par1.p4.position)};
	int i;

	printf ("Parallelogram %p\n", &par1);
	printf ("=====================\n");
	printf ("[");
	for (i = 0; i < 4; i ++) {
		printPoint (*myArray1[i]);
		printf (", ");
	}
	printf ("]\n\n");
}

inline int areTheSameParallelogram (Parallelogram3D par1, Parallelogram3D par2) {
	HPoint3D* myArray1[4] = {&(par1.p1.position), &(par1.p2.position), &(par1.p3.position), &(par1.p4.position)};
	HPoint3D* myArray2[4] = {&(par2.p1.position), &(par2.p2.position), &(par2.p3.position), &(par2.p4.position)};
	int i, j, found;
	float value;
	int equalPointsCounter = 0;

	//printParallelogram (par1);
	//printParallelogram (par2);

	for (i = 0; i < 4; i ++) {
		j = 0;
		found = FALSE;
		while ((j < 4) && (!found)) {
			value = abs(distanceBetweenPoints (*myArray1[i], *myArray2[j]));
			if (value < 20.) {
				equalPointsCounter ++;
				found = TRUE;
			}
			j ++;
		}
	}

	return (equalPointsCounter == 4);
}

inline int mergeParallelogram (Parallelogram3D parallelogram) {
	int i=0,found=0;

	while(i < MAX_PARALLELOGRAMS_IN_MEMORY && !found)	{
		if (groundParallelograms3D[i].isValid == 1) {
			found = areTheSameParallelogram (parallelogram, groundParallelograms3D[i]);
		}
		i ++;
	}

	return found;
}

inline void point2angle () {
	HPoint3D p;
	float pan, tilt;

	if (myMaxSaliency->type == 1)
		p = myMaxSaliency->parallelogram.centroid;
	else if (myMaxSaliency->type == 2)
		p = myMaxSaliency->face.center.position;

	p.X = p.X - robotCamera.position.X;
	p.Y = p.Y - robotCamera.position.Y;
	p.Z = 0. - robotCamera.position.Z;
	p.H = 1.;

	if (p.Y == 0.) pan = 0.;
	else pan = atan(p.Y/p.X);

	if (p.X == 0.) tilt = 0.;
	else tilt = atan (p.Z/p.X); // La Z siempre saldrá negativa, porque estará más alta que el punto

	myMaxSaliency->longitude = pan * 180.0/PI;
	myMaxSaliency->latitude = tilt * 180.0/PI;
	printf ("p = [%f, %f, %f], pan = %f, tilt = %f, New coordinates: [%f, %f]\n", p.X, p.Y, p.Z, pan, tilt, myMaxSaliency->latitude, myMaxSaliency->longitude);
}

inline void getPolygonCentroid (Parallelogram3D* parallelogram) {
	HPoint3D vertexList[5]; // Remember: Counter Clockwise Winding in order to get the centroid
  int i, j;
  double sum = 0.;
  double area = 0.;
	HPoint3D centroid;

	vertexList[0] = parallelogram->p2.position;
	vertexList[1] = parallelogram->p1.position;
	vertexList[2] = parallelogram->p3.position;
	vertexList[3] = parallelogram->p4.position;
	vertexList[4] = vertexList[0];

  centroid.X = 0.;
  centroid.Y = 0.;
  centroid.Z = 0.;
	centroid.H = 1.;

  for (i = 0; i < 5; i++) {
		j = (i+1)%5;
    area = vertexList[i].X * vertexList[j].Y - vertexList[i].Y * vertexList[j].X;
    sum += area;
    centroid.X += (vertexList[i].X + vertexList[j].X) * area;
    centroid.Y += (vertexList[i].Y + vertexList[j].Y) * area;
  }
  sum *= 3.;
  centroid.X /= sum;
  centroid.Y /= sum;

	parallelogram->centroid = centroid;
	printf ("Centroid = [%f, %f, %f]\n", centroid.X, centroid.Y, centroid.Z);
}

inline void fillParallelogramElement (Parallelogram3D parallelogram) {
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));
	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;

	// Si estamos aquí es porque acabamos de ver un paralelogramo, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima

	myActualElement->type = 1; // elemento paralelogramo
	myActualElement->parallelogram = parallelogram;
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline int isMergedElementAndInsert () {
	struct elementStruct *r;
	int found = 0;

	if (myActualElement != NULL) {
		r = myElements;
		if (elementsStructCounter != 0) { // ahora haremos uso de p
			while ((r->next != NULL)&&(!found)) { // Llegamos hasta el último de la cola, si no lo encontramos antes
				if ((r->type == myActualElement->type) && 
						(((r->type == 1) && (areTheSameParallelogram (r->parallelogram, myActualElement->parallelogram))) ||
						((r->type == 3) && (areTheSameArrow (r->arrow, myActualElement->arrow))) ||
						((r->type == 2) && (areTheSameFace (r->face, myActualElement->face))))) {
					r->saliency -= SALIENCY_DECREMENT;
					r->liveliness += LIVELINESS_INCREMENT;
					found = 1; // ya existía de antes
					printf ("Element has been merged with another similar one!\n");
				}	else {
					r = r->next;
				}
			}
		} 
		if ((elementsStructCounter == 0) || ((elementsStructCounter != 0) && (!found))) { // Inserción: no ha sido encontrado en los existentes
			r = myElements; // nos lo guardamos
			myElements = myActualElement;
			myActualElement->next = r; // ahora el nuevo será el primero en la lista
			elementsStructCounter ++;
			printf ("Element type %i has been inserted correctly!\n", myActualElement->type);
		}
	} else
		printf ("Error: element is NULL\n");
}

inline void insertParallelogramOnAttentionSystem (Parallelogram3D parallelogram) {
	getPolygonCentroid (&parallelogram); // después podremos calcular el foco de atención
	fillParallelogramElement (parallelogram);
	isMergedElementAndInsert ();
}

inline void storeParallelogram3D (Parallelogram3D parallelogram) {
	int isMerge = 0, pos;

	if (numParallelograms == 0)	{
		groundParallelograms3D[numParallelograms].p1.position = parallelogram.p1.position;
		groundParallelograms3D[numParallelograms].p2.position = parallelogram.p2.position;
		groundParallelograms3D[numParallelograms].p3.position = parallelogram.p3.position;
		groundParallelograms3D[numParallelograms].p4.position = parallelogram.p4.position;
		groundParallelograms3D[numParallelograms].isValid = 1;

		numParallelograms ++;
	} else { // We already have a parallelogram, so we've to try to overlap it or store it
		isMerge = mergeParallelogram (parallelogram);

		if (!isMerge) { // check insertion position
			if (numParallelograms < MAX_PARALLELOGRAMS_IN_MEMORY)	{
				pos = numParallelograms;
				numParallelograms ++;
			} else {
				pos = incNumParallelograms;
				incNumParallelograms++;
				if (incNumParallelograms == MAX_PARALLELOGRAMS_IN_MEMORY) {
					incNumParallelograms=0;
				}
			}
			groundParallelograms3D[numParallelograms].p1.position = parallelogram.p1.position;
			groundParallelograms3D[numParallelograms].p2.position = parallelogram.p2.position;
			groundParallelograms3D[numParallelograms].p3.position = parallelogram.p3.position;
			groundParallelograms3D[numParallelograms].p4.position = parallelogram.p4.position;
			groundParallelograms3D[numParallelograms].isValid = 1;

		} //else printf ("Parallelogram merged TOTAL PARALLELOGRAMS = %i\n", numParallelograms);
	}
}

inline void storeSegment3D (Segment3D segment) {
	int isMerge = 0, pos;

	if (segmentMagnitude(segment)>MIN_TAM_SEG) {
		if (numSegments == 0)	{
			groundSegments3D[numSegments].start = segment.start;
			groundSegments3D[numSegments].end = segment.end;
			groundSegments3D[numSegments].idColor = segment.idColor;
			groundSegments3D[numSegments].isValid = 1;

			numSegments ++;
		} else { // We already have a segment, so we've to try to overlap it or store it
			isMerge = mergeSegment (segment);
			if (!isMerge) { // check insertion position
				//printf ("Storing new segments in memory\n");
				if (numSegments < MAX_LINES_IN_MEMORY)	{
					pos = numSegments;
					numSegments ++;
				} else {
					pos = incNumSegments;
					incNumSegments++;
					if (incNumSegments == MAX_LINES_IN_MEMORY) {
						incNumSegments=0;
					}
				}
				groundSegments3D[pos].start = segment.start;
				groundSegments3D[pos].end = segment.end;
				groundSegments3D[pos].idColor = segment.idColor;
				groundSegments3D[pos].isValid = 1;
			}
		}
	}
}

inline void	createSegment3D (HPoint3D startPoint, HPoint3D endPoint) {
	Segment3D segment;

	segment.start.position.X = startPoint.X;
	segment.start.position.Y = startPoint.Y;
	segment.start.position.Z = startPoint.Z;
	segment.start.position.H = 1.;
	segment.end.position.X = endPoint.X;
	segment.end.position.Y = endPoint.Y;
	segment.end.position.Z = endPoint.Z;
	segment.end.position.H = 1.;

	if (comeFromCenter)
		segment.idColor = 1;
	else if (last_full_movement==left)
		segment.idColor = 2;
	else if (last_full_movement==right)
		segment.idColor = 3;

	storeSegment3D (segment);
}

inline void segmentProjection (Segment2D segment) {
	myCamera = &robotCamera;

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1;

	myStartPoint2D.y = segment.start.x;
	myStartPoint2D.x = 240.-1.-segment.start.y;
	myStartPoint2D.h = 1.;
	myEndPoint2D.y = segment.end.x;
	myEndPoint2D.x = 240.-1.-segment.end.y;
	myEndPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myStartPoint3D, myStartPoint2D, *myCamera);
	backproject(&myEndPoint3D, myEndPoint2D, *myCamera);

	linePlaneIntersection (myStartPoint3D, cameraPos3D);
	intersectionStartPoint = intersectionPoint;
	linePlaneIntersection (myEndPoint3D, cameraPos3D);
	intersectionEndPoint = intersectionPoint;

	createSegment3D (intersectionStartPoint, intersectionEndPoint);
}

inline void pixel2Optical (HPoint2D *p) {
	float aux;

	aux = p->x;
	p->x = 240-1-p->y;
	p->y = aux;		
}

inline void getCameraWorldPos () {
	float actualPan, actualTilt;
	gsl_matrix *robotRT, *baseCuelloRT, *alturaEjeTiltRT, *ejeTiltRT, *camaraRT, *temp1, *temp2, *temp3, *temp4, *foaRel, *foaAbs, *myComodin, *myComodin2;

	actualPan = *mypan_angle;
	actualPan=actualPan*PI/180.0;
	actualTilt=(*mytilt_angle)*PI/180.0;

	//printf ("sliderPantiltBaseX = %f\n, panAngle = %f, tiltAngle = %f", sliderPANTILT_BASE_X, panAngle, tiltAngle);

  robotRT = gsl_matrix_calloc(4,4);
  baseCuelloRT = gsl_matrix_calloc(4,4);
  alturaEjeTiltRT = gsl_matrix_calloc(4,4);
  ejeTiltRT = gsl_matrix_calloc(4,4);
  camaraRT = gsl_matrix_calloc(4,4);
  temp1 = gsl_matrix_calloc(4,4);
  temp2 = gsl_matrix_calloc(4,4);
  temp3 = gsl_matrix_calloc(4,4);
  temp4 = gsl_matrix_calloc(4,4);
  myComodin = gsl_matrix_calloc(4,4);
  myComodin2 = gsl_matrix_calloc(4,4);
	foaRel = gsl_matrix_calloc(4,1);
	foaAbs = gsl_matrix_calloc(4,1);

	// 1º - El robot está transladado respecto al (0, 0) absoluto, y rotado respecto al eje Z
	gsl_matrix_set(robotRT,0,0,cos(myencoders[2]));
	gsl_matrix_set(robotRT,0,1,-sin(myencoders[2]));
	gsl_matrix_set(robotRT,0,2,0.);
	gsl_matrix_set(robotRT,0,3,myencoders[0]);
	gsl_matrix_set(robotRT,1,0,sin(myencoders[2]));
	gsl_matrix_set(robotRT,1,1,cos(myencoders[2]));
	gsl_matrix_set(robotRT,1,2,0.);
	gsl_matrix_set(robotRT,1,3,myencoders[1]);
	gsl_matrix_set(robotRT,2,0,0.);
	gsl_matrix_set(robotRT,2,1,0.);
	gsl_matrix_set(robotRT,2,2,1.);
	gsl_matrix_set(robotRT,2,3,0.); // Z de la base robot la considero 0
	gsl_matrix_set(robotRT,3,0,0.);
	gsl_matrix_set(robotRT,3,1,0.);
	gsl_matrix_set(robotRT,3,2,0.);
	gsl_matrix_set(robotRT,3,3,1.0);

	// 2º - La base del cuello está transladado en Z y en X respecto a la base del robot (considerada en el suelo)
	gsl_matrix_set(baseCuelloRT,0,0,cos(0.));
	gsl_matrix_set(baseCuelloRT,0,1,sin(0.));
	gsl_matrix_set(baseCuelloRT,0,2,0.);
	gsl_matrix_set(baseCuelloRT,0,3,sliderPANTILT_BASE_X);
	gsl_matrix_set(baseCuelloRT,1,0,-sin(0.));
	gsl_matrix_set(baseCuelloRT,1,1,cos(0.));
	gsl_matrix_set(baseCuelloRT,1,2,0.);
	gsl_matrix_set(baseCuelloRT,1,3,PANTILT_BASE_Y);
	gsl_matrix_set(baseCuelloRT,2,0,0.);
	gsl_matrix_set(baseCuelloRT,2,1,0.);
	gsl_matrix_set(baseCuelloRT,2,2,1.);
	gsl_matrix_set(baseCuelloRT,2,3,sliderPANTILT_BASE_HEIGHT); // desde el suelo a la base
	gsl_matrix_set(baseCuelloRT,3,0,0.);
	gsl_matrix_set(baseCuelloRT,3,1,0.);
	gsl_matrix_set(baseCuelloRT,3,2,0.);
	gsl_matrix_set(baseCuelloRT,3,3,1.0);

	// 3º - El eje de tilt está transladado en Z respecto a la base del cuello, y rotado respecto al eje Z
	gsl_matrix_set(alturaEjeTiltRT,0,0,cos(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,0,1,-sin(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,0,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,0,3,0.);
	gsl_matrix_set(alturaEjeTiltRT,1,0,sin(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,1,1,cos(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,1,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,1,3,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,0,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,1,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,2,1.);
	gsl_matrix_set(alturaEjeTiltRT,2,3,sliderTILT_HEIGHT);
	gsl_matrix_set(alturaEjeTiltRT,3,0,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,1,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,3,1.0);

	// 4º - El eje de tilt está además rotado respecto al eje Y
	gsl_matrix_set(ejeTiltRT,0,0,cos(actualTilt));
	gsl_matrix_set(ejeTiltRT,0,1,0.);
	gsl_matrix_set(ejeTiltRT,0,2,-sin(actualTilt));
	gsl_matrix_set(ejeTiltRT,0,3,0.);
	gsl_matrix_set(ejeTiltRT,1,0,0.);
	gsl_matrix_set(ejeTiltRT,1,1,1.);
	gsl_matrix_set(ejeTiltRT,1,2,0.);
	gsl_matrix_set(ejeTiltRT,1,3,0.);
	gsl_matrix_set(ejeTiltRT,2,0,sin(actualTilt));
	gsl_matrix_set(ejeTiltRT,2,1,0.);
	gsl_matrix_set(ejeTiltRT,2,2,cos(actualTilt));
	gsl_matrix_set(ejeTiltRT,2,3,0.);
	gsl_matrix_set(ejeTiltRT,3,0,0.);
	gsl_matrix_set(ejeTiltRT,3,1,0.);
	gsl_matrix_set(ejeTiltRT,3,2,0.);
	gsl_matrix_set(ejeTiltRT,3,3,1.0);

	// El centro óptico de la cámara está transladado en X y en Z respecto al eje tilt
	gsl_matrix_set(camaraRT,0,0,cos(0.));
	gsl_matrix_set(camaraRT,0,1,sin(0.));
	gsl_matrix_set(camaraRT,0,2,0.);
	gsl_matrix_set(camaraRT,0,3,sliderISIGHT_OPTICAL_CENTER);
	gsl_matrix_set(camaraRT,1,0,-sin(0.));
	gsl_matrix_set(camaraRT,1,1,cos(0.));
	gsl_matrix_set(camaraRT,1,2,0.);
	gsl_matrix_set(camaraRT,1,3,0.);
	gsl_matrix_set(camaraRT,2,0,0.);
	gsl_matrix_set(camaraRT,2,1,0.);
	gsl_matrix_set(camaraRT,2,2,1.);
	gsl_matrix_set(camaraRT,2,3,sliderCAMERA_TILT_HEIGHT);
	gsl_matrix_set(camaraRT,3,0,0.);
	gsl_matrix_set(camaraRT,3,1,0.);
	gsl_matrix_set(camaraRT,3,2,0.);
	gsl_matrix_set(camaraRT,3,3,1.0);

	gsl_linalg_matmult (robotRT, baseCuelloRT, temp1);
	gsl_linalg_matmult (temp1, alturaEjeTiltRT, temp2);
	gsl_linalg_matmult (temp2, ejeTiltRT, temp3);
	gsl_linalg_matmult (temp3, camaraRT, temp4);

//	gsl_linalg_matmult (robotRT, myComodin, temp1);
//	gsl_linalg_matmult (temp1, myComodin2, temp2);
	gsl_matrix_set(foaRel,0,0,3000.0);
	gsl_matrix_set(foaRel,1,0,0.0);
	gsl_matrix_set(foaRel,2,0,0.0);
	gsl_matrix_set(foaRel,3,0,1.0);

	gsl_linalg_matmult(temp4,foaRel,foaAbs);

	robotCamera.position.X = gsl_matrix_get (temp4, 0, 3);
	robotCamera.position.Y = gsl_matrix_get (temp4, 1, 3);
	robotCamera.position.Z = gsl_matrix_get (temp4, 2, 3);
  robotCamera.foa.X=(float)gsl_matrix_get(foaAbs,0,0);
  robotCamera.foa.Y=(float)gsl_matrix_get(foaAbs,1,0);
  robotCamera.foa.Z=(float)gsl_matrix_get(foaAbs,2,0);

	update_camera_matrix (&robotCamera);

	//printCameraInformation (&robotCamera);

	// Liberación de memoria de matrices
  gsl_matrix_free(robotRT);
  gsl_matrix_free(baseCuelloRT);
  gsl_matrix_free(alturaEjeTiltRT);
  gsl_matrix_free(ejeTiltRT);
  gsl_matrix_free(camaraRT);
  gsl_matrix_free(temp1);
  gsl_matrix_free(temp2);
  gsl_matrix_free(temp3);
  gsl_matrix_free(temp4);
  gsl_matrix_free(foaRel);
  gsl_matrix_free(foaAbs);
  gsl_matrix_free(myComodin);
  gsl_matrix_free(myComodin2);
}

inline void getFoaOnMovement () {
	float myFoaX, myFoaY, myFoaZ;
	float myPosX, myPosY, myPosZ;
	float actualPan, actualTilt, hip;

	actualPan = *mypan_angle;
	actualTilt = *mytilt_angle;
	myPosX = myencoders[1];
	myPosY = myencoders[0];
	myPosZ = 480.; // a capón, yo sé que la cámara está a esa altura

	actualPan=actualPan*PI/180.0;
	actualTilt=actualTilt*PI/180.0;

	hip = myPosZ/tan(actualTilt);
	myFoaY = hip*sin(actualPan);
	myFoaX = sqrt(pow(hip,2) - pow(myFoaY,2));

  // obtener la posición absoluta, contando la posición del robot (posx, posy)
	myFoaX += myPosX;
	myFoaY += myPosY;
	myFoaZ = 0;

  robotCamera.foa.X=myFoaX;
  robotCamera.foa.Y=myFoaY;
	update_camera_matrix (&robotCamera);

	printf ("Foa que adopta la cámara = [%f, %f]\n", robotCamera.foa.X, robotCamera.foa.Y);
}

inline void movePantilt() {  
  *mylongitude_speed=1200*ENCOD_TO_DEG;
  *mylatitude_speed = 0.0;
	//printf ("actualInstant vs. stopInstant = [%f, %f]\n", actualInstant, stopInstant);

	if (comeFromCenter) {
	  if ((last_full_movement==left) && (actualInstant - stopInstant > TIME_TO_LOOK)) { /* Buscamos hacia la izquierda */
	    if ((*mypan_angle > MAXPAN_NEG/3) && (*mypan_angle != MAXPAN_NEG/3)) {
				pantiltStill = FALSE;
	      *mylongitude = *min_pan/3;
	    } else {
				numIterations++;
				if(numIterations==2) {
		      last_full_movement=right;
					comeFromCenter = FALSE;
					getCameraWorldPos ();
					stopInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
					numIterations=0;
					pantiltStill = TRUE;
					numFlashes ++;
				}
	    }
	  } else if ((last_full_movement==right) && (actualInstant - stopInstant > TIME_TO_LOOK)) { /* Buscamos hacia la derecha */
	    if ((*mypan_angle < MAXPAN_POS/3) && (*mypan_angle != MAXPAN_POS/3)){
				pantiltStill = FALSE;
	      *mylongitude = *max_pan/3;
	    } else {
				numIterations++;
				if(numIterations==2) {
		      last_full_movement=left;
					comeFromCenter = FALSE;
					getCameraWorldPos ();
					stopInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
					numIterations=0;
					pantiltStill = TRUE;
					numFlashes ++;
				}
	    }
	  }
	} else if (actualInstant - stopInstant > TIME_TO_LOOK) { // venimos de algún extremo, así que vamos al centro ahora
		if ((*mypan_angle) != 0) {			
			pantiltStill = FALSE;
			*mylongitude = 0;
		} else if ((*mypan_angle) == 0) {
			numIterations++;
			if(numIterations==2) {
				comeFromCenter = TRUE;
				numIterations++;
				getCameraWorldPos ();
				stopInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
				numIterations=0;
				pantiltStill = TRUE;
				numFlashes ++;
			}
		}
	}
}

inline void moveInitialTilt () {
  speed_y = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(tiltAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));
  speed_x = VEL_MAX_PAN-((POS_MAX_PAN-DEG_TO_ENCOD*(abs(panAngle)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN)));

	if ((*mylatitude_speed) < VEL_MIN_TILT)
		*mylatitude_speed = VEL_MIN_TILT*4;
	else *mylatitude_speed = speed_y;
	if ((*mylongitude_speed) < VEL_MIN_PAN)
		*mylongitude_speed = VEL_MIN_PAN*4;
	else *mylongitude_speed = speed_x;

	*mylatitude = tiltAngle;
	*mylongitude = panAngle;
	if (((*mypan_angle) < ((*mylongitude) + 1.)) && ((*mypan_angle) > ((*mylongitude) - 1.)) && ((*mytilt_angle) < ((*mylatitude) + 1.)) && ((*mytilt_angle) > ((*mylatitude) - 1.))) {
		completedMovement = TRUE;
		pantiltStill = TRUE;
	}	else
		completedMovement = FALSE;
}


/*Dibujamos una linea desde p1 a p2 en la imagen*/
inline int projectSegment (IplImage* src, IplImage* src2, HPoint2D p1, HPoint2D p2) {
	/* it takes care of important features */
	/* before/behind the focal plane, inside/outside the image frame */

	CvPoint pt1, pt2;
	HPoint2D gooda,goodb;

	if(displayline(p1,p2,&gooda,&goodb,robotCamera)==1) {
		//printf ("[%f, %f, %f] [%f, %f, %f]\n", p1.x, p1.y, p1.h, p2.x, p2.y, p2.h);
		//Pasamos de coordenadas opticas a pixels

		pt1.x=(int)gooda.y;
		pt1.y=SIFNTSC_ROWS-1-(int)gooda.x;

		pt2.x=(int)goodb.y;
		pt2.y=SIFNTSC_ROWS-1-(int)goodb.x;

		cvLine(src, pt1, pt2, CV_RGB(0.,0.,0.), 3, 8, 0);
		cvLine(src2, pt1, pt2, CV_RGB(0.,0.,0.), 3, 8, 0);

		return 1;
	}

	return 0;
}

inline void projectKnownSegments (IplImage* img, IplImage* img2) {
	int i;
	HPoint3D start3D, end3D;
	HPoint2D start2D, end2D;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) {
		if (groundSegments3D[i].isValid == 1) {
			start3D = groundSegments3D[i].start.position;
			end3D = groundSegments3D[i].end.position;

	    project(start3D, &start2D, robotCamera);
	    project(end3D, &end2D, robotCamera);

			projectSegment (img, img2, start2D, end2D);
		}
	}
}

/*Dibujamos una linea desde p1 a p2 en la imagen*/
inline int projectParallelogram (IplImage* src, IplImage* src2, HPoint2D p1, HPoint2D p2, HPoint2D p3, HPoint2D p4, TPinHoleCamera camera) {
	/* it takes care of important features */
	/* before/behind the focal plane, inside/outside the image frame */

	CvPoint pts[5];

	/*Pasamos de coordenadas opticas a pixels*/
	pts[0].x=(int)p1.y;
	pts[0].y=SIFNTSC_ROWS-1-(int)p1.x;

	pts[1].x=(int)p2.y;
	pts[1].y=SIFNTSC_ROWS-1-(int)p2.x;

	pts[2].x=(int)p3.y;
	pts[2].y=SIFNTSC_ROWS-1-(int)p3.x;

	pts[3].x=(int)p4.y;
	pts[3].y=SIFNTSC_ROWS-1-(int)p4.x;

	pts[4].x=pts[0].x;
	pts[4].y=pts[0].y;

	cvFillConvexPoly(src, &pts[0], 5, CV_RGB(0.,0.,0.), 8, 0);
	cvFillConvexPoly(src2, &pts[0], 5, CV_RGB(0.,0.,0.), 8, 0);

	return 1;
}

inline void projectKnownParallelograms (IplImage* img, IplImage* img2) {
	int i;
	HPoint3D p13D, p23D, p33D, p43D;
	HPoint2D p12D, p22D, p32D, p42D;

	for (i = 0; i < MAX_PARALLELOGRAMS_IN_MEMORY; i++) {
		if (groundParallelograms3D[i].isValid == 1) {
			p13D = groundParallelograms3D[i].p2.position; // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
			p23D = groundParallelograms3D[i].p1.position;
			p33D = groundParallelograms3D[i].p3.position;
			p43D = groundParallelograms3D[i].p4.position;

	    project(p13D, &p12D, robotCamera);
	    project(p23D, &p22D, robotCamera);
	    project(p33D, &p32D, robotCamera);
	    project(p43D, &p42D, robotCamera);

			//projectParallelogram (img, img2, p12D, p22D, p32D, p42D, robotCamera);
			projectSegment (img, img2, p12D, p22D);
			projectSegment (img, img2, p22D, p32D);
			projectSegment (img, img2, p32D, p42D);
			projectSegment (img, img2, p42D, p12D);
		}
	}
}

inline void projectMemory (IplImage* img, IplImage* img2) {
	projectKnownSegments (img, img2);
	//projectKnownParallelograms (img, img2);
}

inline void cannyFilter() {
	IplImage *src;
	IplImage *src2;
	IplImage *gray;
	IplImage *edge;
	IplImage *cedge;
	IplImage *contour;
	CvSeq* lines;
	CvSeq* contours;
	CvMemStorage* storage;
	double distanceResolution, angleResolution, maxGapBetweenSegments;
	int numLines, i;

	storage = cvCreateMemStorage(0);
	contours = 0;
	lines = 0;
	distanceResolution = 1.;
	angleResolution = (PI/180.);
	maxGapBetweenSegments = 30.;

	src = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	src2 = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	cedge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	gray = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	contour = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	
	memcpy(src->imageData, image, src->width*src->height*src->nChannels); // aquí tenemos la original image -> src
	memcpy(src2->imageData, image, src2->width*src2->height*src2->nChannels); // aquí tenemos la original image -> src2
	
	//drawMyLinesOnImage (src2); // src2 (RGB) -> src2 (RGB) with floor lines
	//memcpy(image, src2->imageData, src->width*src2->height*src2->nChannels); // aquí tenemos la original image -> src

	// hago filtrado normal, para luego usar en imageAfiltered (con la visualSonar)
	cvCvtColor(src, gray, CV_RGB2GRAY); // en gray paso de src (RGB) -> gray (B/N)

	cvCanny(gray, edge, sliderThreshold, sliderThreshold*3, 3); // saco bordes gray -> edge
	// cvCanny detects borders between threshold and (3xThreshold)
	cvCvtColor(edge, cedge, CV_GRAY2RGB); // paso los bordes edge (B/N) -> cedge (RGB)

	memcpy(imageAbordered->image, cedge->imageData, src->width*src->height*src->nChannels); // paso de cedge -> imageAbordered

	projectMemory (edge, src2);

	memcpy(imageAfiltered->image, src2->imageData, src2->width*src2->height*src2->nChannels);

	lines = cvHoughLines2 (edge, storage, CV_HOUGH_PROBABILISTIC, distanceResolution, angleResolution, HOUGH_LINE_THRESHOLD, HOUGH_MIN_DIST_SEG, maxGapBetweenSegments);	// probabilistic Hough transform: more efficient in case if picture contains a few long linear segments

	if(lines->total > MAX_LINES_TO_DETECT)
		numLines = MAX_LINES_TO_DETECT;
	else
		numLines = lines->total;

	for (i = 0; i < numLines; i ++) { // ATENCIÓN: BUCLE IMPORTANTE DE DETECCIÓN DE LÍNEAS Y RETROPROYECCIÓN
		CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
		cvLine(contour, line[0], line[1], CV_RGB(255,0,0), 3, 8, 0);
		groundSegments2D[i].start.x = (float)(line[0].x);
		groundSegments2D[i].start.y = (float)(line[0].y);
		groundSegments2D[i].start.h = 1.;
		groundSegments2D[i].end.x = (float)(line[1].x);
		groundSegments2D[i].end.y = (float)(line[1].y);
		groundSegments2D[i].end.h = 1.;
		segmentProjection (groundSegments2D[i]);
	}
	memcpy(contourImage, contour->imageData, src->width*src->height*src->nChannels);

	cvReleaseImage(&src);
	cvReleaseImage(&src2);
	cvReleaseImage(&gray);
	cvReleaseImage(&edge);
	cvReleaseImage(&cedge);
	cvReleaseImage(&contour);
	cvReleaseMemStorage(&storage);
}

inline void processImage () {
	int i, j, c, row;

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) {
		c=i%SIFNTSC_COLUMNS;
		row=i/SIFNTSC_COLUMNS;
		j=row*SIFNTSC_COLUMNS+c;

		if (mycolorA!= NULL) {
		  image[i*3]=(*mycolorA)[j*3+2];
		  image[i*3+1]=(*mycolorA)[j*3+1];
		  image[i*3+2]=(*mycolorA)[j*3];
		}
	}

	if (mycolorA!= NULL) {
	  imageA->image = (char *) *mycolorA; // asignamos directamente lo que viene de mycolorA
	}

	cannyFilter (); // aquí hacemos todo el procesado de imagen y retroproyección de líneas en 3D

	// Imagen con segmentos retroproyectados sobre original
	prepare2draw(imageAfiltered, filteredImageRGB);

	// Imagen de la cara
	//prepare2draw(imageAgrounded, groundedImageRGB);

	// Imagen original con el foco de la cámara
	drawCross(imageA, pixel_camA, 5, 255, 0, 0); // pinta el foco de la cámara
	prepare2draw(imageA, originalImageRGB);
}

inline void moveRobot () {
	coveredDistance = sqrt(abs(pow ((actualPosition.x-myencoders[0]), 2)) + abs(pow ((actualPosition.y-myencoders[1]), 2)));

	printf ("actual = [%f, %f] myencoders[%f, %f], covered = %f\n", actualPosition.x, actualPosition.y, myencoders[0], myencoders[1], coveredDistance);

	if (coveredDistance >= 250) {
		robotStill = TRUE;
		*myv=0.0; *myw=0.0;
	}	else
		*myv=V_MAX; *myw=0.0;
}

inline void getLastPoint (Parallelogram3D* square) {
	float t;

	t = (square->p2.position.X - square->p3.position.X)/((square->p1.position.X - square->p2.position.X)-(square->p1.position.X - square->p3.position.X));

	square->p4.position.X = square->p2.position.X + (square->p1.position.X - square->p3.position.X)*t;
	square->p4.position.Y = square->p2.position.Y + (square->p1.position.Y - square->p3.position.Y)*t;
	square->p4.position.Z = square->p2.position.Z + (square->p1.position.Z - square->p3.position.Z)*t;
}

inline int haveACommonVertex (Segment3D s1, Segment3D s2, Parallelogram3D *square){
	HPoint3D point[4];
	int i,j;
	point[0] = s1.start.position;
	point[1] = s1.end.position;
	point[2] = s2.start.position;
	point[3] = s2.end.position;

	j = 0;
	i = 2;

	if((point[i].X > point[j].X - 100.)&&(point[i].X < point[j].X + 100.)
	&&(point[i].Y > point[j].Y - 100.)&&(point[i].Y < point[j].Y + 100.)
	&&(point[i].Z > point[j].Z - 100.)&&(point[i].Z < point[j].Z + 100.)){
				
			square->p1.position = point[0];
			square->p2.position = point[1];
			square->p3.position = point[3];
			return 1;
	}
	j = 0;
	i = 3;
	if((point[i].X > point[j].X - 100.)&&(point[i].X < point[j].X + 100.)
	&&(point[i].Y > point[j].Y - 100.)&&(point[i].Y < point[j].Y + 100.)
	&&(point[i].Z > point[j].Z - 100.)&&(point[i].Z < point[j].Z + 100.)){
				
			square->p1.position = point[0];
			square->p2.position = point[1];
			square->p3.position = point[2];
			return 1;
	}
	j = 1;
	i = 2;
	if((point[i].X > point[j].X - 100.)&&(point[i].X < point[j].X + 100.)
	&&(point[i].Y > point[j].Y - 100.)&&(point[i].Y < point[j].Y + 100.)
	&&(point[i].Z > point[j].Z - 100.)&&(point[i].Z < point[j].Z + 100.)){
				
			square->p1.position = point[1];
			square->p2.position = point[0];
			square->p3.position = point[3];
			return 1;
	}
	j = 1;
	i = 3;
	if((point[i].X > point[j].X - 100.)&&(point[i].X < point[j].X + 100.)
	&&(point[i].Y > point[j].Y - 100.)&&(point[i].Y < point[j].Y + 100.)
	&&(point[i].Z > point[j].Z - 100.)&&(point[i].Z < point[j].Z + 100.)){
				
			square->p1.position = point[1];
			square->p2.position = point[0];
			square->p3.position = point[2];
			return 1;
	}
	return 0;
}

inline float angleBetweenSegments (Segment3D s1, Segment3D s2) {
	HPoint3D A,B;
	float modA,modB;
	float scalarproduct;
	float a;

	A.X = s1.end.position.X -s1.start.position.X;
	A.Y = s1.end.position.Y -s1.start.position.Y;
	A.Z = s1.end.position.Z -s1.start.position.Z;

	B.X = s2.end.position.X -s2.start.position.X;
	B.Y = s2.end.position.Y -s2.start.position.Y;
	B.Z = s2.end.position.Z -s2.start.position.Z;

	modA = sqrt(SQUARE(A.X)+SQUARE(A.Y)+SQUARE(A.Z));
	modB = sqrt(SQUARE(B.X)+SQUARE(B.Y)+SQUARE(B.Z));

	scalarproduct = A.X*B.X + A.Y*B.Y + A.Z*B.Z;

	a = acos(scalarproduct/(modA*modB));
	
	return a;
}

inline void buildBase (Segment3D* s, HPoint3D p) {
	float dist1, dist2;
	HPoint3D temp;
	dist1 = distanceBetweenPoints ((s->start).position, p);
	dist2 = distanceBetweenPoints ((s->end).position, p);

	if (dist1 < dist2) { // el que suponiamos end del segment está más alejado del punto intersección...
		temp = (s->start).position; // ...con lo que procedemos a cambiar de orden los extremos, en el orden correcto
		(s->start).position = (s->end).position;
		(s->end).position = temp;
	} // en caso contrario el segmento base de la flecha está bien tomado (el extremo end es donde está realmente la flecha)
}

inline void buildSemiArrow (Segment3D s1, Segment3D s2, HPoint3D intersec, float dist1, float dist2, SemiArrow3D* mySemiArrow) {
	if (dist1 > dist2) {
		buildBase (&s1, intersec);
		mySemiArrow->base = s1;
		mySemiArrow->cross = s2;
	} else {
		buildBase (&s2, intersec);
		mySemiArrow->base = s2;
		mySemiArrow->cross = s1;
	}
	mySemiArrow->isValid = 1;	
}

inline void buildArrow (Segment3D base, Arrow3D* myArrow) {
	myArrow->start = base.start.position; // ya comprobamos en su momento que start y end son correctos, ya que será la dirección de la flecha
	myArrow->end = base.end.position;
}

inline void fillArrowElement (Arrow3D arrow) {
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));
	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;

	// Si estamos aquí es porque acabamos de ver un paralelogramo, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima

	myActualElement->type = 3; // elemento flecha
	myActualElement->arrow = arrow;
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline int areTheSameArrow (Arrow3D arrow1, Arrow3D arrow2) {
	return (distanceBetweenPoints (arrow1.end, arrow2.end) < MAX_GAP_ARROWS);
}

inline void insertArrowOnAttentionSystem (Arrow3D arrow) {
	fillArrowElement (arrow);
	isMergedElementAndInsert ();
}

inline void hypothesizeArrows () {
	int twice, found, i, j, k=0;
	float dist1, dist2, ang;
	Parallelogram3D square;
	SemiArrow3D mySemiArrow;
	Arrow3D myArrow;
	HPoint3Dinfo proy1, proy2;
	int ubi1, ubi2;
	float arrowDist1, arrowDist2;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) {
		if (groundSegments3D[i].isValid == 1) {
			dist1 = distanceBetweenPoints (groundSegments3D[i].start.position, groundSegments3D[i].end.position);
			if ((dist1<MAX_TAM_ARROW) && (dist1>MIN_TAM_ARROW)) {
				//twice = 0; // intersection counter
				j = i + 1;
				found = FALSE;
				while ((j < MAX_LINES_IN_MEMORY) && (!found)) {
					if (groundSegments3D[j].isValid == 1) {
						dist2 = distanceBetweenPoints (groundSegments3D[j].start.position, groundSegments3D[j].end.position);
						if ((dist2<MAX_TAM_ARROW) && (dist2>MIN_TAM_ARROW)) {
							if (((dist1<MAX_TAM_BASE) && (dist2>MIN_TAM_ARROW)) || ((dist2<MAX_TAM_BASE) && (dist1>MIN_TAM_ARROW))) {
								printf ("hypothesizeArrows: Arrow LOOP, twice = %i, dist1 = %0.2f, dist2 = %0.2f\n", twice, dist1, dist2);
								if (haveACommonVertex(groundSegments3D[i],groundSegments3D[j],&square)) {
									ang = angleBetweenSegments(groundSegments3D[i],groundSegments3D[j]);
									printf ("hypothesizeArrows: they've a common vertex :), their angle is %0.2f\n", (ang*RADTODEG));
									if((ang > (DEGTORAD*30))&&(ang < (DEGTORAD*60))){
										found = TRUE;
										// square.p1 será el punto de intersección entre base y aspa, nos dirá la dirección de la futura flecha
										buildSemiArrow (groundSegments3D[i], groundSegments3D[j], square.p1.position, dist1, dist2, &mySemiArrow);
										buildArrow (mySemiArrow.base, &myArrow);
										insertArrowOnAttentionSystem (myArrow);
										//semiArrows3D[k]=mySemiArrow;
										//k++;
										printf ("Semi Arrow Found!\n");
									}
								}
							}
						}
					}
					j ++;
				}
			}
		}
	}
/*
	// cuando salgamos de comprobar todos con todos tendremos un array con semi-flechitas, comprobemos las verdaderas flechas :)
	for (i = 0; i < k; i++) {
		if (semiArrows3D[i].isValid == 1) {
			found=0;
			j = i + 1;

			while((j < k) && (!found))	{
				if (semiArrows3D[j].isValid == 1) {
					if (areTheSameSegment (semiArrows3D[i].base, semiArrows3D[j].base)) {
						found = 1;
					} else {
						// Check every segment in memory, with segment. Calculate intersection point between segment i and segment.start perpendicular
						ubi1 = distancePointLine (semiArrows3D[i].base.start, semiArrows3D[j].base, &proy1, &arrowDist1);
						ubi2 = distancePointLine (semiArrows3D[i].base.end, semiArrows3D[j].base, &proy2, &arrowDist2);

						found = overlapping (i, proy1, proy2, ubi1, ubi2, arrowDist1, arrowDist2); // If they're parallel lines -> we finish checking
					}
					if (found) { // los anulamos pare evitar futuras comprobaciones e INSERTAMOS EN SISTEMA DE ATENCIÓN
						printf ("HAY FLECHAS\n");
						semiArrows3D[i].isValid = 0;
						semiArrows3D[j].isValid = 0;
						buildArrow (semiArrows3D[i].base, &myArrow);
						insertArrowOnAttentionSystem (myArrow);
					}
				}
				j ++;
			}
		}
	}
*/
}

inline void hypothesizeParallelograms () {
	int counter, squareFound, i, j;
	float dist1, dist2, ang;
	Parallelogram3D square;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) {
		if (groundSegments3D[i].isValid == 1) {
			counter = 0; // intersection counter

			squareFound = 0;
			j = i + 1;
			while ((j < MAX_LINES_IN_MEMORY) && (!squareFound)) {
				if (groundSegments3D[j].isValid == 1) {
					if (haveACommonVertex(groundSegments3D[i],groundSegments3D[j],&square)){
						dist1 = distanceBetweenPoints (groundSegments3D[i].start.position, groundSegments3D[i].end.position);
						dist2 = distanceBetweenPoints (groundSegments3D[j].start.position, groundSegments3D[j].end.position);
						ang = angleBetweenSegments(groundSegments3D[i],groundSegments3D[j]);

						if(((dist1<MAX_TAM_SEG)&&(dist2<MAX_TAM_SEG))&&((ang > (DEGTORAD*80))&&(ang < (DEGTORAD*100)))){
							getLastPoint (&square);
							insertParallelogramOnAttentionSystem (square);
							squareFound = 1;
							//printf ("Square Found\n");
						}
					}
				}
				j ++;
			}
		}
	}
}

inline void thinkObjetive () {
	//printf ("Trata de arrancarlo por dios!\n");
}

inline void getMaxSaliency () {
	struct elementStruct *p, *q;
	int found = FALSE;
	float maxSaliency = -9999.;
	p = myElements;
	q = NULL;

	// Recorremos la lista buscando al de mayor saliencia
	while (p != NULL) {
	/*	if (p->next != NULL)
			printf ("getMaxSaliency: myElements[%i]. Pos: [%.2f, %.2f], live = %.1f, sal = %.1f p es %p y sig es %p\n", elementsStructCounter-1, p->longitude, p->latitude, p->liveliness, p->saliency, p, p->next);
		else
			printf ("getMaxSaliency: myElements[%i]. Pos: [%.2f, %.2f], live = %.1f, sal = %.1f p es %p y sig es NULL\n", elementsStructCounter-1, p->longitude, p->latitude, p->liveliness, p->saliency, p);
*/
		if (p->saliency > maxSaliency) {
			maxSaliency = p->saliency;
			q = p; // me lo guardo
		}
		p = p->next; // preparamos a p para la siguiente vuelta :)
	}

	if (q != NULL) { // nos aseguramos que q tenga algo, para que no casque
		myMaxSaliency = q; // q tendrá al elemento con mayor saliencia
		myMaxSaliency->saliency = MIN_SALIENCY;
		myMaxSaliency->liveliness = MAX_LIVELINESS;
		myMaxSaliency->lastInstant = actualInstant;
		if (myMaxSaliency->type != 0) // tenemos un elemento que no es virtual
			point2angle (); // obtenemos su posición en el mundo y las posiciones (pan, tilt) para el cuello mecánico

		printf ("getMaxSaliency: choosen new maxSaliency element (%i)-> myElements[%i]. Pos: [%.2f, %.2f], live = %.1f, sal = %.1f\n", myMaxSaliency->type, elementsStructCounter-1, myMaxSaliency->latitude, myMaxSaliency->longitude, myMaxSaliency->liveliness, myMaxSaliency->saliency);
	} else myMaxSaliency = NULL; // no tenemos candidato ahora mismo
}

inline void searchObjetive () {
	float aux_x, aux_y, speed_x, speed_y, latit, longit;
	
	if (!completedSearch) {
		aux_y = myMaxSaliency->latitude; // + (*mytilt_angle);
		aux_x = myMaxSaliency->longitude; // + (*mypan_angle);
		printf ("Moving pan-tilt to [%f, %f]...\n", aux_y, aux_x);

	  speed_x = 8*(VEL_MAX_PAN-((POS_MAX_PAN-DEG_TO_ENCOD*(abs(aux_x)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN))));
	  speed_y = 8*(VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(aux_y)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT))));

		if ((*mylongitude_speed) < VEL_MIN_PAN)
			*mylongitude_speed = VEL_MIN_PAN*4;
		else *mylongitude_speed = speed_x;
		if ((*mylatitude_speed) < VEL_MIN_TILT)
			*mylatitude_speed = VEL_MIN_TILT*4;
		else *mylatitude_speed = speed_y;

		if (aux_x > MAX_PAN_ANGLE)
			*mylongitude = MAX_PAN_ANGLE;
		else if (aux_x < MIN_PAN_ANGLE)
			*mylongitude = MIN_PAN_ANGLE;
		else
			*mylongitude = aux_x;

		if (aux_y > MAX_TILT_ANGLE)
			*mylatitude = MAX_TILT_ANGLE;
		else if (aux_y < MIN_TILT_ANGLE)
			*mylatitude = MIN_TILT_ANGLE;
		else
			*mylatitude = aux_y;

		if (((*mypan_angle) < ((*mylongitude) + 1.)) && ((*mypan_angle) > ((*mylongitude) - 1.)) && ((*mytilt_angle) < ((*mylatitude) + 1.)) && ((*mytilt_angle) > ((*mylatitude) - 1.))) {
			completedSearch = TRUE;
		}	else {
			completedSearch = FALSE;
		}
	}
}

inline void facesHaarDetection (IplImage *img) {
  static unsigned long int lastClock = 0;
  cvClearMemStorage( storageTmp );
	int i;

  if( cascade && img )
  {
    //detectionTimeAux = (double) cvGetTickCount();
    facesTmp = cvHaarDetectObjects( img, cascade, storageTmp,
                                  1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                  cvSize(MIN_EXP_WIDTH, MIN_EXP_HEIGHT) );
		//--------------------------------------------------------------------------
		// A little of general culture :)
		// cvGetTickCount(void): Returns the number of ticks.
		// cvGetTickFrequency(void): Returns the number of ticks per microsecond.
		// Thus, the quotient of cvGetTickCount() and cvGetTickFrequency() will give
		// the number of microseconds starting from the platform-dependent event.
		//--------------------------------------------------------------------------

    //detectionTimeAux = (double) cvGetTickCount() - detectionTimeAux; // ticks to detect face...
    //detectionTimeAux =  detectionTimeAux/((double)cvGetTickFrequency()*1000.); // ...translated into seconds

    //pthread_mutex_lock(&iFollowFaceMutex);
    //facesClock = lastClock;
    //lastClock++;
    //pthread_mutex_unlock(&iFollowFaceMutex);
  }
}

void detectFaces () {
	static unsigned char **mycolorATmp = NULL;
	struct faceStruct *p;

	if (mycolorA != mycolorATmp) { // Source image has been changed
		img = cvMat(240, 320, CV_8UC3, *mycolorA);
		if (imgLocal != NULL)
			cvReleaseImage(&imgLocal);

		imgLocal = cvCreateImage(cvSize(320, 240), IPL_DEPTH_8U, 1);
		mycolorATmp = mycolorA;
	}
	if (imgLocal != NULL) {
		cvCvtColor(&img, imgLocal, CV_BGR2GRAY);
		facesHaarDetection (imgLocal);
	}
}

inline void faceProjection (HPoint2D point2D) {
	myCamera = &robotCamera;

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1;

	myPoint2D.y = point2D.x;
	myPoint2D.x = 240.-1.-point2D.y;
	myPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myPoint3D, myPoint2D, *myCamera);

	linePlaneIntersection (myPoint3D, cameraPos3D);
}

inline void fillFaceElement (Face3D face) {
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));
	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;

	// Si estamos aquí es porque acabamos de ver una cara, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima

	myActualElement->type = 2; // elemento face
	myActualElement->face = face;
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline void insertFaceOnAttentionSystem (Face3D face) {
	fillFaceElement (face);
	isMergedElementAndInsert ();
}

inline int areTheSameFace (Face3D face1, Face3D face2) {
	return (distanceBetweenPoints (face1.center.position, face2.center.position) < MAX_GAP_FACES);
}

inline int mergeFace (Face3D face) {
	int i=0,found=0;
	
	while(i < MAX_FACES_IN_MEMORY && !found)	{
		if ((groundFaces3D[i].isValid == 1) && areTheSameFace (groundFaces3D[i], face))
			found = 1;

		i ++;
	}

	return found;
}

inline void storeFace3D (Face3D face) { // el punto de intersección está en intersectionPoint
	int isMerge = 0, pos;

	if (numFaces == 0)	{
		groundFaces3D[numFaces] = face;
		groundFaces3D[numFaces].isValid = 1;
		numFaces ++;
	} else { // We already have a parallelogram, so we've to try to overlap it or store it
		isMerge = mergeFace (face);

		if (!isMerge) { // check insertion position
			if (numFaces < MAX_FACES_IN_MEMORY)	{
				pos = numFaces;
				numFaces ++;
			} else {
				pos = incNumFaces;
				incNumFaces++;
				if (incNumFaces == MAX_FACES_IN_MEMORY) {
					incNumFaces=0;
				}
			}
			groundFaces3D[numFaces] = face;
			groundFaces3D[numFaces].isValid = 1;
		} else printf ("Face merged TOTAL FACES = %i\n", numFaces);
	}
}

inline void getAndStoreFaces () {
	CvRect r;
	int i;
	Face3D face;
	face.isValid = TRUE; // las caras que sacaremos serán todas válidas
	detectFaces (); // face haar detection -> facesTmp
	if (facesTmp->total <= 0) {
		faceCenter.x = 9999;
		faceCenter.y = 9999;
	} else {
		printf ("HAY CARASSSSSSS\n");
		for( i = 0; i < (facesTmp ? facesTmp->total : 0); i++ ) {
			r = *(CvRect*)cvGetSeqElem(facesTmp, i);
			faceCenter.x = cvRound((r.x + r.width*0.5));
			faceCenter.y = cvRound((r.y + r.height*0.5));
			printf ("Cara detectada en [%f, %f]\n", faceCenter.x, faceCenter.y);

			if (mycolorA!= NULL) {
			  imageAgrounded->image = (char *) *mycolorA; // asignamos directamente lo que viene de mycolorA
				drawCross(imageAgrounded, faceCenter, 20, 0, 255, 0); // pinta el foco de la cámara
				prepare2draw(imageAgrounded, groundedImageRGB);
			}

			faceProjection (faceCenter); // en intersectionPoint estará el punto retroproyectado en 3D
			face.center.position = intersectionPoint;
			// TODO: debería funcionar sin storeFace3D (face);
			insertFaceOnAttentionSystem (face);
		}
		faceCenter.x = 9999;
		faceCenter.y = 9999; // para la siguiente vuelta
	}
}

inline void analizeImage () {
	// #1º# Obtención de posición exacta de la cámara de donde procede la imagen en curso
	getCameraWorldPos ();
	printf ("analizeImage: getCameraWorldPos()... done!\n");

	// #2º# Obtención de bordes2D (canny) -> projectMemory2D sobre imagen actual (borrado de existentes) ->
	// -> segments2D (hough) -> groundSegments2D (merge) -> groundSegments3D (merge)
	processImage ();
	printf ("analizeImage: processImage()... done!\n");

	// #3º# Hipotetizar sobre groundSegments3D -> sistema de atención
	hypothesizeParallelograms ();
	printf ("analizeImage: hypothesizeParallelograms()... done!\n");

	// #4º# Hipotetizar sobre groundSegments3D -> sistema de atención
	hypothesizeArrows ();
	printf ("analizeImage: hypothesizeArrows()... done!\n");

	// #5º# Obtención de caras2D -> proyección a groundFaces3D (merge) -> inserción en sistema atentivo
	getAndStoreFaces (); // Detectamos si hay objetos interesantes en la imagen actual
	printf ("analizeImage: getAndStoreFaces()... done!\n");

	// es necesario validate ()?? -> sistema de atención lo resuelve?
	analizedImage = TRUE;
	printf ("analizeImage: image anlized correctly!\n");
}

inline float distancia (imagePoint punto1, imagePoint punto2) {
  return (sqrt (pow((punto2.x-punto1.x), 2.)+pow((punto2.y-punto1.y), 2.)));
}

inline double pasar_radianes_grados (double radianes) {
  return ((radianes * 180.)/PI);
}

inline double pasar_grados_radianes (double grados) {
  return ((grados * PI)/180.);
}

inline scenePoint coordenadas2PT (int x, int y) {
	imagePoint punto1, punto2, punto3, punto_origen;
	t_vector vector1, vector2;
	float dist, ang1, ang2;
	scenePoint c_angulares;

	punto1.x = TILT_MIN; punto1.y = RADIO_MAX;
	punto2.x = TILT_MAX; punto2.y = RADIO_MIN;
	vector1.x = punto2.x - punto1.x;
	vector1.y = punto2.y - punto1.y;

	punto3.x = x; punto3.y = y;
	punto_origen.x = CENTRO_X; punto_origen.y = CENTRO_Y;
	dist =  distancia (punto3, punto_origen);

	c_angulares.tilt = (((dist - punto1.y)*(vector1.x))/(vector1.y))+punto1.x;
	  
	vector1.x = CENTRO_X - x;
	vector1.y = CENTRO_Y - y;
	vector2.x = CENTRO_X - ANCHO_ESCENA_COMPUESTA / 2;
	vector2.y = CENTRO_Y - 0;
	ang1 = atan2 (vector1.x, vector1.y);
	ang2 = atan2 (vector2.x, vector2.y);

	c_angulares.pan = -1*(pasar_radianes_grados (ang2-ang1));

	return c_angulares;
}

inline void fillVirtualElement () {
	int signoP, signoT;
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));

	if (randomPosition == TRUE) {
	  srand(time(0));
		signoP = random()%2;
		signoT = random()%2;
		if (signoP==1)
			myActualElement->longitude = -random()%159;
		else
			myActualElement->longitude = random()%159;
/*
		if (signoT==1)
			myActualElement->latitude = -random()%31;
		else
			myActualElement->latitude = random()%31;
*/
		myActualElement->latitude = tiltAngle;

		randomPosition = FALSE;
	} else { // toca posicionarse en la siguiente hubicación sistemática
		myActualElement->longitude = longitudeScenePositions[nextLongitude];
		//myActualElement->latitude = latitudeScenePositions[nextLatitude];
		myActualElement->latitude = tiltAngle;

		if (nextLongitude == LONGITUDE_NUM_POSITIONS-1) {
			nextLongitude = 0;
/*			if (nextLatitude == 3)
				nextLatitude = 0;
			else nextLatitude ++;*/
		} else nextLongitude ++;

		//randomPosition = TRUE;
	}

	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;
	// Si estamos aquí es porque acabamos de ver un paralelograma, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima
	myActualElement->type = 0; // elemento paralelogramo
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline void insertVirtualElement () {
	fillVirtualElement ();
	isMergedElementAndInsert ();
}

inline void updateElements (double instant) {
	struct elementStruct *p, *q, *r;
	int found = FALSE;

	p = myElements;

	// 1º - Recorremos la lista eliminando todos los que se les ha acabado la vida :(
	q = p;
	while (p != NULL) {
		//printf ("updateElements: Search Loop\n");
		//printf ("myElements[%i]. Pos: [%.2f, %.2f], time in live = %f, live = %.1f, sal = %.1f\n", elementsStructCounter, p->longitude, p->latitude, (instant - p->lastInstant), p->liveliness, p->saliency);
		if ((instant - p->lastInstant) > TIME_TO_DEAD) { // esta antigua elemento será borrada de memoria
			// borrado en otro sitio que no sea el principio
			printf ("updateElements: Deleting old element of memory...\n");
			q = p->next; // nos lo guardamos

			if (p == myMaxSaliency)
				myMaxSaliency = NULL;
			if (p == myElements)
				myElements = myElements->next;

			free (p);
			elementsStructCounter --;
			p = q;
		} else {
			p->liveliness -= LIVELINESS_DECREMENT;
			p->saliency += SALIENCY_INCREMENT;
			p = p->next; // preparamos a p para la siguiente vuelta :)
		}
	}

	p = myElements;

	do {
		if (p == myMaxSaliency) {
			if (p == myElements) { 
				q = myElements;
				found = TRUE;
			}
		} else {
			q = p;
			p = p->next;
		}
	}	while ((p != NULL) && (p != myMaxSaliency));

	// si found = true -> q tendrá el valor de myElements
  // si found = false -> q tendrá el valor anterior de p

  if (p != NULL) { // p contendrá el valor de myMaxSaliency
		//printf ("updateElements: %p myElements[%i]. Pos: [%.2f, %.2f], type = %i, time in live = %.1f, live = %.1f, sal = %.1f\n", p, elementsStructCounter-1, p->longitude, p->latitude, p->type, (instant - p->lastInstant), p->liveliness, p->saliency);
		if ((p->type != 0) && ((instant - p->lastInstant) > FOLLOW_TIME)) { // si es una elemento real y se le ha pasado el tiempo de seguimiento
			printf ("updateElements: FOLLOW TIME expired\n");
			getMaxSaliency (); // cambiamos de maxSaliency

			if (myMaxSaliency != NULL)
				myMaxSaliency->lastInstant = instant;

		} else if ((p->type == 0) && (analizedImage)) { // es una elemento virtual ya analizada y que no contenía ninguna elemento, la borramos
			printf ("updateElements: Virtual element analized --> Deleting...\n");

			if (found) // q contendrá el valor de myElements
				myElements = myElements->next;
			else
				q->next = p->next;

			free (p);
			elementsStructCounter --;
			getMaxSaliency ();
			if (myMaxSaliency != NULL)
				myMaxSaliency->lastInstant = instant;

		} else if ((p->type != 0) && (p->liveliness <= LIVELINESS_TO_DEAD)) { // se ha muerto, es posible, ya que si se deja de ver... nos lo cargamos :P
			printf ("updateElements: This element has dead because of its liveliness\n");

			if (found) // q contendrá el valor de myElements
				myElements = myElements->next;
			else
				q->next = p->next;

			free (p);
			elementsStructCounter --;
			getMaxSaliency ();
			if (myMaxSaliency != NULL)
				myMaxSaliency->lastInstant = instant;

		} else if (p->type != 0) { // si es una elemento real, que no le ha pasado el tiempo, vamos actualizando su vida
			p->saliency = MIN_SALIENCY;
			p->liveliness += LIVELINESS_INCREMENT;
			if (p->liveliness > MAX_LIVELINESS) // para evitar saturación
				p->liveliness = MAX_LIVELINESS;
		}
	}

	if (elementsStructCounter != 0)
		timeToForcedSearch = elementsStructCounter * TIME_TO_FORCED_SEARCH;

	analizedImage = FALSE;
}

inline void loadCascade () {
	cascade = (CvHaarClassifierCascade*) cvLoad (cascade_name, 0, 0, 0 );
	if (!cascade) {
		fprintf(stderr, "visualSonar: Could not load classifier cascade\n");
		jdeshutdown(1);
	}
	storage = cvCreateMemStorage(0);
	storageTmp = cvCreateMemStorage(0);
}

inline int chooseState () {
	if ((myActualState == think) && ((elementsStructCounter == 0) || ((myMaxSaliency != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)))) {
		printf ("chooseState: 1ª opcion\n");
		if ((myMaxSaliency != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)) {
			isForcedSearch = TRUE; // para que a la siguiente vuelta tengamos el flag activado y hagamos búsqueda
			timeForced = actualInstant;
		}
		printf ("FORCED SEARCH\n");

		if ((myMaxSaliency != NULL) && (myMaxSaliency->type == 0)) { // elemento virtual
			myElements = myElements->next;
			free (myMaxSaliency);
			myMaxSaliency = NULL;
			elementsStructCounter --;
		}

		insertVirtualElement (); // generaremos un destino si no hay destino o si, aun habiendo hay una búsqueda forzada
		getMaxSaliency ();
	}

	else if (((myActualState == think) && ((elementsStructCounter > 0) && ((TRUE) && (((myMaxSaliency != NULL) && (myMaxSaliency->type == 0)) || (((myMaxSaliency != NULL) && (myMaxSaliency->type != 0)) && ((myMaxSaliency->liveliness) > LIVELINESS_TO_DEAD)))))) || (isForcedSearch == TRUE)) {
		printf ("chooseState: 2ª opcion\n");
		myActualState = search;
		isForcedSearch = FALSE;
	}

	else if ((myActualState == search) && (completedSearch)) {
		printf ("chooseState: 3ª opcion\n");
		myActualState = analizeSearch;
		completedSearch = FALSE;
	}

	else if (myActualState == analizeSearch) {
		printf ("chooseState: 4ª opcion\n");
		myActualState = think;
	}
}

inline void runState () {
	switch (myActualState) {
		case think: printf ("runState: state think\n"); thinkObjetive ();
		break;
		case search: printf ("runState: state search\n"); searchObjetive ();
		break;
		case analizeSearch: printf ("runState: state analizeSearch\n"); analizeImage ();
		break;
	}
}

inline void visualSonar_iteration() {
	static char d = 0;
	speedcounter(visualSonar_id);
	actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	
// TODO:	pthread_mutex_lock(&main_mutex);

	if (d == 0) loadCascade ();

	if (!completedMovement) { // agachamos la cámara inicialmente
		moveInitialTilt ();
	} else {
		updateElements (actualInstant); // en función del tiempo, actualizaremos: vida, saliencia y quitar muertos
		chooseState (); // actualizamos (en su caso) myActualState
		runState (); // ejecutamos según el estado que sea
	}

//	pthread_mutex_unlock(&main_mutex);
}

/*Importar símbolos*/
inline void visualSonar_imports() {
	/* importar colorA */
	mycolorA = (char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");
	mycolorAwidth	= (int*) myimport("colorA", "width");
	mycolorAheight = (int*) myimport("colorA", "height");

	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL){
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}

	/* importamos motors de pantilt */
  mylongitude=myimport("ptmotors", "longitude");
  mylatitude=myimport ("ptmotors", "latitude");
  mylongitude_speed=myimport("ptmotors", "longitude_speed");
  mylatitude_speed=myimport("ptmotors","latitude_speed");
  
  max_pan=myimport("ptmotors", "max_longitude");
  max_tilt=myimport("ptmotors", "max_latitude");
  min_pan=myimport("ptmotors", "min_longitude");
  min_tilt=myimport("ptmotors", "min_latitude");

	ptmotorsrun=myimport("ptmotors","run");
	ptmotorsstop=myimport("ptmotors","stop");

  /* importamos encoders de pantilt */
  mypan_angle=myimport("ptencoders", "pan_angle");
  mytilt_angle=myimport("ptencoders", "tilt_angle");

  ptencodersrun=myimport("ptencoders", "run");
  ptencodersstop=myimport("ptencoders", "stop");

	/* importamos encoders y motores de la base */
	myencoders=(float *)myimport("encoders","jde_robot");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsrun=(runFn)myimport("motors","run");
  motorsstop=(stopFn)myimport("motors","stop");
}

inline void visualSonar_stop()
{
	colorAstop();

	RGB2HSV_destroyTable();

	pthread_mutex_lock(&(all[visualSonar_id].mymutex));
	put_state(visualSonar_id,slept);
	printf("visualSonar: off\n");
	pthread_mutex_unlock(&(all[visualSonar_id].mymutex));

	free(imageA);
	free(imageAbordered);
	free(imageAfiltered);
	free(imageAgrounded);

	remove_image(originalImageRGB);
	remove_image(borderImageRGB);
	remove_image(filteredImageRGB);
	remove_image(groundedImageRGB);
}


inline void visualSonar_run(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[visualSonar_id].mymutex)); // CERROJO -- LOCK
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[visualSonar_id].children[i]=FALSE;
 
  all[visualSonar_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) visualSonar_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {visualSonar_brothers[i]=brothers[i];i++;}
    }
  visualSonar_callforarbitration=fn;
  put_state(visualSonar_id,notready);
  printf("visualSonar: on\n");
  visualSonar_imports();
  /* Wake up drivers schemas */	
  colorArun(visualSonar_id, NULL, NULL);

  pthread_cond_signal(&(all[visualSonar_id].condition));
  pthread_mutex_unlock(&(all[visualSonar_id].mymutex)); // CERROJO -- UNLOCK
}

inline void *visualSonar_thread(void *not_used) {
	struct timeval a,b;
	long n=0; /* iteration */
	long next,bb,aa;

	for(;;)	{
		pthread_mutex_lock(&(all[visualSonar_id].mymutex));

		if (all[visualSonar_id].state==slept) {
			pthread_cond_wait(&(all[visualSonar_id].condition),&(all[visualSonar_id].mymutex));
			pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
		}
		else {
			if (all[visualSonar_id].state==notready) /* check preconditions. For now, preconditions are always satisfied*/
				put_state(visualSonar_id,ready);

			if (all[visualSonar_id].state==ready) { /* check brothers and arbitrate. For now this is the only winner */
				put_state(visualSonar_id,winner);
  			*mylongitude_speed= 0.0;
  			*mylatitude_speed= 0.0;
	      all[visualSonar_id].children[(*(int *)myimport("ptencoders","id"))]=TRUE;
	      all[visualSonar_id].children[(*(int *)myimport("ptmotors","id"))]=TRUE;
	      all[visualSonar_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      all[visualSonar_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      ptencodersrun(visualSonar_id,NULL,NULL);
	      ptmotorsrun(visualSonar_id,NULL,NULL);
	      encodersrun(visualSonar_id,NULL,NULL);
	      motorsrun(visualSonar_id,NULL,NULL);

				gettimeofday(&a,NULL);
				aa=a.tv_sec*1000000+a.tv_usec;
				n=0;
			}

			if (all[visualSonar_id].state==winner) {
				/* I'm the winner and must execute my iteration */
				pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
				/* gettimeofday(&a,NULL); */
				n++;
				visualSonar_iteration();
				gettimeofday(&b,NULL);
				bb=b.tv_sec*1000000+b.tv_usec;
				next=aa+(n+1)*(long)visualSonar_cycle*1000-bb;

				if (next>5000) {
					usleep(next-5000); /* discounts 5ms taken by calling usleep itself, on average */
				}	else {
					usleep(25000); /* If iteration takes a long time, sleep 25 ms to avoid overload */
				}
			}	else { /* just let this iteration go away. overhead time negligible */
				pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
				usleep(visualSonar_cycle*1000);
			}
		}
	}
}

inline void visualSonar_terminate()
{
  pthread_mutex_lock(&(all[visualSonar_id].mymutex));
  visualSonar_stop();  
  pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
  sleep(2);
  //fl_free_form(fd_visualSonargui->visualSonargui);
//  free(imagenA_buf);
//  free(imagenFiltrada_buf);
//  free(imagenSuelo_buf);
}

inline inline void createConfigFile () {
	FILE *salida;
	salida = fopen (configFile,"w");

	if (salida == NULL) {
		fprintf(stderr,"Parser: I can't found file %s\n", configFile); exit(-1);
	}

	printf("Building config file %s...\n", configFile);
	fprintf(salida,"dimension = %d\n", 4000);
	fprintf(salida,"resolucion = 50\n");
	fprintf(salida,"tipo = ec_diferencial\n");
	fprintf(salida,"ec_diferencial_speed = 1\n");
	fprintf(salida,"paso_tiempo = nulo\n");
	fprintf(salida,"cell_angles = 0\n");
	fprintf(salida,"mayoria_saturacion = 30\n");
	fprintf(salida,"mayoria_ruido = 10\n");
	fprintf(salida,"long_historia = 0\n");
	fprintf(salida,"\n");
	fprintf(salida,"sonar_filtra = independientes\n");
	fprintf(salida,"sonar_geometria = cono_denso\n");
	fprintf(salida,"sonar_apertura = 20.\n");
	fprintf(salida,"sonar_noobstacle = 3000.\n");
	fprintf(salida,"sonar_radialerror = 10.\n");
	fprintf(salida,"sonar_fdistancia = lineal\n");
	fprintf(salida,"sonar_residuo = 0.05\n");
	fprintf(salida,"sonar_o = 0.4\n");
	fprintf(salida,"sonar_e = -0.5\n");
	fprintf(salida,"sonar_mind = 700\n");
	fprintf(salida,"sonar_maxd = 1100.\n");
	fprintf(salida,"\n");
	fprintf(salida,"robot_geometria = cilindro\n");
	fprintf(salida,"robot_radio = 248.\n");
	fprintf(salida,"robot_e = -0.8\n");
	fprintf(salida,"\n");
	fprintf(salida,"laser_geometria = cono_denso\n");
	fprintf(salida,"laser_apertura = 0.5\n");
	fprintf(salida,"laser_muestras = 90\n");
	fprintf(salida,"laser_noobstacle = 8000.\n");
	fprintf(salida,"laser_o = 1\n");
	fprintf(salida,"laser_e = -0.7\n");

	fclose(salida);
}

/*Exportar símbolos*/
inline void visualSonar_exports()
{
   myexport("visualSonar","cycle",&visualSonar_cycle);
   myexport("visualSonar","resume",(void *)visualSonar_run);
   myexport("visualSonar","suspend",(void *)visualSonar_stop);
}

inline void visualSonar_init(char *configfile)
{
  int i=0;
  pthread_mutex_lock(&(all[visualSonar_id].mymutex)); // CERROJO -- LOCK

  printf("visualSonar schema started up\n");
  visualSonar_exports();
  put_state(visualSonar_id,slept);
  pthread_create(&(all[visualSonar_id].mythread),NULL,visualSonar_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[visualSonar_id].mymutex)); // CERROJO -- UNLOCK

	// Valores iniciales para la cámara virtual con la que observo la escena de puntos 3D en el suelo
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la cámara virtual con la que observo la escena
  virtualcam1.position.X=4000.;
  virtualcam1.position.Y=4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=0.;
  virtualcam1.foa.Y=0.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la cámara virtual con la que observo el entorno de visualSonar al robot
  virtualcam2.position.X=4000.;
  virtualcam2.position.Y=4000.;
  virtualcam2.position.Z=6000.;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=0.;
  virtualcam2.foa.Y=0.;
  virtualcam2.foa.Z=0.;
  virtualcam2.foa.H=1.;
  virtualcam2.roll=0.;

	// Valores para la cámara virtual de la esquina derecha del laboratorio (myColorA)
  roboticLabCam0.position.X=7875.000000;
  roboticLabCam0.position.Y=-514.000000;
  roboticLabCam0.position.Z=3000.000000;
  roboticLabCam0.position.H=1.;
  roboticLabCam0.foa.X=6264.000000;
  roboticLabCam0.foa.Y=785.0000000;
  roboticLabCam0.foa.Z=1950.00000;
  roboticLabCam0.foa.H=1.;
  roboticLabCam0.fdistx=405.399994;
	roboticLabCam0.fdisty=roboticLabCam0.fdistx;
	roboticLabCam0.skew=0.;
  roboticLabCam0.u0=142.600006;
  roboticLabCam0.v0=150.399994;
  roboticLabCam0.roll=3.107262;

	// Valores para la cámara virtual de la esquina derecha de enfrente del laboratorio (myColorB)
  roboticLabCam1.position.X=7875.000000;
  roboticLabCam1.position.Y=4749.000000;
  roboticLabCam1.position.Z=3000.000000;
  roboticLabCam1.position.H=1.;
  roboticLabCam1.foa.X=6264.000000;
  roboticLabCam1.foa.Y=3700.000000;
  roboticLabCam1.foa.Z=1950.00000;
  roboticLabCam1.foa.H=1.;
	roboticLabCam1.fdistx=405.399994;
	roboticLabCam1.fdisty=roboticLabCam1.fdistx;
	roboticLabCam1.skew=0.;
  roboticLabCam1.u0=142.600006;
  roboticLabCam1.v0=142.600006;
  roboticLabCam1.roll=3.007028;

	// Valores para la cámara virtual de la esquina izquierda de enfrente del laboratorio (myColorC)
  roboticLabCam2.position.X=50.000000;
  roboticLabCam2.position.Y=4471.000000;
  roboticLabCam2.position.Z=2955.000000;
  roboticLabCam2.position.H=1.;
  roboticLabCam2.foa.X=2432.399902;
  roboticLabCam2.foa.Y=2918.000000;
  roboticLabCam2.foa.Z=1300.000000;
  roboticLabCam2.foa.H=1.;
	roboticLabCam2.fdistx=405.399994;
	roboticLabCam2.fdisty=roboticLabCam2.fdistx;
	roboticLabCam2.skew=0.;
  roboticLabCam2.u0=142.600006;
  roboticLabCam2.v0=150.399994;
  roboticLabCam2.roll=3.107262;

	// Valores para la cámara virtual de la esquina izquierda del laboratorio (myColorD)
  roboticLabCam3.position.X=50.000000;
  roboticLabCam3.position.Y=100.000000;
  roboticLabCam3.position.Z=2955.000000;
  roboticLabCam3.position.H=1.;
  roboticLabCam3.foa.X=2160.000000;
  roboticLabCam3.foa.Y=1151.000000;
  roboticLabCam3.foa.Z=1550.000000;
  roboticLabCam3.foa.H=1.;
	roboticLabCam3.fdistx=405.399994;
	roboticLabCam3.fdisty=roboticLabCam3.fdistx;
	roboticLabCam3.skew=0.;
  roboticLabCam3.u0=142.600006;
  roboticLabCam3.v0=150.399994;
  roboticLabCam3.roll=2.956911;

	// Valores para la cámara REAL del ROBOT. Valores iniciales, que no se usan ya que luego los modificamos convenientemente :)
  robotCamera.position.X=116.407570;
  robotCamera.position.Y=400.009766;
  robotCamera.position.Z=449.958099;
  robotCamera.position.H=1.;
  robotCamera.foa.X=930.;
  robotCamera.foa.Y=330.;
  robotCamera.foa.Z=0.;
  robotCamera.foa.H=1.;
	robotCamera.fdistx=425.871368;
	robotCamera.fdisty=robotCamera.fdistx;
	robotCamera.skew=0.;
	robotCamera.u0=98.245613;
  robotCamera.v0=164.678360;
  robotCamera.roll=-0.019950;
	robotCamera.rows=SIFNTSC_ROWS;
	robotCamera.columns=SIFNTSC_COLUMNS;

  ceilLabCam.position.X=-400.;//260.000000;
  ceilLabCam.position.Y=400.000000;
  ceilLabCam.position.Z=800.;//400.000000;
  ceilLabCam.position.H=1.;
  ceilLabCam.foa.X=700.000000;
  ceilLabCam.foa.Y=400.000000;
  ceilLabCam.foa.Z=0.000000;
  ceilLabCam.foa.H=1.;
	ceilLabCam.fdistx=408.350891;
	ceilLabCam.fdisty=ceilLabCam.fdistx;
	ceilLabCam.skew=0.;
  ceilLabCam.u0=156.;
  ceilLabCam.v0=200.233917;
  ceilLabCam.roll=(PI/2)-0.059950;

	tiltAngle = -30.;//-(float) atan2 (480,600)*360./(2.*PI); // grados
	panAngle = 0.;//-1.5; // grados
  speed_y = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(tiltAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));
  speed_x = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(panAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));

	sliderPANTILT_BASE_HEIGHT = PANTILT_BASE_HEIGHT;
	sliderISIGHT_OPTICAL_CENTER = ISIGHT_OPTICAL_CENTER;
	sliderTILT_HEIGHT = TILT_HEIGHT;
	sliderCAMERA_TILT_HEIGHT = CAMERA_TILT_HEIGHT;
	sliderPANTILT_BASE_X = PANTILT_BASE_X;
	sliderThreshold = BORDER_THRESHOLD;

	update_camera_matrix (&roboticLabCam0);
	update_camera_matrix (&roboticLabCam1);
	update_camera_matrix (&roboticLabCam2);
	update_camera_matrix (&roboticLabCam3);
	update_camera_matrix (&robotCamera);
	update_camera_matrix (&ceilLabCam);

  actualCameraView = 5; // empezamos con la cámara del robot

	completedMovement = FALSE;

	actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	stopInstant = actualInstant;
	comeFromCenter = TRUE;
	last_full_movement = right;

  xcam = robotCamera.position.X;
  ycam = robotCamera.position.X;
  zcam = robotCamera.position.X;

//	pixel_camA.x = robotCamera.u0; // parameters extracted from Redo's calibrator gui
//	pixel_camA.y = robotCamera.v0;

	pixel_camA.x = SIFNTSC_COLUMNS/2; // parameters extracted from Redo's calibrator gui
	pixel_camA.y = SIFNTSC_ROWS/2;

	faceCenter.x = 9999;
	faceCenter.y = 9999;

	pantiltStill = FALSE;
	robotStill = TRUE;
	numFlashes = 0;
	numSegments = 0;
	numParallelograms = 0;
	incNumSegments = 0;
	incNumParallelograms = 0;
	numFaces = 0;
	incNumFaces = 0;

	myElements = NULL;
	myMaxSaliency = NULL;
 	myActualElement = NULL;
	elementsStructCounter = 0;
	systemInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	timeForced = systemInstant;
	completedSearch = FALSE;
	analizedImage = FALSE;
	completedTrack = FALSE;
	myActualState = think;
	timeToForcedSearch = TIME_TO_FORCED_SEARCH;
	isForcedSearch = FALSE;
	randomPosition = FALSE;
	nextLatitude = 0;
	nextLongitude = 0;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) // inicialización de segmentos en memoria
		groundSegments3D[i].isValid = 0;

	for (i = 0; i < MAX_PARALLELOGRAMS_IN_MEMORY; i++) // inicialización de segmentos en memoria
		groundParallelograms3D[i].isValid = 0;

	configFile = "./gradientPlanning.conf";
	//createConfigFile ();

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));

	image = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);
	contourImage = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);

  //imageA = create_image(*mycolorAwidth, *mycolorAheight, 3);
  imageA = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  //free(imageA->image); // vaciamos el espacio reservado para la imagen

  imageAfiltered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  imageAgrounded = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  imageAbordered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);

  originalImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  filteredImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  groundedImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  borderImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);

	RGB2HSV_init();
	RGB2HSV_createTable();
}

inline static int initFloorOGL (int w, int h) {
	glClearColor(0.f, 0.8f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	glOrtho (0, w, h, 0, 0, 1);

	return 0;
}

inline static int initvisualSonarOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
	GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat local_view[] = {0.0};

	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* With this, the pioneer appears correctly, but the cubes don't */
	glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv (GL_LIGHT0, GL_POSITION, position);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable (GL_LIGHT0);
	/*glEnable (GL_LIGHTING);*/

	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

inline void initWorldCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_visualSonarcanvas.x/360.;
	    lati=2*PI*mouse_on_visualSonarcanvas.y/360.;
	    r=500;
	    virtualcam2.position.X=radius*(virtualcam2.position.X)/radius_old;
	    virtualcam2.position.Y=radius*(virtualcam2.position.Y)/radius_old;
	    virtualcam2.position.Z=radius*(virtualcam2.position.Z)/radius_old;
	    if (centrado==0){
				//Si centrado = 0 se ha pulsado el boton de centrar. Si vale 1, no está pulsado.
				virtualcam2.foa.X=r*cos(lati)*cos(longi);
				virtualcam2.foa.Y=r*cos(lati)*sin(longi);
				virtualcam2.foa.Z=r*sin(lati);
			}
	  }
	  
	  if (cam_mode) {
			centrado = 0;

	    longi=2*PI*mouse_on_visualSonarcanvas.x/360.;
	    lati=2*PI*mouse_on_visualSonarcanvas.y/360.;
	    
	    virtualcam2.position.X=radius*cos(lati)*cos(longi);
	    virtualcam2.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam2.position.Z=radius*sin(lati);
	  }
		radius_old=radius;
	}

	initvisualSonarOGL(640,480);
	  
	/* Virtual camera */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 

	/* perspective projection. intrinsic parameters + frustrum */
	gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
	/* extrinsic parameters */
	if (actualCameraView == 0) // user camera view
	  gluLookAt(virtualcam2.position.X,virtualcam2.position.Y,virtualcam2.position.Z,
	          virtualcam2.foa.X,virtualcam2.foa.Y,virtualcam2.foa.Z,
	          0.,0.,1.);
	else if (actualCameraView == 1) // robotics lab camera 1
		gluLookAt(roboticLabCam0.position.X,roboticLabCam0.position.Y,roboticLabCam0.position.Z,roboticLabCam0.foa.X,roboticLabCam0.foa.Y,roboticLabCam0.foa.Z,0.,0.,1.);
	else if (actualCameraView == 2) // robotics lab camera 2
		gluLookAt(roboticLabCam1.position.X,roboticLabCam1.position.Y,roboticLabCam1.position.Z,roboticLabCam1.foa.X,roboticLabCam1.foa.Y,roboticLabCam1.foa.Z,0.,0.,1.);
	else if (actualCameraView == 3) // robotics lab camera 3
		gluLookAt(roboticLabCam2.position.X,roboticLabCam2.position.Y,roboticLabCam2.position.Z,roboticLabCam2.foa.X,roboticLabCam2.foa.Y,roboticLabCam2.foa.Z,0.,0.,1.);
	else if (actualCameraView == 4) // robotics lab camera 4
		gluLookAt(roboticLabCam3.position.X,roboticLabCam3.position.Y,roboticLabCam3.position.Z,roboticLabCam3.foa.X,roboticLabCam3.foa.Y,roboticLabCam3.foa.Z,0.,0.,1.);
	else if (actualCameraView == 5) // robotics lab ceil camera
		gluLookAt(ceilLabCam.position.X,ceilLabCam.position.Y,ceilLabCam.position.Z,ceilLabCam.foa.X,ceilLabCam.foa.Y,ceilLabCam.foa.Z,0.,0.,1.);
}

inline int loadWorldLines (FILE *myfile) {
  #define limit 256		
  char word1[limit],word2[limit],word3[limit],word4[limit],word5[limit];
  char word6[limit],word7[limit],word8[limit];
  char word[limit];
  int i=0;
  char buffer_file[limit];   

  buffer_file[0]=fgetc(myfile);
  if (feof(myfile)) return EOF;
  if (buffer_file[0]==(char)255) return EOF; 
  if (buffer_file[0]=='#') {while(fgetc(myfile)!='\n'); return 0;}
  if (buffer_file[0]==' ') {while(buffer_file[0]==' ') buffer_file[0]=fgetc(myfile);}
  if (buffer_file[0]=='\t') {while(buffer_file[0]=='\t') buffer_file[0]=fgetc(myfile);}

  /* Captures a line and then we will process it with sscanf checking that the last character is \n. We can't doit with fscanf because this function does not difference \n from blank space. */
  while((buffer_file[i]!='\n') && 
	(buffer_file[i] != (char)255) &&  
	(i<limit-1) ) {
    buffer_file[++i]=fgetc(myfile);
  }
  
  if (i >= limit-1) { 
    printf("%s...\n", buffer_file); 
    printf ("Line too long in config file!\n"); 
    exit(-1);
  }
  buffer_file[++i]='\0';


  if (sscanf(buffer_file,"%s",word)!=1) return 0; 
  /* return EOF; empty line*/
  else {
     if(strcmp(word,"worldline")==0){
				sscanf(buffer_file,"%s %s %s %s %s %s %s %s %s",word,word1,word2,word3,word4,word5,word6,word7,word8);
				myfloor[myfloor_lines*2+0].X=(float)atof(word1); myfloor[myfloor_lines*2+0].Y=(float)atof(word2); myfloor[myfloor_lines*2+0].Z=(float)atof(word3); myfloor[myfloor_lines*2+0].H=(float)atof(word4);
				myfloor[myfloor_lines*2+1].X=(float)atof(word5); myfloor[myfloor_lines*2+1].Y=(float)atof(word6); myfloor[myfloor_lines*2+1].Z=(float)atof(word7); myfloor[myfloor_lines*2+1].H=(float)atof(word8);
				myfloor_lines++;
     }
  }
  return 1;
}

// calculates intersection and checks for parallel lines.  
// also checks that the intersection point is actually on  
// the line segment p1-p2  
// return values:  0 -> lines are parallel
//                -1 -> lines aren't parallel and they don't intersect
//                 1 -> lines aren't parallel and they intersect in pt point
inline int findIntersection(HPoint2D p1, HPoint2D p2, HPoint2D p3, HPoint2D p4, HPoint2D* pt) {  
	float xD1, yD1, xD2, yD2, xD3, yD3;
	float dot, deg, len1, len2;
	float segmentLen1, segmentLen2;
	float ua, ub, div;

	// calculate differences  
	xD1 = p2.x-p1.x;  
	xD2 = p4.x-p3.x;  
	yD1 = p2.y-p1.y;  
	yD2 = p4.y-p3.y;  
	xD3 = p1.x-p3.x;  
	yD3 = p1.y-p3.y;    

	// calculate the lengths of the two lines  
	len1 = sqrt(xD1*xD1+yD1*yD1);  
	len2 = sqrt(xD2*xD2+yD2*yD2);  

	// calculate angle between the two lines.  
	dot = (xD1*xD2+yD1*yD2); // dot product  
	deg = dot/(len1*len2);  

	// if abs(angle)==1 then the lines are parallell,  
	// so no intersection is possible  
	if (abs(deg)==1) return 0;  

	// find intersection Pt between two lines  
	div=yD2*xD1-xD2*yD1;  
	ua=(xD2*yD3-yD2*xD3)/div;  
	ub=(xD1*yD3-yD1*xD3)/div;  
	pt->x=p1.x+ua*xD1;  
	pt->y=p1.y+ua*yD1;  

	// calculate the combined length of the two segments  
	// between Pt-p1 and Pt-p2  
	xD1=pt->x-p1.x;  
	xD2=pt->x-p2.x;  
	yD1=pt->y-p1.y;  
	yD2=pt->y-p2.y;  
	segmentLen1=sqrt(xD1*xD1+yD1*yD1)+sqrt(xD2*xD2+yD2*yD2);  

	// calculate the combined length of the two segments  
	// between Pt-p3 and Pt-p4  
	xD1=pt->x-p3.x;  
	xD2=pt->x-p4.x;  
	yD1=pt->y-p3.y;  
	yD2=pt->y-p4.y;  
	segmentLen2=sqrt(xD1*xD1+yD1*yD1)+sqrt(xD2*xD2+yD2*yD2);  

	// if the lengths of both sets of segments are the same as
	// the lenghts of the two lines the point is actually
	// on the line segment.

	// if the point isn’t on the line, return -1
	if(abs(len1-segmentLen1)>0.01 || abs(len2-segmentLen2)>0.01)
	return -1;

	// return the intersection is valid
	return 1;
}  

inline int areNearlyParallelLines (HPoint3Dinfo *Line1Start, HPoint3Dinfo *Line1End, HPoint3Dinfo *Line2Start, HPoint3Dinfo *Line2End) {
	// Let me take m = (y2-y1)/(x2-x1)
	int areParallel = 0;
	float m1, m2;
	float start1, start2, end1, end2;

	if (Line1End->position.X > Line1Start->position.X) {
		start1 = Line1Start->position.X;
		end1 = Line1End->position.X;
	} else {
		end1 = Line1Start->position.X;
		start1 = Line1End->position.X;
	}

	if (Line2End->position.X > Line2Start->position.X) {
		start2 = Line2Start->position.X;
		end2 = Line2End->position.X;
	} else {
		end2 = Line2Start->position.X;
		start2 = Line2End->position.X;
	}

	if ((Line1End->position.X - Line1Start->position.X) != 0.)
		m1 = ((Line1End->position.Y - Line1Start->position.Y)/(Line1End->position.X - Line1Start->position.X));
	else
		m1 = 999999.9;

	if ((Line2End->position.X - Line2Start->position.X) != 0.)
		m2 = ((Line2End->position.Y - Line2Start->position.Y)/(Line2End->position.X - Line2Start->position.X));
	else
		m2 = 999999.9;

	//printf ("[%f, %f] [%f, %f] m1 = %f, m2 = %f\n", Line1Start->X, Line1Start->Y, Line2Start->X, Line2Start->Y, m1, m2);

	//if (m1 == m2) {
	if ((!(m1 > 990000.9)) && (!(m2 > 990000.9))) {
		if ((((m1 > 0) && (m2 > 0)) || ((m1 < 0) && (m2 < 0))) && (abs (m1-m2) < 1.)) { // We consider parallel lines
		// both positive slopes or negative ones... and a bit threshold
			areParallel = 1;
		}
	}
	return areParallel;
}

inline void getLineEquation(int xini, int yini, int xfin, int yfin, float * A, float * B, float * C) {
	/*Line equation, producto vectorial de los puntos*/
	*A = (float)yini - (float)yfin; /*y1*z2 - z1*y2*/
	*B = (float)xfin - (float)xini; /*z1*x2 - x1*z2*/
	*C = (float)xini*(float)yfin - (float)yini*(float)xfin; /*x1*y2 - y1*x2*/
}

inline int drawline(IplImage* src, HPoint2D p1, HPoint2D p2) {
	/* it takes care of important features */
	/* before/behind the focal plane, inside/outside the image frame */

	CvPoint pt1, pt2;
	HPoint2D gooda,goodb;

	//printf ("p1 = [%f, %f] y p2 = [%f, %f]\n", p1.x, p1.y, p2.x, p2.y);

	if(displayline(p1,p2,&gooda,&goodb,robotCamera)==1) {
		/*Pasamos de coordenadas opticas a pixels*/
		pt1.x=(int)gooda.y;
		pt1.y=(SIFNTSC_ROWS-1)-(int)gooda.x;
		pt2.x=(int)goodb.y;
		pt2.y=(SIFNTSC_ROWS-1)-(int)goodb.x;
		
		cvLine(src, pt1, pt2, CV_RGB(0,0,255), 1, 8, 0);

		return 1;
	}

	return 0;
}

inline void drawMyLinesOnImage (IplImage *src) {
	int i;
	HPoint2D a,b;

  /* jmvega's lines in camera image */
  for(i=0;i<myfloor_lines;i++){
		//printf ("myfloor[%i]=[%f, %f, %f, %f]\n", i, myfloor[i*2+0].X, myfloor[i*2+0].Y, myfloor[i*2+0].Z, myfloor[i*2+0].H);
    project(myfloor[i*2+0],&a,robotCamera);
    project(myfloor[i*2+1],&b,robotCamera);
		//printf ("myfloor3D[%i]=[%f, %f, %f, %f], myfloor3D[i+1]=[%f, %f, %f, %f], a2D = [%f, %f], b2D = [%f, %f]\n", i, myfloor[i*2+0].X, myfloor[i*2+0].Y, myfloor[i*2+0].Z, myfloor[i*2+0].H, myfloor[i*2+1].X, myfloor[i*2+1].Y, myfloor[i*2+1].Z, myfloor[i*2+1].H, a.x, a.y, b.x, b.y);
    drawline(src,a,b);
  }
}

inline void drawGroundLines () {
	int i;

  glLineWidth(1.f);
	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) {
		if (groundSegments3D[i].isValid == 1) {
			if (groundSegments3D[i].idColor == 1)
				glColor3f(0.0f, 0.0f, 1.0f);
			else if (groundSegments3D[i].idColor == 2)
				glColor3f(0.0f, 0.5f, 0.0f);
			else if (groundSegments3D[i].idColor == 3)
				glColor3f(1.0f, 0.0f, 0.0f);
			glBegin (GL_LINES);
				glVertex3f (groundSegments3D[i].start.position.X, groundSegments3D[i].start.position.Y, groundSegments3D[i].start.position.Z);
				glVertex3f (groundSegments3D[i].end.position.X, groundSegments3D[i].end.position.Y, groundSegments3D[i].end.position.Z);
			glEnd ();
		}
	}
}

inline void DrawCircle(float cx, float cy, float r, int num_segments) { 
	int ii;
	float x, y, theta;

	glBegin(GL_LINE_LOOP); 
	for(ii = 0; ii < num_segments; ii++) { 
		theta = 2.0f * 3.1415926f * ((float)ii) / ((float)num_segments);//get the current angle 

		x = r * cosf(theta);//calculate the x component 
		y = r * sinf(theta);//calculate the y component 

		glVertex3f(-(x + cx), -(y + cy), 0); // output vertex 

	} 
	glEnd(); 
}

inline void drawParallelogram (Parallelogram3D parallelogram) {
	glShadeModel(GL_FLAT);
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
		glVertex3f (parallelogram.p2.position.X, parallelogram.p2.position.Y, parallelogram.p2.position.Z);
		glVertex3f (parallelogram.p1.position.X, parallelogram.p1.position.Y, parallelogram.p1.position.Z);
		glVertex3f (parallelogram.p3.position.X, parallelogram.p3.position.Y, parallelogram.p3.position.Z);
		glVertex3f (parallelogram.p4.position.X, parallelogram.p4.position.Y, parallelogram.p4.position.Z);
	glEnd ();
}

inline void drawFace (Face3D face) {
	glColor3f(0.0f, 0.0f, 1.0f);
  glLineWidth(2.f);

	DrawCircle (face.center.position.X, face.center.position.Y, 70., 5);
}

inline void drawArrow (Arrow3D arrow) {
	float ang, dist, dist2, angarrow;

	dist = 210.; // longitud línea base
	dist2 = 100.; // longitud líneas de aspas
	angarrow = 45.*PI/180.; // ángulo de las aspas respecto a la base

	ang = atan((arrow.end.Y - arrow.start.Y)/(arrow.end.X - arrow.start.X)); // ángulo de la flechita en el suelo (eje (X,Y))

	glColor3f(0.5f, 1.0f, 0.0f);
  glLineWidth(3.f);

	glBegin(GL_LINES);
		glVertex3f(arrow.end.X, arrow.end.Y, 0.); // línea base, punto end
		glVertex3f((arrow.end.X + (dist * (-cos(ang)))),(arrow.end.Y + (dist * sin(ang))), 0.); // línea base, punto start

		glVertex3f(arrow.end.X, arrow.end.Y, 0.); // línea aspa1, punto end
		glVertex3f((arrow.end.X + (dist2 * (-cos(ang - angarrow)))), (arrow.end.Y + (dist2 * sin(ang - angarrow))), 0.); // línea aspa1, punto start

		glVertex3f(arrow.end.X, arrow.end.Y, 0.); // línea aspa2, punto end
		glVertex3f((arrow.end.X + (dist2 * (-cos(ang + angarrow)))), (arrow.end.Y + (dist2 * sin(ang + angarrow))), 0.); // línea aspa2, punto start
	glEnd();
}

inline void drawMemory () {
	int i;
	struct elementStruct *r;

	r = myElements;

	if (r != NULL) {
		while (r->next != NULL) {
			if (r->type == 1) // dibujamos un paralelogramo
				drawParallelogram (r->parallelogram);
			else if (r->type == 2) // dibujamos una cara
				drawFace (r->face);
			else if (r->type == 3) // dibujamos una flecha
				drawArrow (r->arrow);
		}
	}
}

inline static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;
	float dxPioneer, dyPioneer, dzPioneer, longiPioneer, latiPioneer, rPioneer;

	pthread_mutex_lock(&gl_mutex);

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	if (worldView == TRUE) {
		initWorldCanvasSettings ();

	  /** Robot Frame of Reference **/
	  glMatrixMode(GL_MODELVIEW);
/*	  glLoadIdentity();
	  if (myencoders!=NULL){
	     mypioneer.posx=myencoders[0];
	     mypioneer.posy=myencoders[1];
	     mypioneer.posz=0.;
	     mypioneer.foax=myencoders[0];
	     mypioneer.foay=myencoders[1];
	     mypioneer.foaz=10.;
	     mypioneer.roll=myencoders[2]*RADTODEG;
	  }
	  else{
	     mypioneer.posx=0.;
	     mypioneer.posy=0.;
	     mypioneer.posz=0.;
	     mypioneer.foax=0.;
	     mypioneer.foay=0.;
	     mypioneer.foaz=10.;
	     mypioneer.roll=0.;
	  }
	  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
	  dxPioneer = (mypioneer.foax-mypioneer.posx);
	  dyPioneer = (mypioneer.foay-mypioneer.posy);
	  dzPioneer = (mypioneer.foaz-mypioneer.posz);
	  longiPioneer = (float)atan2(dyPioneer,dxPioneer)*360./(2.*PI);
	  glRotatef (longiPioneer,0.,0.,1.);
	  rPioneer = sqrt(dxPioneer*dxPioneer+dyPioneer*dyPioneer+dzPioneer*dzPioneer);
	  if (rPioneer<0.00001) latiPioneer=0.;
	  else latiPioneer=acos(dzPioneer/rPioneer)*360./(2.*PI);
	  glRotatef(latiPioneer,0.,1.,0.);
	  glRotatef(mypioneer.roll,0.,0.,1.);

		glEnable (GL_LIGHTING); // LUCES Y.... ACCION!!
		glPushMatrix();
		glTranslatef(1.,0.,0.);
		// the body it is not centered. With this translation we center it
		glScalef (100., 100., 100.);
		loadModel();
		glPopMatrix();
		glDisable (GL_LIGHTING); // FUERA LUCES, VOLVEMOS A LA VIDA REAL
*/
		glLoadIdentity ();
		//drawMyLines ();
		//drawGroundLines ();
		drawMemory ();
	}

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}


inline void visualSonar_guidisplay()
{
	pthread_mutex_lock(&main_mutex);
	gdk_threads_enter();
	GtkImage *filImg = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	GtkImage *GroImg = GTK_IMAGE(glade_xml_get_widget(xml, "groundedImage"));
	GtkImage *OriImg = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	GtkImage *BorImg = GTK_IMAGE(glade_xml_get_widget(xml, "borderImage"));

	sliderPANTILT_BASE_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"));
	sliderISIGHT_OPTICAL_CENTER = gtk_range_get_value(glade_xml_get_widget(xml, "sliderOPTICAL_CENTER"));
	sliderTILT_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderTILT_HEIGHT"));
	sliderCAMERA_TILT_HEIGHT = gtk_range_get_value(glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"));
	sliderPANTILT_BASE_X = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"));
	sliderThreshold = gtk_range_get_value(glade_xml_get_widget(xml, "sliderThreshold"));
	panAngle = gtk_range_get_value(glade_xml_get_widget(xml, "sliderPAN_ANGLE"));
	tiltAngle = gtk_range_get_value(glade_xml_get_widget(xml, "sliderTILT_ANGLE"));

	gtk_widget_queue_draw(GTK_WIDGET(filImg));
	gtk_widget_queue_draw(GTK_WIDGET(GroImg));
	gtk_widget_queue_draw(GTK_WIDGET(OriImg));
	gtk_widget_queue_draw(GTK_WIDGET(BorImg));
	//if (worldView == TRUE) if (visualSonarCanvas!=NULL) 
	expose_event(visualSonarCanvas, NULL, NULL);
	//else if (floorView == TRUE) if (floorCanvas!=NULL) expose_event(floorCanvas, NULL, NULL);

	gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();
	pthread_mutex_unlock(&main_mutex);
}

inline void visualSonar_hide() {
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(visualSonar_guidisplay);
	all[visualSonar_id].guistate=off;
}

/*Callback of window closed*/
inline void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
   gdk_threads_leave();
   visualSonar_hide();
   gdk_threads_enter();
}

inline void visualSonar_show()
{
	int loadedgui=0;
	static pthread_mutex_t visualSonar_gui_mutex;
	GtkButton *floorButton, *worldButton, *go1Button, *go50Button, *turn45RButton, *turn45LButton, *turn90RButton, *turn90LButton;
	GtkToggleButton *camera1Button, *camera2Button, *camera3Button, *camera4Button, *ceilCameraButton, *userCameraButton;
	GtkWidget *widget1;

	pthread_mutex_lock(&visualSonar_gui_mutex);
	if (!loadedgui){
		loadglade ld_fn;
		loadedgui=1;
		pthread_mutex_unlock(&visualSonar_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

		xml = ld_fn ("visualSonar.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical visualSonar on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));
		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");

			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
		}

		//floorCanvas = glade_xml_get_widget(xml, "floorCanvas");

    visualSonarCanvas = glade_xml_get_widget(xml, "visualSonarCanvas");
    //gtk_widget_set_size_request (visualSonarCanvas, 1264, 568);

    gtk_widget_unrealize(visualSonarCanvas);
    /* Set OpenGL-capability to the widget. */
    if (gtk_widget_set_gl_capability (visualSonarCanvas,glconfig,NULL,TRUE,GDK_GL_RGBA_TYPE)==FALSE)
    {
        printf ("No Gl capability\n");
        jdeshutdown(1);
    }
    gtk_widget_realize(visualSonarCanvas);

		gtk_widget_set_child_visible (GTK_WIDGET(visualSonarCanvas), TRUE);
		//gtk_widget_set_child_visible (GTK_WIDGET(floorCanvas), TRUE);

		gtk_widget_add_events ( visualSonarCanvas,
		                        GDK_BUTTON1_MOTION_MASK    |
		                        GDK_BUTTON2_MOTION_MASK    |
		                        GDK_BUTTON3_MOTION_MASK    |
		                        GDK_BUTTON_PRESS_MASK      |
		                        GDK_BUTTON_RELEASE_MASK    |
		                        GDK_VISIBILITY_NOTIFY_MASK);	

		widget1=(GtkWidget *)glade_xml_get_widget(xml, "visualSonarCanvas");

		g_signal_connect (G_OBJECT (widget1), "button_press_event", G_CALLBACK (button_press_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (motion_notify_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (scroll_event), visualSonarCanvas);

		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		//glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		// OpenGl worlds control buttons:
		floorButton = (GtkToggleButton *)glade_xml_get_widget(xml, "floorButton");
		g_signal_connect (G_OBJECT (floorButton), "pressed", G_CALLBACK (floorButton_pressed), NULL);
		g_signal_connect (G_OBJECT (floorButton), "released", G_CALLBACK (floorButton_released), NULL);

		worldButton = (GtkToggleButton *)glade_xml_get_widget(xml, "worldButton");
		g_signal_connect (G_OBJECT (worldButton), "pressed", G_CALLBACK (worldButton_pressed), NULL);
		g_signal_connect (G_OBJECT (worldButton), "released", G_CALLBACK (worldButton_released), NULL);

		// Movement control buttons:
		go1Button = (GtkButton *)glade_xml_get_widget(xml, "go1Button");
		g_signal_connect (G_OBJECT (go1Button), "pressed", G_CALLBACK (go1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go1Button), "released", G_CALLBACK (go1Button_released), NULL);

		go50Button = (GtkButton *)glade_xml_get_widget(xml, "go50Button");
		g_signal_connect (G_OBJECT (go50Button), "pressed", G_CALLBACK (go50Button_pressed), NULL);
		g_signal_connect (G_OBJECT (go50Button), "released", G_CALLBACK (go50Button_released), NULL);

		turn45RButton = (GtkButton *)glade_xml_get_widget(xml, "turn45RButton");
		g_signal_connect (G_OBJECT (turn45RButton), "pressed", G_CALLBACK (turn45RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45RButton), "released", G_CALLBACK (turn45RButton_released), NULL);

		turn45LButton = (GtkButton *)glade_xml_get_widget(xml, "turn45LButton");
		g_signal_connect (G_OBJECT (turn45LButton), "pressed", G_CALLBACK (turn45LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn45LButton), "released", G_CALLBACK (turn45LButton_released), NULL);

		turn90RButton = (GtkButton *)glade_xml_get_widget(xml, "turn90RButton");
		g_signal_connect (G_OBJECT (turn90RButton), "pressed", G_CALLBACK (turn90RButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90RButton), "released", G_CALLBACK (turn90RButton_released), NULL);

		turn90LButton = (GtkButton *)glade_xml_get_widget(xml, "turn90LButton");
		g_signal_connect (G_OBJECT (turn90LButton), "pressed", G_CALLBACK (turn90LButton_pressed), NULL);
		g_signal_connect (G_OBJECT (turn90LButton), "released", G_CALLBACK (turn90LButton_released), NULL);

		g_signal_connect (G_OBJECT (glade_xml_get_widget(xml, "flashButton")), "clicked", G_CALLBACK (flashButton_pressed), NULL);

		camera1Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
		g_signal_connect (G_OBJECT (camera1Button), "pressed", G_CALLBACK (camera1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera1Button), "released", G_CALLBACK (camera1Button_released), NULL);

		camera2Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
		g_signal_connect (G_OBJECT (camera2Button), "pressed", G_CALLBACK (camera2Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera2Button), "released", G_CALLBACK (camera2Button_released), NULL);

		camera3Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
		g_signal_connect (G_OBJECT (camera3Button), "pressed", G_CALLBACK (camera3Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera3Button), "released", G_CALLBACK (camera3Button_released), NULL);

		camera4Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
		g_signal_connect (G_OBJECT (camera4Button), "pressed", G_CALLBACK (camera4Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera4Button), "released", G_CALLBACK (camera4Button_released), NULL);

		ceilCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
		g_signal_connect (G_OBJECT (ceilCameraButton), "pressed", G_CALLBACK (ceilCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (ceilCameraButton), "released", G_CALLBACK (ceilCameraButton_released), NULL);

		userCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
		g_signal_connect (G_OBJECT (userCameraButton), "pressed", G_CALLBACK (userCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (userCameraButton), "released", G_CALLBACK (userCameraButton_released), NULL);

		gtk_range_set_value(glade_xml_get_widget(xml, "sliderPAN_ANGLE"),panAngle);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderTILT_ANGLE"),tiltAngle);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"),sliderPANTILT_BASE_HEIGHT);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderOPTICAL_CENTER"),sliderISIGHT_OPTICAL_CENTER);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderTILT_HEIGHT"),sliderTILT_HEIGHT);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"),sliderCAMERA_TILT_HEIGHT);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"),sliderPANTILT_BASE_X);
		gtk_range_set_value(glade_xml_get_widget(xml, "sliderThreshold"),sliderThreshold);

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
		pthread_mutex_unlock(&visualSonar_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}

	gdk_threads_enter();
	// Initialization of the image buffers
	myregister_displaycallback(visualSonar_guidisplay);

	GdkPixbuf *originalImageBuf, *filteredImageBuf, *borderImageBuf, *groundedImageBuf;

	originalImage = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	groundedImage = GTK_IMAGE(glade_xml_get_widget(xml, "groundedImage"));
	borderImage = GTK_IMAGE(glade_xml_get_widget(xml, "borderImage"));

	originalImageBuf = gdk_pixbuf_new_from_data(originalImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					originalImageRGB->width,originalImageRGB->height,
					originalImageRGB->width*3,NULL,NULL);
	filteredImageBuf = gdk_pixbuf_new_from_data(filteredImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					filteredImageRGB->width,filteredImageRGB->height,
					filteredImageRGB->width*3,NULL,NULL);
	groundedImageBuf = gdk_pixbuf_new_from_data(groundedImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					groundedImageRGB->width,groundedImageRGB->height,
					groundedImageRGB->width*3,NULL,NULL);
	borderImageBuf = gdk_pixbuf_new_from_data(contourImage,
					GDK_COLORSPACE_RGB,0,8,
					SIFNTSC_COLUMNS,SIFNTSC_ROWS,
					SIFNTSC_COLUMNS*3,NULL,NULL);

	gtk_image_set_from_pixbuf(originalImage, originalImageBuf);
	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);
	gtk_image_set_from_pixbuf(groundedImage, groundedImageBuf);
	gtk_image_set_from_pixbuf(borderImage, borderImageBuf);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	gdk_threads_leave();
}
