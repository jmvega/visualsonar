/*
 *  Copyright (C) 2009 Julio M. Vega Pérez 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio M. Vega Pérez <julio.vega@urjc.es>
 */
#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>
#include <unistd.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <progeo.h>
#include <colorspaces.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

#include <opencv/cv.h>

#define ROOM_MAX_X 7925.
#define v3f glVertex3f
#define PI 3.141592654
//#define FLT_MAX 999999.9
#define SQUARE(a) (a)*(a)

#define MAX_RADIUS_VALUE 100000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 10

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240
#define MAXWORLD 20000.
#define V_MAX 100

#define ENCOD_TO_DEG (3.086/60.) /* CTE que nos pasa de unidades PANTILT a grados */
#define DEG_TO_ENCOD (60./3.086) /* CTE que nos pasa de grados a unidades PANTILT */
#define MAXPAN_POS 95
#define MAXPAN_NEG -95
#define VEL_MAX_PAN 2500.0*ENCOD_TO_DEG
#define VEL_MIN_PAN 350.0*ENCOD_TO_DEG
#define POS_MIN_PAN 40.0
#define POS_MAX_PAN 160.0
#define VEL_MAX_TILT 1000.0*ENCOD_TO_DEG
#define VEL_MIN_TILT 300.0*ENCOD_TO_DEG
#define POS_MIN_TILT 40.0
#define POS_MAX_TILT 120.0
#define TIME_TO_LOOK 2

// Parámetros extrínsecos de los distintos elementos del Pioneer + Pantilt + Cámara
#define PANTILT_BASE_HEIGHT 250.
#define ISIGHT_OPTICAL_CENTER -80.
#define TILT_HEIGHT 70.
#define CAMERA_TILT_HEIGHT 120.
#define PANTILT_BASE_X 60.
#define PANTILT_BASE_Y 0.

// Parámetros relacionados con el tratamiento de los segmentos
#define MIN_TAM_SEG 20
#define MAX_TAM_SEG 700
#define MAX_LINES_TO_DETECT 70
#define MAX_LINES_IN_MEMORY 100
#define MAX_PARALLELOGRAMS_IN_MEMORY 10
#define MAX_PAR 50
#define MAX_DIST 30
#define BORDER_THRESHOLD 10
#define HOUGH_MIN_DIST_SEG 30
#define HOUGH_LINE_THRESHOLD 40

#define MAX_TAM_ARROW 250 // realmente mis flechas miden como máximo (la base) unos 210 mm.
#define MIN_TAM_ARROW 80 // realmente mis flechas miden como máximo (el aspa) unos 100 mm.
#define MAX_TAM_BASE 115

// Parámetros relacionados con la atención visual
#define SALIENCY_INCREMENT 1
#define SALIENCY_DECREMENT 1
#define MAX_SALIENCY 100
#define MIN_SALIENCY 0
#define LIVELINESS_INCREMENT 20
#define LIVELINESS_DECREMENT 10
#define MIN_LIVELINESS 0
#define MAX_LIVELINESS 5000
#define LIVELINESS_TO_DEAD -1
#define PENALTY_FACTOR 200
#define BONUS_FACTOR 100
#define FOLLOW_TIME 2
#define TIME_TO_FORCED_SEARCH 3
#define TIME_TO_DEAD 50
#define TILT_MAX 90
#define TILT_MIN -90
#define CENTRO_X 260
#define CENTRO_Y 260
#define RADIO_MAX 258
#define RADIO_MIN 0
#define ANCHO_ESCENA_COMPUESTA 520
#define LONGITUDE_NUM_POSITIONS 3
#define MAX_FACES_IN_MEMORY 10
#define MIN_EXP_WIDTH 20
#define MIN_EXP_HEIGHT 20
#define MAX_GAP_FACES 20 // mm. máxima distancia entre caras que se consideran la misma
#define MAX_GAP_ARROWS 20

extern void visualSonar_init();
extern void visualSonar_stop();
extern void visualSonar_run(int father, int *brothers, arbitration fn);
extern int visualSonar_cycle;

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
} SofReference;

struct image_struct {
	int width;
	int height;
	int bpp;	// bytes per pixel
	char *image;
};

typedef struct HPoint3DInformation {
	HPoint3D position;
  int idColor;
	int real;
	//...
} HPoint3Dinfo;

typedef struct colorRGBInf {
	float R;
	float G;
	float B;
} colorRGB;

typedef struct Segment3Dim {
	HPoint3Dinfo start;
	HPoint3Dinfo end;
	float length;
	int idColor;
	int isValid;
	//...
} Segment3D;

typedef struct Segment2Dim {
	HPoint2D start;
	HPoint2D end;
} Segment2D;

typedef struct Parallelogram3Dim {
	HPoint3Dinfo p1;
	HPoint3Dinfo p2;
	HPoint3Dinfo p3;
	HPoint3Dinfo p4;
	HPoint3D centroid;
	int isValid;
	//...
} Parallelogram3D;

typedef struct Face3Dim {
	HPoint3Dinfo center;
	int isValid;
	//...
} Face3D;

typedef struct Arrow3Dim {
	HPoint3D start; // base de la flecha
	HPoint3D end; // extremo de la flecha (hacia donde apunta)
	colorRGB color;
	float length; // quizás redundante
	float direction;
	//...
} Arrow3D;

typedef struct SemiArrow3Dim {
	int isValid;
	Segment3D base; // base (segmento más largo) de la flecha
	Segment3D cross; // uno de las aspas de la flecha
	colorRGB color;
} SemiArrow3D;

enum movement_pantilt {up,down,left,right};

// Parámetros relacionados con la atención visual
int longitudeScenePositions[LONGITUDE_NUM_POSITIONS] = {-39, 0, 39};
int latitudeScenePositions[4] = {31, 14, -14, -31};
enum state {think, search, analizeSearch};

typedef struct {
	float pan;
	float tilt;
} scenePoint;

typedef struct {
	int x;
	int y;
} imagePoint;

typedef struct {
  int x;
  int y;
} t_vector;

struct elementStruct {
	double lastInstant; // último instante de tiempo en su detección
	double firstInstant; // primer instante de tiempo en su detección
	int pixel_x;
	int pixel_y;
	float latitude; // posición absoluta del pantilt, en eje tilt
	float longitude; // posición absoluta del pantilt, en eje pan
	float scenePoint_pan;
	float scenePoint_tilt;
	int scenePos; // it can be left, center or right, depends on pantilt pos where face was detected
	float saliency;
	float liveliness;
	int type; // 0 = elemento virtual; 1 = rectángulo; 2 = face; 3 = arrow
	int isVisited;
	int treatedNow; // sólo puede haber uno que esté siendo tratado en un momento
	Parallelogram3D parallelogram;
	Face3D face;
	Arrow3D arrow;
	struct elementStruct* next;
};

