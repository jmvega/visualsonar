/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This schema was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "visualNav.h"

int visualNav_brothers[MAX_SCHEMAS];
arbitration visualNav_callforarbitration;

int DEBUG = 1;
pthread_mutex_t main_mutex;
int visualNav_id=0; 
int visualNav_brothers[MAX_SCHEMAS];
arbitration visualNav_callforarbitration;
int visualNav_cycle=100; /* ms */
float *myencoders=NULL;
float *myv=NULL;
float *myw=NULL;
runFn encodersrun, motorsrun;
stopFn encodersstop, motorsstop;
runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;
HPoint2D actualPosition, lastPosition;
double actualInstant;

/************************************************************************************************************/
/**************** V A R I A B L E S   D E L   M O V I M I E N T O   D E L   P I O N E E R *******************/
/************************************************************************************************************/
TdatosResultante resultante;
TdatosResultante *result;
short distanciaObjetivo;
float xRep,yRep,xAtr,yAtr,xRes,yRes;
float difAngle;
float vActual,wActual;
float peligro;
Tvoxel miObjetivo, fuerzaResultante, repulsiva, atractiva;
double navigationTime;
struct CacheList *cache; // lista de elementos en cache
struct HPoint3DList *segPointList;
HPoint2D windowA, windowB, windowC, windowD, windowE, windowF, windowG, windowH, windowI, windowJ; // Puntos clave de la ventana de seguridad
/*****************************************************************************************************************************/
/****************************F U N C I O N E S   R E L A T I V A S   A   L A   N A V E G A C I Ó N****************************/
/*****************************************************************************************************************************/
inline float translateAngle (float angulo) {
	/* Transformamos un angulo dado en radianes en grados de tal manera k si el
	angulo introducido es 180>thetha>0 se kda tal cual y si es 180<theta<0 kda
	como -thetha*/ 
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; /* PASAMOS la entrada A GRADOS */
	auxi=angulo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  /* Esta conversion la hacemos para quedarnos con 2 decimales */

	return salida;
}

inline int calculateForce (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return fuerza;
}

inline int sameAngleAndDistance (Tvoxel a,int distanciaPuntoMem,int anguloPuntoNuevo,int distanciaNueva) {
	int angulo_aux;
	int salida=FALSE;
	float margen = 500;
	int dist = FALSE;

	dist = distanciaPuntoMem < (distanciaNueva-margen);
	angulo_aux = (int)(atan2(a.y-myencoders[1],a.x-myencoders[0])*RADTODEG);
	salida = ((anguloPuntoNuevo <= (angulo_aux + 0.7)) && (anguloPuntoNuevo >= (angulo_aux - 0.7)));

	return salida && dist;
}

inline float transformoAngulo (float angulo) {
	// Transformamos un angulo dado en radianes en grados de tal manera que si el	angulo introducido es 180>thetha>0 se queda tal cual y si es 180<theta<0 queda	como -thetha
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; // PASAMOS la entrada A GRADOS
	auxi=angulo*100; // Nos quedamos con 2 decimales
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  // Esta conversion la hacemos para quedarnos con 2 decimales

	return salida;
}

inline void guardarCoordObjetivo(TdatosResultante *res) {
	float auxi1;
	double radianes;

	(*res).direccionFuerzaAtractiva[0]=miObjetivo.x-myencoders[0];
	(*res).direccionFuerzaAtractiva[1]=miObjetivo.y-myencoders[1];

	(*res).anguloFuerzaAtractiva=atan2((*res).direccionFuerzaAtractiva[1],(*res).direccionFuerzaAtractiva[0]);
		
	(*res).moduloFuerzaAtractiva=moduloAtractiva;

	radianes=(*res).anguloFuerzaAtractiva;

	(*res).direccionFuerzaAtractiva[0]=cos(radianes)*(*res).moduloFuerzaAtractiva;
	(*res).direccionFuerzaAtractiva[1]=sin(radianes)*(*res).moduloFuerzaAtractiva;

	auxi1=transformoAngulo((*res).anguloFuerzaAtractiva);
	(*res).anguloFuerzaAtractiva=auxi1;
}

void calcVectorial(int xini, int yini, int xfin, int yfin, HPoint2D *u, HPoint2D *p) {
    /*Vector director*/
    u->x = (float) (xfin - xini);
    u->y = (float) (yfin - yini);
    u->h = 1.0;

    /*Punto*/
    p->x = (float) xini;
    p->y = (float) yini;
    p->h = 1.0;
}

inline float distanceBetweenPoints (HPoint3D p1, HPoint3D p2) {
  float dist;

  dist = abs(sqrt(pow(p2.X-p1.X,2) + pow(p2.Y-p1.Y,2) + pow(p2.Z-p1.Z,2)));
  return dist;
}

inline void divideSegment (Segment3D segment) {
  HPoint3D start, end;
	HPoint2D u, p;
	struct HPoint3DList *myPoint, *lastPoint;
	float itmp;
	int i;
	float dist;

	lastPoint = segPointList;
	myPoint = lastPoint;

	while (lastPoint != NULL) {
		lastPoint = myPoint->next;
		free (myPoint);
		myPoint = lastPoint;
	}

	segPointList = NULL; // inicializamos puntero inicial y final	
	lastPoint = segPointList;
	
	start = segment.start.position;
	end = segment.end.position;

	dist = distanceBetweenPoints (start, end);

	if (dist > MIN_TAM_SEG_TO_DIVIDE) {
		calcVectorial(start.X, start.Y, end.X, end.Y, &u, &p);
    for(i=0;i<=100;i=i+25) { // dividimos el segmento en 5 partes
			itmp = ((float)i)/100.0;

			myPoint = NULL;
			myPoint = (struct HPoint3DList*)malloc(sizeof(struct HPoint3DList));
			myPoint->point.X = (float)(p.x + itmp*u.x);
			myPoint->point.Y = (float)(p.y + itmp*u.y);
			//if (DEBUG) printf ("divideSegment: storing point [%0.2f, %0.2f]...\n", myPoint->point.X, myPoint->point.Y);
			myPoint->point.Z = 0.;
			myPoint->point.H = 1;
			myPoint->next = NULL;

			if (segPointList != NULL) {
				lastPoint->next = myPoint;
				lastPoint = lastPoint->next;
			} else { // list = NULL, así que inicializamos
				segPointList = myPoint; // inicialización de list
				lastPoint = segPointList;
			}
		}
	}
}

inline int calculoFuerza (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return 0; // TODO fuerza;
}

inline void calculaResultante(TdatosResultante *res) {
	int angulo,ang_robot,diff_ang;
	int sumaX, sumaY;
	int distancia,modulo;
	float auxi, auxi1;
	double radianes;
	struct HPoint3DList *actualPoint;
	HPoint3D point;
	struct CacheList *p;

	sumaX=0;
	sumaY=0;
	p = cache;

	while (p != NULL) {
		if ((p->segment->isValid == TRUE)/* && (!segmentIsInOverlappArea (*(p->segment)))*/) {
			divideSegment (*(p->segment));
			/* calculo la distancia al robot y el angulo respecto a este */
			actualPoint = segPointList;
			while (actualPoint != NULL) {
				point.X = actualPoint->point.X;
				point.Y = actualPoint->point.Y;
				distancia = (((point.X-myencoders[0])*(point.X-myencoders[0]))+((point.Y-myencoders[1])*(point.Y-myencoders[1])));
				radianes = atan2((point.Y-myencoders[1]),(point.X-myencoders[0]));
				angulo = (int)(radianes*RADTODEG);
				ang_robot = (int)(myencoders[2]*RADTODEG);

				/* SUTILEZA PARA QUE EL ROBOT TENGA QUE GIRAR LO MENOS POSIBLE */
				if(ang_robot>=180) ang_robot=-360+ang_robot; /* =-(360-ang_robot)-->pasa a negativo */

				diff_ang=abs(angulo-ang_robot); /* calculamos el total que debería girar el robot */

				if (diff_ang>=180) diff_ang=360-diff_ang; /* mejor si gira por el camino más corto */
				/* FIN DE LA SUTILEZA */

				/* Miramos si el punto esta dentro del alcance en el que los puntos generan fuerza de repulsión. La distancia es distinta segun el angulo en el que se encuentre el punto respecto del robot */
				if (((diff_ang>45)&&(diff_ang<135)&&(distancia<alcanceLateral))||
					(((diff_ang<=45)||(diff_ang>=135))&&(distancia<alcance))) { 
					/* Primero miramos si la distancia es menor que la seguridad asignada. Si es menor giramos sin avanzar */
					if (((diff_ang>45)&&(diff_ang<135)&&(distancia<seguridadLateral))||
						((diff_ang<=45)&&(distancia<seguridad)))
	        
						(*res).avanzaEnGiro=FALSE; /* inicialmente está a TRUE */

					/* sumo al angulo 180 ya que lo que buscamos es fuerza repulsiva y queremos el angulo opuesto */
					radianes=radianes+(180.0*DEGTORAD);

					/* calculamos el modulo */
					modulo=calculoFuerza(distancia,diff_ang); /* FUERZA REPULSIVA */

					/* Añadimos las componentes a la suma de x y de y.*/
					sumaX+=cos(radianes)*modulo;
					sumaY+=sin(radianes)*modulo;	
				}
				actualPoint = actualPoint->next;
			} // fin while (actualPoint != NULL)
		}
		p = p->next;
	} // fin FOR

	(*res).direccionFuerzaRepulsiva[0]= sumaX;
	(*res).direccionFuerzaRepulsiva[1]= sumaY;

	/*if (DEBUG) printf("xRep:%d f_0.yRep:%d\n",(*res).direccionFuerzaRepulsiva[0],(*res).direccionFuerzaRepulsiva[1]);*/

	/* Calculo la fuerza resultante atribuyendo factores de proporción a la fuerza atractiva y repulsiva */
	(*res).direccionFuerzaResultante[0]=(alpha *(*res).direccionFuerzaAtractiva[0]) +(beta*(*res).direccionFuerzaRepulsiva[0]);
	(*res).direccionFuerzaResultante[1]=(alpha *(*res).direccionFuerzaAtractiva[1]) +(beta*(*res).direccionFuerzaRepulsiva[1]);

	sumaX=(*res).direccionFuerzaResultante[0];
	sumaY=(*res).direccionFuerzaResultante[1];

	(*res).moduloFuerzaResultante=sqrt((sumaX*sumaX)+(sumaY*sumaY));
	(*res).anguloFuerzaResultante=atan2(sumaY,sumaX);	  

	auxi1=transformoAngulo((*res).anguloFuerzaResultante);
	(*res).anguloFuerzaResultante=auxi1;
	/*if (DEBUG) printf("Angulo resultante:%0.2f\n",(*res).anguloFuerzaResultante);*/

	if ((sumaX==(*res).direccionFuerzaAtractiva[0])&&(sumaY==(*res).direccionFuerzaAtractiva[1])) { /* ¿? sumaX y sumaY han variado, por los factores aplicados...esto NO FUNCIONARÁ */
		/* en caso k no actuen fuerzas, solo tenemos la atractiva*/
		(*res).anguloFuerzaResultante=(*res).anguloFuerzaAtractiva;;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=auxi1/100;
	}

	if((*res).anguloFuerzaResultante==0.) {
		/* es mas facil detectar 6.28 k 2*Pi, por eso aproximamos*/
		(*res).anguloFuerzaResultante=2*M_PI;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=transformoAngulo(auxi1/100);
	}

	auxi=((*res).anguloFuerzaResultante)*100;
	auxi1=ceil(auxi);
	(*res).anguloFuerzaResultante=(auxi1/100);
}

inline int overpass () {
	return ((abs((int)(miObjetivo.x-myencoders[0]))<1100)&&(abs((int)(miObjetivo.y-myencoders[1]))<1100));
}

inline void getAnguloQueLlevo () {
	float auxi,auxi1;

	// Todo esto es para quedarnos con el ángulo que llevamos, pero sólo con 2 decimales
	resultante.anguloQueLlevo=myencoders[2];
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=auxi1/100;

	// Transformo el ángulo (sutileza por el recorrido más corto)
	auxi1=transformoAngulo(resultante.anguloQueLlevo);
	resultante.anguloQueLlevo=auxi1;

	// Y nos volvemos a quedar con 2 decimales
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=(auxi1/100);
}

inline void generateVirtualObjetive (HPoint3D *myLocalObjetive) {
	float ang, dist;

	dist = 2000.; // longitud a prolongar la flecha (2 metros)
	ang = myencoders[2];

	myLocalObjetive->X = myencoders[0] + (dist * (cos(ang)));
	myLocalObjetive->Y = myencoders[1] + (dist * sin(ang));
	if (DEBUG) printf ("virtualObjetive: robot position = [%0.2f, %0.2f], objetivo = [%0.2f, %0.2f]\n", myencoders[0], myencoders[1], myLocalObjetive->X, myLocalObjetive->Y);
}

inline void getObjetivo () {
	// busco si hay flechas en mi memoria atentiva, y tengo en cuenta la más cercana
	//struct elementStruct *r;
	HPoint3D myPosition, myLocalObjetive;/*
	float actualDist, minDist = 9999999.;
	int found = FALSE;
	Arrow3D myArrow;

	myPosition.X = myencoders[0];
	myPosition.Y = myencoders[1];
	myPosition.Z = 0.;
	myPosition.H = 1.;

	r = myElements;

	while (r != NULL) { // cuando encuentres una cara o flecha en la que solape, paramos de buscar
		if (r->type == 3) { // si es una flecha
			found = TRUE;
			actualDist = distanceBetweenPoints (myPosition, r->arrow.start);
			if (actualDist < minDist) {
				minDist = actualDist;
				myArrow = r->arrow;
			}
		}
		r = r->next;
	} // cuando salgamos tendremos la flecha más cercana al robot, caso de haberla

	if (found) extendArrow (myArrow, &myLocalObjetive); // si hemos encontrado flecha, calculamos el objetivo según ésta
	else */generateVirtualObjetive (&myLocalObjetive);

	miObjetivo.x = myLocalObjetive.X;
	miObjetivo.y = myLocalObjetive.Y;
	if (DEBUG) printf ("getObjetivo: objetivo elegido = [%0.2f, %0.2f]\n", miObjetivo.x, miObjetivo.y);
}

inline void checkRobotIsInObjetive () {
	if ((abs((int)(miObjetivo.x-myencoders[0]))<200)&&(abs((int)(miObjetivo.y-myencoders[1]))<200)) {   
		distanciaObjetivo=0;
		resultante.direccionFuerzaAtractiva[0]=0.0;
		resultante.direccionFuerzaAtractiva[1]=0.0;
	}	else distanciaObjetivo=1;
}

inline void getForcesParams () {
	xRep=resultante.direccionFuerzaRepulsiva[0]; // REPULSIVA
	yRep=resultante.direccionFuerzaRepulsiva[1];

	xAtr=resultante.direccionFuerzaAtractiva[0]; // ATRACTIVA
	yAtr=resultante.direccionFuerzaAtractiva[1];

	xRes=resultante.direccionFuerzaResultante[0]; // RESULTANTE = REPULSIVA vs. ATRACTIVA
	yRes=resultante.direccionFuerzaResultante[1];
}

inline void getActualWindow () {
	// actualizamos las coordenadas de los puntos extremos de la ventana de seguridad
	// windowA, windowB, windowC, windowD, windowE, windowF, windowG, windowH, windowI, windowJ
	windowA.x = frenteY*(cos(myencoders[2])) - huecoX*(sin(myencoders[2])) + myencoders[0];
	windowA.y = huecoX*cos(myencoders[2]) + frenteY*sin(myencoders[2]) + myencoders[1];

	windowB.x = frenteY*(cos(myencoders[2])) - (-huecoX)*(sin(myencoders[2])) + myencoders[0];
	windowB.y = (-huecoX)*cos(myencoders[2]) + frenteY*sin(myencoders[2]) + myencoders[1];

	windowC.x = ventanaY*(cos(myencoders[2])) - huecoX*(sin(myencoders[2])) + myencoders[0];
	windowC.y = huecoX*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowD.x = ventanaY*(cos(myencoders[2])) - ventanaX*(sin(myencoders[2])) + myencoders[0];
	windowD.y = ventanaX*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowE.x = ventanaY*(cos(myencoders[2])) - (-ventanaX)*(sin(myencoders[2])) + myencoders[0];
	windowE.y = (-ventanaX)*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowF.x = ventanaY*(cos(myencoders[2])) - (-huecoX)*(sin(myencoders[2])) + myencoders[0];
	windowF.y = (-huecoX)*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowG.x = 0.*(cos(myencoders[2])) - huecoX*(sin(myencoders[2])) + myencoders[0];
	windowG.y = huecoX*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];

	windowH.x = 0.*(cos(myencoders[2])) - ventanaX*(sin(myencoders[2])) + myencoders[0];
	windowH.y = ventanaX*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];

	windowI.x = 0.*(cos(myencoders[2])) - (-ventanaX)*(sin(myencoders[2])) + myencoders[0];
	windowI.y = (-ventanaX)*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];

	windowJ.x = 0.*(cos(myencoders[2])) - (-huecoX)*(sin(myencoders[2])) + myencoders[0];
	windowJ.y = (-huecoX)*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];
}

inline void buildParallelogramFront (Parallelogram3D *p1) {
	p1->p1.position.X = windowA.x;
	p1->p1.position.Y = windowA.y;
	p1->p1.position.Z = 0.;
	p1->p1.position.H = 1;

	p1->p2.position.X = windowC.x;
	p1->p2.position.Y = windowC.y;
	p1->p2.position.Z = 0.;
	p1->p2.position.H = 1;

	p1->p3.position.X = windowF.x;
	p1->p3.position.Y = windowF.y;
	p1->p3.position.Z = 0.;
	p1->p3.position.H = 1;

	p1->p4.position.X = windowB.x;
	p1->p4.position.Y = windowB.y;
	p1->p4.position.Z = 0.;
	p1->p4.position.H = 1;
}

inline void buildParallelogramWindow (Parallelogram3D *p2) {
	p2->p1.position.X = windowC.x;
	p2->p1.position.Y = windowC.y;
	p2->p1.position.Z = 0.;
	p2->p1.position.H = 1;

	p2->p2.position.X = windowG.x;
	p2->p2.position.Y = windowG.y;
	p2->p2.position.Z = 0.;
	p2->p2.position.H = 1;

	p2->p3.position.X = windowH.x;
	p2->p3.position.Y = windowH.y;
	p2->p3.position.Z = 0.;
	p2->p3.position.H = 1;

	p2->p4.position.X = windowD.x;
	p2->p4.position.Y = windowD.y;
	p2->p4.position.Z = 0.;
	p2->p4.position.H = 1;
}

inline void buildParallelogramBeside1 (Parallelogram3D *p3) {
	p3->p1.position.X = windowE.x;
	p3->p1.position.Y = windowE.y;
	p3->p1.position.Z = 0.;
	p3->p1.position.H = 1;

	p3->p2.position.X = windowI.x;
	p3->p2.position.Y = windowI.y;
	p3->p2.position.Z = 0.;
	p3->p2.position.H = 1;

	p3->p3.position.X = windowJ.x;
	p3->p3.position.Y = windowJ.y;
	p3->p3.position.Z = 0.;
	p3->p3.position.H = 1;

	p3->p4.position.X = windowF.x;
	p3->p4.position.Y = windowF.y;
	p3->p4.position.Z = 0.;
	p3->p4.position.H = 1;
}

inline void buildParallelogramBeside2 (Parallelogram3D *p4) {
	p4->p1.position.X = windowD.x;
	p4->p1.position.Y = windowD.y;
	p4->p1.position.Z = 0.;
	p4->p1.position.H = 1;

	p4->p2.position.X = windowH.x;
	p4->p2.position.Y = windowH.y;
	p4->p2.position.Z = 0.;
	p4->p2.position.H = 1;

	p4->p3.position.X = windowI.x;
	p4->p3.position.Y = windowI.y;
	p4->p3.position.Z = 0.;
	p4->p3.position.H = 1;

	p4->p4.position.X = windowE.x;
	p4->p4.position.Y = windowE.y;
	p4->p4.position.Z = 0.;
	p4->p4.position.H = 1;
}

inline void commandVelocity () {
	difAngle=(resultante.anguloFuerzaResultante)-(resultante.anguloQueLlevo);

	// Sutileza para que el robot gire por el lado de menos giro
	if (difAngle>180) difAngle=difAngle-360;
	if (difAngle<-180) difAngle=360+difAngle;

	/*if (!resultante.avanzaEnGiro) peligro = 1;
	else */ //TODO
  peligro = 0;

	if (distanciaObjetivo==0) { // estamos en el objetivo
		if (DEBUG) printf ("commandVelocity: Estamos en el objetivo\n");
		*myv=0.0;
		*myw=0.0;
		vActual=*myv;
		wActual=*myw;
	} else { // reglas AD-HOC
//		if ((thinPlace ()) && (!overpass())) { // TODO COMPORTAMIENTO VFF VS VENTANA
//			*myv = vMax; 
//			*myw = 0.0; // a toa leche, podemos seguir recto por el sitio estrecho
//		} else { // COMPORTAMIENTO VFF PURO
			if(peligro==1) { // LO MAS URGENTE, SI HAY PELIGRO INMINENTE, GIRAMOS LENTAMENTE
				if (DEBUG) printf ("commandVelocity: Hay Peligro. Giramos lentamente\n");
				if(difAngle<0) {*myv=0.0; *myw=wNegMax/2;}
				else {*myv=0.0; *myw=wPosMax/2;}
			}	else if (abs(difAngle)<limiteTrayectoriaSimilar) {
				if (DEBUG) printf ("commandVelocity: No hay Peligro. Viento en popa\n");
				*myv=vMax; *myw=0.0; // VIENTO EN POPA
			} else if (abs(difAngle)>limiteTrayectoriaDesigual) { // GIRAMOS RAPIDAMENTE
				if (DEBUG) printf ("commandVelocity: Giramos rápidamente\n");
				if (difAngle<0) {*myv=0.0; *myw=wNegMax;}
				else {*myv=0.0; *myw=wPosMax;}
			}	else if (difAngle<0) { // MITAD, MITAD... (FIFTY, FIFTY)
				if (DEBUG) printf ("commandVelocity: Mitad, mitad... (fifty, fifty)\n");
				*myv=vMax/2; *myw=wNegMax/2;
			} else {
				*myv=vMax/2; *myw=wPosMax/2;
			}
	} // fin reglas AD-HOC
}

inline void comportamientoVFF() {
	getAnguloQueLlevo (); // Ángulo que llevo

	getObjetivo ();
	if (DEBUG) printf ("getObjetivo OK\n");
	guardarCoordObjetivo(result); // guardamos la FUERZA ATRACTIVA
	if (DEBUG) printf ("guardarCoordObjetivo OK\n");
	resultante.avanzaEnGiro=TRUE; // si no hay obstaculos por debajo de la seguridad se avanza a la vez que se gira

	calculaResultante(result); // Función importante donde calculamos las fuerzas según la memoria de segmentos
	if (DEBUG) printf ("calculaResultante OK\n");
	checkRobotIsInObjetive (); // Miro si he alcanzado el objetivo
	if (DEBUG) printf ("checkRobotIsInObjetive OK\n");
	getForcesParams (); // Obtenemos los valores de fuerzas, para su posterior pintado
	if (DEBUG) printf ("getForcesParams OK\n");
	commandVelocity (); // Comandamos v y w del robot, según la diferencia de ángulo
}

inline void visualNav_iteration() {
	static char d = 0;
	speedcounter(visualNav_id);

	sleep (1.);

	pthread_mutex_lock(&main_mutex);

	actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

	//TODO: comportamientoVFF (); // TEMPORALMENTE SÓLO HACEMOS ESPIRAL
	
	*myv=60; *myw=6;

	actualPosition.x = myencoders[0];
	actualPosition.y = myencoders[1];

	pthread_mutex_unlock(&main_mutex);
}

/*Importar símbolos*/
inline void visualNav_imports() {
	/* importamos encoders y motores de la base */
	myencoders=(float *)myimport("encoders","jde_robot");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsrun=(runFn)myimport("motors","run");
  motorsstop=(stopFn)myimport("motors","stop");
}

inline void visualNav_stop()
{
	pthread_mutex_lock(&(all[visualNav_id].mymutex));
	put_state(visualNav_id,slept);
	if (DEBUG) printf("visualNav: off\n");
	pthread_mutex_unlock(&(all[visualNav_id].mymutex));
}


inline void visualNav_run(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[visualNav_id].mymutex)); // CERROJO -- LOCK
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[visualNav_id].children[i]=FALSE;
 
  all[visualNav_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) visualNav_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {visualNav_brothers[i]=brothers[i];i++;}
    }
  visualNav_callforarbitration=fn;
  put_state(visualNav_id,notready);
  if (DEBUG) printf("visualNav: on\n");
  visualNav_imports();

	// inmediatamente a continuación vemos su posición
	actualPosition.x = myencoders[0];
	actualPosition.y = myencoders[1];

	lastPosition = actualPosition;

  /* Wake up drivers schemas */	
  colorArun(visualNav_id, NULL, NULL);

  pthread_cond_signal(&(all[visualNav_id].condition));
  pthread_mutex_unlock(&(all[visualNav_id].mymutex)); // CERROJO -- UNLOCK
}

inline void *visualNav_thread(void *not_used) {
	struct timeval a,b;
	long n=0; /* iteration */
	long next,bb,aa;

	for(;;)	{
		pthread_mutex_lock(&(all[visualNav_id].mymutex));

		if (all[visualNav_id].state==slept) {
			pthread_cond_wait(&(all[visualNav_id].condition),&(all[visualNav_id].mymutex));
			pthread_mutex_unlock(&(all[visualNav_id].mymutex));
		}
		else {
			if (all[visualNav_id].state==notready) /* check preconditions. For now, preconditions are always satisfied*/
				put_state(visualNav_id,ready);

			if (all[visualNav_id].state==ready) { /* check brothers and arbitrate. For now this is the only winner */
				put_state(visualNav_id,winner);
	      all[visualNav_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      all[visualNav_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      encodersrun(visualNav_id,NULL,NULL);
	      motorsrun(visualNav_id,NULL,NULL);

				gettimeofday(&a,NULL);
				aa=a.tv_sec*1000000+a.tv_usec;
				n=0;
			}

			if (all[visualNav_id].state==winner) {
				/* I'm the winner and must execute my iteration */
				pthread_mutex_unlock(&(all[visualNav_id].mymutex));
				/* gettimeofday(&a,NULL); */
				n++;
				visualNav_iteration();
				gettimeofday(&b,NULL);
				bb=b.tv_sec*1000000+b.tv_usec;
				next=aa+(n+1)*(long)visualNav_cycle*1000-bb;

				if (next>5000) {
					usleep(next-5000); /* discounts 5ms taken by calling usleep itself, on average */
				}	else {
					usleep(25000); /* If iteration takes a long time, sleep 25 ms to avoid overload */
				}
			}	else { /* just let this iteration go away. overhead time negligible */
				pthread_mutex_unlock(&(all[visualNav_id].mymutex));
				usleep(visualNav_cycle*1000);
			}
		}
	}
}

inline void visualNav_terminate()
{
  pthread_mutex_lock(&(all[visualNav_id].mymutex));
  visualNav_stop();  
  pthread_mutex_unlock(&(all[visualNav_id].mymutex));
  sleep(2);
}

/*Exportar símbolos*/
inline void visualNav_exports() {
  myexport("visualNav","cycle",&visualNav_cycle);
  myexport("visualNav","resume",(void *)visualNav_run);
  myexport("visualNav","suspend",(void *)visualNav_stop);
}

inline void visualNav_init(char *configfile) {
  pthread_mutex_lock(&(all[visualNav_id].mymutex)); // CERROJO -- LOCK

  if (DEBUG) printf("visualNav schema started up\n");
  visualNav_exports();
  put_state(visualNav_id,slept);
  pthread_create(&(all[visualNav_id].mythread),NULL,visualNav_thread,NULL);

  pthread_mutex_unlock(&(all[visualNav_id].mymutex)); // CERROJO -- UNLOCK
}

inline void visualNav_guidisplay() {}

inline void visualNav_hide() {
	all[visualNav_id].guistate=off;
}

/*Callback of window closed*/
inline void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data) {}

inline void visualNav_show() {}
