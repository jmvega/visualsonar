/*
 *  Copyright (C) 2010 Julio M. Vega Pérez 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio M. Vega Pérez <julio.vega@urjc.es>
 */
#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <progeo.h>
#include <glcanvas.h>
#include <unistd.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>
#include <opencv/cv.h>

// Parámetros relativos al tratamiento de la navegación del robot
#define K 500*500                   /* Constantes de repulsión*/
#define K2 1000*1000					/*ANTES 3000*3000 */
//#define K3 4000*4000

#define moduloAtractiva 500          /* Modulo de la fuerza atractiva */

#define alcanceMuyPeligroso 1000*1000
#define alcancePeligroso 2000*2000
#define alcance 5000*5000             /* Distancia a la que las fuerzas repulsivas empiezan a actuar sobre nosotros */
#define alcanceLateral 2000*2000 
#define seguridad 500*500             /* a partir de esta distancia giramos sin avanzar*/
#define seguridadLateral 200*200      /* a partir de esta distancia lateral giramos sin avanzar*/

#define alpha 1.f                    /* Proporcion de la fuerza atractiva en la fuerza resultante */
#define beta  0.f                    /* Proporcion de la fuerza repulsiva en la fuerza resultante */
#define vMax 200
#define wPosMax 30
#define wNegMax -30
#define limiteTrayectoriaSimilar 10
#define limiteTrayectoriaDesigual 80
#define ventanaY 850.00 /* 85 cm de longitud lado Y */
#define frenteY 950.00 /* 95 cm de frente */
#define ventanaX 220.00 /* en total tendrá una longitud X de 44 cm */
#define huecoX 370.00 /* en total tendrá una longitud X de 74 cm nuestra macro ventana */
#define MIN_TAM_SEG_TO_DIVIDE 100

extern void visualNav_init();
extern void visualNav_stop();
extern void visualNav_run(int father, int *brothers, arbitration fn);
extern int visualNav_cycle;

typedef struct {
	int   moduloFuerzaAtractiva;
	float direccionFuerzaAtractiva[2];
	float anguloFuerzaAtractiva;
	int   moduloFuerzaRepulsiva;
	int   direccionFuerzaRepulsiva[2];
	float anguloFuerzaRepulsiva;
	int   moduloFuerzaResultante;
	float direccionFuerzaResultante[2];
	float anguloFuerzaResultante;
	float anguloQueLlevo; 
	int   avanzaEnGiro; /* Sera 1 siempre k la distancia sea > seguridad; si la distancia leida es < seguridad, giraremos sin avanzar */
} TdatosResultante;

typedef struct datosMemoria {
	int horaInicio[SIFNTSC_COLUMNS];
	int masViejo;
	int ultimo;
} TdatosMemoria;

typedef struct HPoint3DInformation {
	HPoint3D position;
  int idColor;
	int real;
	//...
} HPoint3Dinfo;

typedef struct Parallelogram3Dim {
	HPoint3Dinfo p1;
	HPoint3Dinfo p2;
	HPoint3Dinfo p3;
	HPoint3Dinfo p4;
	HPoint3D centroid;
	int isValid;
	//...
} Parallelogram3D;

typedef struct Segment3Dim {
	HPoint3Dinfo start;
	HPoint3Dinfo end;
	float length;
	int idColor;
	int isValid;
	double timestamp;
	int isWellPredicted;
	float traveled; // cuánto lleva viajado el robot desde que lo vió por primera vez
	int isInCache;
	//...
} Segment3D;

struct CacheList {
	Segment3D* segment;
	struct CacheList* next;
};

struct HPoint3DList {
	HPoint3D point;
	struct HPoint3DList* next;
};

