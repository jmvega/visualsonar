/*
 *  Copyright (C) 2009 Julio M. Vega Pérez 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio M. Vega Pérez <julio.vega@urjc.es>
 */

#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>
#include <unistd.h>
#include <string.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <progeo.h>
#include <colorspaces.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_multifit.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "pioneeropengl.h"

#define ROOM_MAX_X 7925.
#define v3f glVertex3f
#define PI 3.141592654
//#define FLT_MAX 999999.9
#define SQUARE(a) (a)*(a)

#define MAX_RADIUS_VALUE 100000000
#define MIN_RADIUS_VALUE 0
#define WHEEL_DELTA 100

#define ANCHO_IMAGEN 320
#define LARGO_IMAGEN 240
#define MAXWORLD 10000.
#define V_MAX 100

// Para el filtrado de blanco sobre verde
#define H_MIN_BLANCO 0.
#define H_MAX_BLANCO 6.18
#define S_MIN_BLANCO 0.
#define S_MAX_BLANCO 0.22
#define V_MIN_BLANCO 128.
#define V_MAX_BLANCO 255.

#define H_MIN_VERDE 0.96
#define H_MAX_VERDE 3.27
#define S_MIN_VERDE 0.26
#define S_MAX_VERDE 1.
#define V_MIN_VERDE 0.
#define V_MAX_VERDE 198.7

#define ENCOD_TO_DEG (3.086/60.) /* CTE que nos pasa de unidades PANTILT a grados */
#define DEG_TO_ENCOD (60./3.086) /* CTE que nos pasa de grados a unidades PANTILT */
#define MAXPAN_POS 95
#define MAXPAN_NEG -95
#define VEL_MAX_PAN 2500.0*ENCOD_TO_DEG
#define VEL_MIN_PAN 350.0*ENCOD_TO_DEG
#define POS_MIN_PAN 40.0
#define POS_MAX_PAN 160.0
#define VEL_MAX_TILT 1000.0*ENCOD_TO_DEG
#define VEL_MIN_TILT 300.0*ENCOD_TO_DEG
#define POS_MIN_TILT 40.0
#define POS_MAX_TILT 120.0

// Parámetros extrínsecos de los distintos elementos del Pioneer + Pantilt + Cámara
#define PANTILT_BASE_HEIGHT 250.
#define ISIGHT_OPTICAL_CENTER -250.
#define TILT_HEIGHT 70.
#define CAMERA_TILT_HEIGHT 120.
#define PANTILT_BASE_X 60.
#define PANTILT_BASE_Y 0.

// Parámetros relacionados con el tratamiento de los segmentos
#define MIN_TAM_SEG 70
#define MAX_TAM_SEG 700
#define MAX_LINES_TO_DETECT 70
#define MAX_LINES_IN_MEMORY 1000
#define MAX_PARALLELOGRAMS_IN_MEMORY 10
#define MAX_PAR 70
#define MAX_DIST 70
#define MAX_PAR_2D 15
#define MAX_DIST_2D 15
#define BORDER_THRESHOLD 60
#define HOUGH_MIN_DIST_SEG 15
#define HOUGH_LINE_THRESHOLD 35
#define MAX_GAP_BETWEEN_SEGMENTS 100
#define COMMON_VERTEX_THRESHOLD 100.

#define MAX_TAM_ARROW 250 // realmente mis flechas miden como máximo (la base) unos 210 mm.
#define MIN_TAM_ARROW 50 // realmente mis flechas miden como máximo (el aspa) unos 100 mm.
#define MIN_TAM_BASE 120

// Parámetros relacionados con la atención visual
#define SALIENCY_INCREMENT 1
#define SALIENCY_DECREMENT 1
#define MAX_SALIENCY 100
#define MIN_SALIENCY 0
#define LIVELINESS_INCREMENT 20
#define LIVELINESS_DECREMENT 10
#define MIN_LIVELINESS 0
#define MAX_LIVELINESS 5000
#define LIVELINESS_TO_DEAD -1
#define PENALTY_FACTOR 200
#define BONUS_FACTOR 100
#define FOLLOW_TIME 0.
#define TIME_TO_FORCED_SEARCH 4
#define TIME_TO_DEAD 50
#define TILT_MAX 90
#define TILT_MIN -90
#define CENTRO_X 260
#define CENTRO_Y 260
#define RADIO_MAX 258
#define RADIO_MIN 0
#define ANCHO_ESCENA_COMPUESTA 520
#define LONGITUDE_NUM_POSITIONS 3
#define MAX_FACES_IN_MEMORY 10
#define MIN_EXP_WIDTH 20
#define MIN_EXP_HEIGHT 20
#define MAX_GAP_FACES 20 // mm. máxima distancia entre caras que se consideran la misma
#define MAX_GAP_ARROWS 20
#define CACHE_SPACE 4000 // mm. de radio sobre el que están los objetos de cache
#define TIME_UPDATE_CACHE 15 // tiempo que transcurre entre cada actualización de cache
#define TIME_TO_SAVE_IMAGE 10 // tiempo que transcurre entre cada fotograma guardado de la memoria de líneas
#define TIME_MAINTENANCE 2

// Parámetros relativos al tratamiento de la navegación del robot
#define K 500*500                   /* Constantes de repulsión*/
#define K2 1000*1000					/*ANTES 3000*3000 */
//#define K3 4000*4000

#define moduloAtractiva 500          /* Modulo de la fuerza atractiva */

#define alcanceMuyPeligroso 1000*1000
#define alcancePeligroso 2000*2000
#define alcance 5000*5000             /* Distancia a la que las fuerzas repulsivas empiezan a actuar sobre nosotros */
#define alcanceLateral 2000*2000 
#define seguridad 500*500             /* a partir de esta distancia giramos sin avanzar*/
#define seguridadLateral 200*200      /* a partir de esta distancia lateral giramos sin avanzar*/

#define alpha 1.f                    /* Proporcion de la fuerza atractiva en la fuerza resultante */
#define beta  0.f                    /* Proporcion de la fuerza repulsiva en la fuerza resultante */
#define vMax 200
#define wPosMax 30
#define wNegMax -30
#define limiteTrayectoriaSimilar 10
#define limiteTrayectoriaDesigual 80
#define MIN_TAM_SEG_TO_DIVIDE 100
#define SEGMENT_LIFE_TIME 300
#define DIST_OVERLAPPING 200 // 20 cm de radio alrededor de flechas o caras, es lo que consideramos de área overlapped
#define MAX_LIFE_INCREMENTS 30000 // mm que ha de andar el robot para olvidar segmentos
#define ventanaY 850.00 /* 85 cm de longitud lado Y */
#define frenteY 950.00 /* 95 cm de frente */
#define ventanaX 220.00 /* en total tendrá una longitud X de 44 cm */
#define huecoX 370.00 /* en total tendrá una longitud X de 74 cm nuestra macro ventana */

extern void visualSonar_init();
extern void visualSonar_stop();
extern void visualSonar_run(int father, int *brothers, arbitration fn);
extern int visualSonar_cycle;

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
} SofReference;

struct image_struct {
	int width;
	int height;
	int bpp;	// bytes per pixel
	char *image;
};

typedef struct HPoint3DInformation {
	HPoint3D position;
  int idColor;
	int real;
	//...
} HPoint3Dinfo;

typedef struct colorRGBInf {
	float R;
	float G;
	float B;
} colorRGB;

typedef struct Segment3Dim {
	HPoint3Dinfo start;
	HPoint3Dinfo end;
	float length;
	int idColor;
	int isValid;
	double timestamp;
	int isWellPredicted;
	float traveled; // cuánto lleva viajado el robot desde que lo vió por primera vez
	int isInCache;
	//...
} Segment3D;

typedef struct Segment2Dim {
	HPoint2D start;
	HPoint2D end;
} Segment2D;

typedef struct Parallelogram3Dim {
	HPoint3Dinfo p1;
	HPoint3Dinfo p2;
	HPoint3Dinfo p3;
	HPoint3Dinfo p4;
	HPoint3D centroid;
	int isValid;
	//...
} Parallelogram3D;

typedef struct Face3Dim {
	HPoint3Dinfo center;
	int isValid;
	//...
} Face3D;

typedef struct Arrow3Dim {
	HPoint3D start; // base de la flecha
	HPoint3D end; // extremo de la flecha (hacia donde apunta)
	colorRGB color;
	int isAttainable; // es atendible? (si está dentro de un cierto radio le prestamos atención de rumbo)
	//...
} Arrow3D;

typedef struct SemiArrow3Dim {
	int isValid;
	Segment3D base; // base (segmento más largo) de la flecha
	Segment3D cross; // uno de las aspas de la flecha
	colorRGB color;
} SemiArrow3D;

enum movement_pantilt {up,down,left,right};

// Parámetros relacionados con la atención visual
int longitudeScenePositions[LONGITUDE_NUM_POSITIONS] = {-39, 0, 39};
int latitudeScenePositions[4] = {31, 14, -14, -31};
enum state {think, search, analizeSearch};

typedef struct {
	float pan;
	float tilt;
} scenePoint;

typedef struct {
	int x;
	int y;
} imagePoint;

typedef struct {
  int x;
  int y;
} t_vector;

struct elementStruct {
	double lastInstant; // último instante de tiempo en su detección
	double firstInstant; // primer instante de tiempo en su detección
	float latitude; // posición absoluta del pantilt, en eje tilt
	float longitude; // posición absoluta del pantilt, en eje pan
	int scenePos; // it can be left, center or right, depends on pantilt pos where face was detected
	float saliency;
	float liveliness;
	int type; // 0 = elemento virtual; 1 = rectángulo; 2 = face; 3 = arrow
	int isVisited;
	Parallelogram3D parallelogram;
	Face3D face;
	Arrow3D arrow;
	struct elementStruct* next;
};

struct HPoint2DList {
	HPoint2D point;
	struct HPoint2DList* next;
};

struct HPoint3DList {
	HPoint3D point;
	struct HPoint3DList* next;
};

struct Segment2DList {
	Segment2D segment;
	struct Segment2DList* next;
};

struct Segment2y3DList {
	Segment2D segment2D;
	Segment3D* segment3D;
	struct Segment2y3DList* next;
};

struct CacheList {
	Segment3D* segment;
	struct CacheList* next;
};

/********************************************************************************************************/
/**************************E S T R U C T U R A S    D E    N A V E G A C I Ó N***************************/
/********************************************************************************************************/
typedef struct {
	int   moduloFuerzaAtractiva;
	float direccionFuerzaAtractiva[2];
	float anguloFuerzaAtractiva;
	int   moduloFuerzaRepulsiva;
	int   direccionFuerzaRepulsiva[2];
	float anguloFuerzaRepulsiva;
	int   moduloFuerzaResultante;
	float direccionFuerzaResultante[2];
	float anguloFuerzaResultante;
	float anguloQueLlevo; 
	int   avanzaEnGiro; /* Sera 1 siempre k la distancia sea > seguridad; si la distancia leida es < seguridad, giraremos sin avanzar */
} TdatosResultante;

typedef struct datosMemoria {
	int horaInicio[SIFNTSC_COLUMNS];
	int masViejo;
	int ultimo;
} TdatosMemoria;
