/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This schema was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "visualSonar.h"

int visualSonar_brothers[MAX_SCHEMAS];
arbitration visualSonar_callforarbitration;

int DEBUG = 0;
int idRandColor = 0;
// Control estático de los movimientos de traslación/rotación del robot
int moveAndFlash = 0;
int traslacion = 0, rotacion = 0;
int flashImage = TRUE;

// Variables para guardar las matrices K, R y T de nuestra cámara:
gsl_matrix *K_1,*R_1;
gsl_vector* x_1;
HPoint2D progtest2D;
HPoint3D progtest3D;

struct HSV* myHSV;
TPinHoleCamera virtualcam0; /* para ver el minimundillo */
TPinHoleCamera virtualcam1; /* para moverme por el mundillo */
TPinHoleCamera virtualcam2; /* para ver el entorno circundante al robot */
TPinHoleCamera roboticLabCam0; // cámaras del techo del laboratorio
TPinHoleCamera roboticLabCam1; //
TPinHoleCamera roboticLabCam2; //
TPinHoleCamera roboticLabCam3; //
TPinHoleCamera ceilLabCam;
TPinHoleCamera robotCamera;
TPinHoleCamera robotCamera2;
TPinHoleCamera *myCamera; // puntero temporal
HPoint2D pixel_camA;
HPoint3D myfloor[18*2]; /* mm */
int myfloor_lines=0;

HPoint2D lastPosition;
float coveredDistance;
static SofReference mypioneer;
int actualCameraView; // identificador de la cámara de la escena actual
int showPredictions, showBorders, showRefuted, processImageButton; // booleanos para tales botones

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

char *configFile;
char imageName[20] = "memoryImage";
char imageCharNumber[4] = "\0";
char imageFormat[5] = ".jpg\0";

HPoint2D myActualPoint2D; // variables para el cálculo de los rayos virtuales
HPoint3D myActualPoint3D;
HPoint2D myStartPoint2D;
HPoint2D myEndPoint2D;
HPoint3D myStartPoint3D;
HPoint3D myEndPoint3D;
HPoint3D cameraPos3D;
HPoint2D myPoint2D;
HPoint3D myPoint3D;
HPoint3D intersectionPoint;
HPoint3D intersectionStartPoint;
HPoint3D intersectionEndPoint;
HPoint2D windowA, windowB, windowC, windowD, windowE, windowF, windowG, windowH, windowI, windowJ; // Puntos clave de la ventana de seguridad

Segment3D groundSegments3D [MAX_LINES_IN_MEMORY];
//SemiArrow3D semiArrows3D [MAX_LINES_IN_MEMORY/2];
//Parallelogram3D groundParallelograms3D [MAX_PARALLELOGRAMS_IN_MEMORY];
int numSegments, incNumSegments;//, numParallelograms, incNumParallelograms, numFaces, incNumFaces;

int primerPunto = FALSE, segundoPunto = FALSE, tercerPunto = FALSE, cuartoPunto = FALSE;
float distanciaPlanoImagen, incremento;
int numIterations = 0;
int imageNumber;

HPoint2D mouse_on_visualSonarcanvas;

pthread_mutex_t main_mutex;
/*GTK variables*/
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkWidget *visualSonarCanvas;

GtkImage *filteredImage;

float sliderPANTILT_BASE_HEIGHT, sliderISIGHT_OPTICAL_CENTER, sliderTILT_HEIGHT, sliderCAMERA_TILT_HEIGHT, sliderPANTILT_BASE_X, sliderThreshold;

struct image_struct *imageAfiltered = NULL;
struct image_struct *filteredImageRGB = NULL;

/*Some control variable*/
char *image;

int visualSonar_id=0; 
int visualSonar_brothers[MAX_SCHEMAS];
arbitration visualSonar_callforarbitration;
int visualSonar_cycle=100; /* ms */

char **myActualCamera;

int *mycolorAwidth		= NULL;
int *mycolorAheight		= NULL;
char **mycolorA;
float *myencoders=NULL;
float *myv=NULL;
float *myw=NULL;
runFn colorArun, encodersrun, motorsrun;
stopFn colorAstop, encodersstop, motorsstop;
float *mypan_angle=NULL, *mytilt_angle=NULL;  /* degs */
float *mylongitude=NULL; /* degs, pan angle */
float *mylatitude=NULL; /* degs, tilt angle */
float *mylongitude_speed=NULL;
float *mylatitude_speed=NULL;
float *max_pan=NULL;
float *min_pan=NULL;
float *max_tilt=NULL;
float *min_tilt=NULL;
runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;

int botonPulsado;

float old_x=0., old_y=0.;
float longi_foa = 0.0;
float lati_foa = 0.0;
float foax = 0.0;
float foay = 0.0;
float foaz = 0.0;
float xcam = -1.0;
float ycam = -1.0;
float zcam = -1.0;
float lati = 0.0;
float longi = 0.0;
int foa_mode, cam_mode;
float radius = 500;
float radius_old;
int centrado = 0;
float t = 0.5; // lambda
float phi = 0., theta = 0.;
int boton_pulsado;
float tiltAngle, panAngle;
float speed_y, speed_x;
int completedMovement;

int pantiltStill;
int robotStill;
int comeFromCenter;
int last_movement;
int last_full_movement;

int numFlashes;

double actualInstant;
double stopInstant;
double whatsTheTimeBefore, whatsTheTimeAfter;

// Variables relativas a la atención visual
static CvMemStorage* storage = 0;
static CvMemStorage* storageTmp = 0;
static CvHaarClassifierCascade* cascade = 0;
const char* cascade_name = "haarcascade_frontalface_alt.xml";
IplImage *imgLocal = NULL, *memoryImage = NULL;
CvMat img; // Envoltura a mycolorA
CvSeq *facesTmp, *facesAux;
HPoint2D faceCenter;
double timeForced;
double timeToForcedSearch;
double startToFollowTime, actualFollowTime;
double timeToUpdateCache, timeToSaveImage, timeToMaintenance;
int completedSearch;
int analizedImage;
int checkedParallelogram;
int checkedFace;
int checkedArrow;
int completedTrack;
int isForcedSearch;
int myActualState;
struct elementStruct* myElements; // apuntará al inicio de la lista de elementos
struct elementStruct* myMaxSaliency; // apuntará al elemento de mayor saliencia
struct elementStruct* myPrevMaxSaliency; // apuntará al elemento anterior (en la lista) al de mayor saliencia
struct elementStruct *myActualElement;
struct Segment2y3DList* seg2y3Dlist;
struct HPoint3DList *segPointList;
struct CacheList *cache; // lista de elementos en cache
int elementsStructCounter;
int randomPosition;
int nextLatitude;
int nextLongitude;
Face3D groundFaces3D [MAX_FACES_IN_MEMORY];

/************************************************************************************************************/
/**************** V A R I A B L E S   D E L   M O V I M I E N T O   D E L   P I O N E E R *******************/
/************************************************************************************************************/
TdatosResultante resultante;
TdatosResultante *result;
short distanciaObjetivo;
float xRep,yRep,xAtr,yAtr,xRes,yRes;
float difAngle;
float vActual,wActual;
float peligro;
Tvoxel miObjetivo, fuerzaResultante, repulsiva, atractiva;
double navigationTime;

void printCameraInformation (TPinHoleCamera* actualCamera) {
	if (DEBUG) printf ("CAMERA INFORMATION\n");
	if (DEBUG) printf ("==================\n");
	if (DEBUG) printf ("Position = [%0.2f, %0.2f, %0.2f]\n", actualCamera->position.X, actualCamera->position.Y, actualCamera->position.Z);
	if (DEBUG) printf ("FOA = [%0.2f, %0.2f, %0.2f]\n", actualCamera->foa.X, actualCamera->foa.Y, actualCamera->foa.Z);
	if (DEBUG) printf ("fdist = [%0.2f, %0.2f], u0 = %0.2f, v0 = %0.2f, roll = %0.2f\n", actualCamera->fdistx, actualCamera->fdisty, actualCamera->u0, actualCamera->v0, actualCamera->roll);
	if (DEBUG) printf ("K matrix:\n");
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->k11, actualCamera->k12, actualCamera->k13, actualCamera->k14);
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->k21, actualCamera->k22, actualCamera->k23, actualCamera->k24);
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->k31, actualCamera->k32, actualCamera->k33, actualCamera->k34);
	if (DEBUG) printf ("RT matrix:\n");
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt11, actualCamera->rt12, actualCamera->rt13, actualCamera->rt14);
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt21, actualCamera->rt22, actualCamera->rt23, actualCamera->rt24);
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt31, actualCamera->rt32, actualCamera->rt33, actualCamera->rt34);
	if (DEBUG) printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt41, actualCamera->rt42, actualCamera->rt43, actualCamera->rt44);
}

inline void drawCross(struct image_struct *image, HPoint2D p2d, int side,	unsigned char r, unsigned char g, unsigned char b) {
		int i, offset;

		// linea vertical
		for (i=-side; i<side+1; i++) {
			offset = image->width * ((int)p2d.y+i) * image->bpp + ((int)p2d.x * image->bpp);
			image->image[offset + 0] = b;
			image->image[offset + 1] = g;
			image->image[offset + 2] = r;
		}

		// linea horizontal
		for (i=-side; i<side+1; i++) {
			offset = image->width * (int)p2d.y * image->bpp + (((int)p2d.x+i) * image->bpp);
			image->image[offset + 0] = b;
			image->image[offset + 1] = g;
			image->image[offset + 2] = r;
		}
}

/*CALLBACKS*/
void camera1Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 1;
}

void camera1Button_released (GtkButton *button, gpointer user_data) {}

void camera2Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 2;
}

void camera2Button_released (GtkButton *button, gpointer user_data) {}

void camera3Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 3;
}

void camera3Button_released (GtkButton *button, gpointer user_data) {}

void camera4Button_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 4;
}

void camera4Button_released (GtkButton *button, gpointer user_data) {}

void ceilCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 5;
}

void ceilCameraButton_released (GtkButton *button, gpointer user_data) {}

void userCameraButton_pressed (GtkButton *button, gpointer user_data) {
	GtkToggleButton *myCameraTempButton;

	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);
	myCameraTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
	gtk_toggle_button_set_active (myCameraTempButton, FALSE);

  actualCameraView = 0;
}

void userCameraButton_released (GtkButton *button, gpointer user_data) {}

inline static gboolean button_press_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
  event = (GdkEventButton*)event;
  old_x=event->x;
  old_y=event->y;

  if (2 == event->button){
  	boton_pulsado=2;
  	flashImage = TRUE;
 	  return TRUE;
  } else {
  	flashImage = FALSE;
  	if (1 == event->button){
  	  boton_pulsado=1;
  	  cam_mode = 1;
   	  foa_mode = 0;
	  } else if (3 == event->button) {
	    boton_pulsado=3;
	    foa_mode = 1;
	    cam_mode = 0;
	  }
		return TRUE;
  }
}

inline static gboolean motion_notify_event (GtkWidget *widget, GdkEventButton *event, gpointer data) {
   float x=event->x;
   float y=event->y;
  
	event = (GdkEventButton*)event;

	if (2==boton_pulsado) {
		flashImage = TRUE;
	  return TRUE;
	}	

	mouse_on_visualSonarcanvas.x = ((event->x*360/visualSonarCanvas->allocation.width)-180);
	mouse_on_visualSonarcanvas.y = ((event->y*-180/visualSonarCanvas->allocation.height)+90);
	//if (DEBUG) printf ("Valores de mouse_on_canvas: %0.2f y %0.2f \n",mouse_on_visualSonarcanvas.x,mouse_on_visualSonarcanvas.y);
 
	if (GDK_BUTTON1_MASK){ /*Si está pulsado el botón 1*/
      theta -= x - old_x;
      phi -= y - old_y;
      gtk_widget_queue_draw (widget);
      old_x=x;
      old_y=y;
   }
 return TRUE;
}

inline static gboolean scroll_event (GtkRange *range, GdkEventScroll *event, gpointer data){
	if (event->direction == GDK_SCROLL_DOWN){
	if (radius > MIN_RADIUS_VALUE)
   	radius-=WHEEL_DELTA;
   }
   if (event->direction == GDK_SCROLL_UP){
     if (radius<MAX_RADIUS_VALUE){
      radius+=WHEEL_DELTA;
       }
   }
   if (radius < 0.5) radius = 0.5;
   gtk_widget_queue_draw(GTK_WIDGET((GtkWidget *)data));
   
   return TRUE;
}

inline struct image_struct *create_image(int width, int height, int bpp) {
	struct image_struct *w;

	w = (struct image_struct*) malloc(sizeof(struct image_struct));
	w->width = width;
	w->height = height;
	w->bpp = bpp;
	w->image = (char*) malloc(width * height * bpp);

	return w;
}

inline void remove_image(struct image_struct *w) {
	free(w->image);
	free(w);
}

/** prepare2draw ************************************************************************
* Prepare an image to draw it with a GtkImage in BGR format with 3 pixels per byte.	*
*	src: source image								*
*	dest: destiny image. It has to have the same size as the source image and	*
*	      3 bytes per pixel.
*****************************************************************************************/
inline void prepare2draw (struct image_struct *src, struct image_struct *dest) {
	int i;

	for (i=0; i<src->width*src->height; i++) {
		dest->image[i*dest->bpp+0] = src->image[i*src->bpp+2];
		dest->image[i*dest->bpp+1] = src->image[i*src->bpp+1];
		dest->image[i*dest->bpp+2] = src->image[i*src->bpp+0];
	}
}

inline void notPrepare2draw (struct image_struct *src, struct image_struct *dest) {
	int i;

	for (i=0; i<src->width*src->height; i++) {
		dest->image[i*dest->bpp+0] = src->image[i*src->bpp+0];
		dest->image[i*dest->bpp+1] = src->image[i*src->bpp+1];
		dest->image[i*dest->bpp+2] = src->image[i*src->bpp+2];
	}
}

inline unsigned long int dameTiempo() {
	struct timeval t; 

	gettimeofday(&t,NULL);
	return t.tv_sec*1000000 + t.tv_usec;	
}

inline void drawRobocupLines () {
	glLineWidth(5.5f);
	glColor3f(1.f, 1.f, 1.f);

  glBegin(GL_LINES);
  v3f(2710., 960., 0.);
  v3f(6720., 960., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(6720., 960., 0.);
  v3f(6720., 3960., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(6720., 3960., 0.);
  v3f(2710., 3960., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(2710., 3960., 0.);
  v3f(2710., 960., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(6210., 3960., 0.);
  v3f(6210., 3360., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(6210., 3360., 0.);
  v3f(3210., 3360., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(3210., 3360., 0.);
  v3f(3210., 3960., 0.);
  glEnd();

	// Línea verde auxiliar
  glBegin(GL_LINES);
  v3f(2710., 2580., 0.);
  v3f(6720., 2580., 0.);
  glEnd();
	
	// Aspa del punto de penalty
  glBegin(GL_LINES);
  v3f(4700., 2185., 0.);
  v3f(4740., 2185., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(4720., 2165., 0.);
  v3f(4720., 2205., 0.);
  glEnd();

	// Aspa del punto verde, quemado
  glBegin(GL_LINES);
  v3f(5950., 2310., 0.);
  v3f(5990., 2310., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(5970., 2290., 0.);
  v3f(5970., 2330., 0.);
  glEnd();

	// Semicírculo central
  glBegin(GL_LINES);
  v3f(4720., 940., 0.);
  v3f(4720., 980., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(5320., 960., 0.);
  v3f(5080., 1440., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(5080., 1440., 0.);
  v3f(4720., 1565., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(4720., 1560., 0.);
  v3f(4360., 1440., 0.);
  glEnd();

  glBegin(GL_LINES);
  v3f(4360., 1440., 0.);
  v3f(4120., 960., 0.);
  glEnd();
}

inline void drawFloor () {
	int i;
	glLineWidth(0.5f);
	glColor3f(0.5f, 0.5f, 0.5f);

	glBegin(GL_LINES);
	for(i=0;i<((int)MAXWORLD+1);i=i+10) {
		v3f(-(int)MAXWORLD*100/2.+(float)i*100,-(int)MAXWORLD*100/2.,0.);
		v3f(-(int)MAXWORLD*100/2.+(float)i*100,(int)MAXWORLD*100/2.,0.);
		v3f(-(int)MAXWORLD*100/2.,-(int)MAXWORLD*100/2.+(float)i*100,0.);
		v3f((int)MAXWORLD*100/2.,-(int)MAXWORLD*100/2.+(float)i*100,0.);
	}
	glEnd();
}

inline void drawMyLines () {
  glLineWidth(1.5f);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	glColor3f (1.,1.,1.);

	// Borde exterior del recinto
  glBegin(GL_LINES);
  v3f(0., 0., 0.000000);
  v3f(2600., 0., 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2600., 0., 0.000000);
  v3f(2600., 1400., 0.000000);
  glEnd();

  glBegin(GL_LINES);
  v3f(2600., 1400., 0.000000);
  v3f(0., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(0., 1400., 0.000000);
  v3f(0., 0., 0.000000);
	glEnd();

	// Líneas transversales
  glBegin(GL_LINES);
  v3f(500., 0., 0.000000);
  v3f(500., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1000., 0., 0.000000);
  v3f(1000., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1500., 0., 0.000000);
  v3f(1500., 1400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(2000., 0., 0.000000);
  v3f(2000., 1400., 0.000000);
	glEnd();

	// Triángulos rectos
	// 1
  glBegin(GL_LINES);
  v3f(800., 100., 0.000000);
  v3f(500., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(800., 750., 0.000000);
  v3f(500., 400., 0.000000);
	glEnd();

	// 2
  glBegin(GL_LINES);
  v3f(1350., 85., 0.000000);
  v3f(1000., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1350., 800., 0.000000);
  v3f(1000., 400., 0.000000);
	glEnd();

	// 3
  glBegin(GL_LINES);
  v3f(1850., 75., 0.000000);
  v3f(1500., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(1900., 800., 0.000000);
  v3f(1500., 400., 0.000000);
	glEnd();

	// 4
  glBegin(GL_LINES);
  v3f(2350., 85., 0.000000);
  v3f(2000., 400., 0.000000);
	glEnd();

  glBegin(GL_LINES);
  v3f(2400., 850., 0.000000);
  v3f(2000., 400., 0.000000);
	glEnd();
}

/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
inline void linePlaneIntersection (HPoint3D A, HPoint3D B) {
	HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...

	A.X = A.X;
	A.Y = A.Y;
	A.Z = A.Z;

	B.X = B.X;
	B.Y = B.Y;
	B.Z = B.Z;

	v.X = (B.X - A.X);
	v.Y = (B.Y - A.Y);
	v.Z = (B.Z - A.Z);

	// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
	intersectionPoint.Z = 0.; // intersectionPoint.Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
	t = (-A.Z) / (v.Z);

	intersectionPoint.X = A.X + (t*v.X);
	intersectionPoint.Y = A.Y + (t*v.Y);
	intersectionPoint.H = 1.;
}

inline float distanceBetweenPoints (HPoint3D p1, HPoint3D p2) {
  float dist;

  dist = abs(sqrt(pow(p2.X-p1.X,2) + pow(p2.Y-p1.Y,2) + pow(p2.Z-p1.Z,2)));
  return dist;
}

inline void getMaximizedSegment (HPoint3D *seg1Start, HPoint3D *seg1End, HPoint3D *seg2Start, HPoint3D *seg2End, HPoint3D *startPoint, HPoint3D *endPoint) {

	HPoint3D* myArray[4] = {seg1Start, seg1End, seg2Start, seg2End};
	float max = 0;
	int i, j;
	float value;

	for (i = 0; i < 4; i ++) {
		for (j = i+1; j < 4; j ++) {
			value = abs(distanceBetweenPoints (*myArray[i], *myArray[j]));
			if (value > max) {
				max = value;
				*startPoint = *myArray[i];
				*endPoint = *myArray[j];
			}
		}
	}
	// Finally, we'll get two points in order to maximize distance between them
}

inline int overlapping (Segment3D segment, HPoint3Dinfo proy1, HPoint3Dinfo proy2, int ubi1, int ubi2, float dist1, float dist2, int isIn2D) {
	int found = 1;
	float d1, d2;
	HPoint3D startPoint, endPoint;
	float maxPar, maxDist;

	if (isIn2D) { // puede que vengamos de una comparación de segmentos 2D, en cuyo caso los límites de paralelismo y distancia varían
		maxPar = MAX_PAR_2D;
		maxDist = MAX_DIST_2D;
	} else {
		maxPar = MAX_PAR;
		maxDist = MAX_DIST;
	}

	//printf ("overlapping: ubi1 = %i, ubi2 = %i, dist1 = %0.2f, dist2 = %0.2f\n", ubi1, ubi2, dist1, dist2);
	if ((abs(dist1)<maxPar) && (abs(dist2)<maxPar)) { // They're parallel segments. Now we check if they're in distance
		// Posible cases: 1-Two intermediate points; 2-One intermediate point; 3-Any intermediate point
		getMaximizedSegment (&segment.start.position, &segment.end.position, &proy1.position, &proy2.position, &startPoint, &endPoint);

		if ((ubi1==0)&&(ubi2==0)) { // 1-Two intermediate points. We don't do anything
			//printf ("found!\n");
			found = 1;
		} else { // 2-One intermediate point
			if ((ubi1==0)||(ubi2==0)) {
				if ((ubi1==0)&&(ubi2==1)) {
					segment.end = proy2;
				} else {
					if ((ubi1==0)&&(ubi2==-1)) {
						segment.start = proy2; 
					} else {
						if ((ubi1==1)&&(ubi2==0)) {
							segment.end = proy1; 
						} else {
							if ((ubi1==-1)&&(ubi2==0)) {
								segment.start = proy1; 
							}
						}
					}
				}
			} else { // 3-Any intermediate point
				if ((ubi1==-1)&&(ubi2==1)) { // One on the right side and the other one on the left side
					segment.start = proy1;
					segment.end = proy2;
				} else {
					if ((ubi1==1)&&(ubi2==-1)) {
						segment.start = proy2;
						segment.end = proy1;
					} else {
						if (ubi1==1) { // segment is on the right side
							d1 = distanceBetweenPoints(proy1.position, segment.end.position);
							d2 = distanceBetweenPoints(proy2.position, segment.end.position);

							if ((abs(d1)<maxDist)||(abs(d2)<maxDist)) {
								if (d1<d2) {
									segment.end = proy2; 
								} else {
									segment.end = proy1;
								}
							} else { // They don't fusion themselves
								found = 0;
							}
						} else {    
							d1 = distanceBetweenPoints(proy1.position, segment.start.position);
							d2 = distanceBetweenPoints(proy2.position, segment.start.position); 

							if ((abs(d1)<maxDist)||(abs(d2)<maxDist)) {
								if (d1<d2) {
									segment.start = proy2; 
								} else {
									segment.start = proy1;
								}
							} else { // They don't fusion themselves
								found = 0;
							}
						} 
					}
				} 
			}
		}
		segment.start.position = startPoint;
		segment.end.position = endPoint;
	} else { // They aren't parallel lines
		found = 0;
	}

	return found;
}

inline float segmentMagnitude (Segment3D segment) {
	HPoint3D vector;

	vector.X = segment.start.position.X - segment.end.position.X;
	vector.Y = segment.start.position.Y - segment.end.position.Y;
	vector.Z = segment.start.position.Z - segment.end.position.Z;
	vector.H = 1.;

	return (float) (sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z));
}

inline int distancePointLine (HPoint3Dinfo Point, Segment3D segment, HPoint3Dinfo *Intersection, float *Distance) {
	float LineMag;
	float U;
	int res;

	LineMag = segmentMagnitude (segment);

	U = ( ( ( Point.position.X - segment.start.position.X ) * ( segment.end.position.X - segment.start.position.X ) ) +
		    ( ( Point.position.Y - segment.start.position.Y ) * ( segment.end.position.Y - segment.start.position.Y ) ) +
		    ( ( Point.position.Z - segment.start.position.Z ) * ( segment.end.position.Z - segment.start.position.Z ) ) ) /
			  ( LineMag * LineMag );

	Intersection->position.X = segment.start.position.X + U * ( segment.end.position.X - segment.start.position.X );
	Intersection->position.Y = segment.start.position.Y + U * ( segment.end.position.Y - segment.start.position.Y );
	Intersection->position.Z = segment.start.position.Z + U * ( segment.end.position.Z - segment.start.position.Z );

	if( U >= 0.0f || U <= 1.0f ) {
		res = 0;
	} else {
		if (U < 0.) { // Intersection will be after segment
			res = -1;
		} else { // Intersection will be before segment
			res = +1;
		}
	}

	*Distance = distanceBetweenPoints(Point.position, Intersection->position);
	return res;
}

inline int areTheSameSegment (Segment3D s1, Segment3D s2) {
	int areTheSame = 0;
	if ((((float)s1.start.position.X==(float)s2.start.position.X) && 
			 ((float)s1.start.position.Y==(float)s2.start.position.Y) && 
			 ((float)s1.start.position.Z==(float)s2.start.position.Z) && 
			 ((float)s1.end.position.X==(float)s2.end.position.X) &&
			 ((float)s1.end.position.Y==(float)s2.end.position.Y) && 
			 ((float)s1.end.position.Z==(float)s2.end.position.Z))	|| 
			(((float)s1.start.position.X==(float)s2.end.position.X) && 
			 ((float)s1.start.position.Y==(float)s2.end.position.Y) && 
			 ((float)s1.start.position.Z==(float)s2.end.position.Z) && 
			 ((float)s1.end.position.X==(float)s2.start.position.X) &&
			 ((float)s1.end.position.Y==(float)s2.start.position.Y) && 
			 ((float)s1.end.position.Z==(float)s2.start.position.Z))) { // they're the same segment
		areTheSame = 1;
	}

	return (areTheSame);
}

inline int mergeSegment (Segment3D segment, int* areTheSame) {
	int found=0;
	HPoint3Dinfo proy1, proy2;
	int ubi1, ubi2;
	float dist1, dist2;
	struct CacheList *p;

	p = cache;
	(*areTheSame) = 0; // suponemos que no es el mismo con el que se fusionará

	while ((p != NULL) && (!found))	{
		if (p->segment->isValid == 1) {
			if (areTheSameSegment (*(p->segment), segment)) {
				found = 1;
				(*areTheSame) = 1;
			} else {
				// Check every segment in memory, with segment. Calculate intersection point between segment i and segment.start perpendicular
				ubi1 = distancePointLine (segment.start, *(p->segment), &proy1, &dist1);
				ubi2 = distancePointLine (segment.end, *(p->segment), &proy2, &dist2);

				found = overlapping (*(p->segment), proy1, proy2, ubi1, ubi2, dist1, dist2, 0); // If they're parallel lines -> we finish checking

				//printf ("mergeSegment s1=[%0.2f, %0.2f, %0.2f]-[%0.2f, %0.2f, %0.2f], s2=[%0.2f, %0.2f, %0.2f]-[%0.2f, %0.2f, %0.2f], overlapped = %i\n", p->segment->start.position.X, p->segment->start.position.Y, p->segment->start.position.Z, p->segment->end.position.X, p->segment->end.position.Y, p->segment->end.position.Z, segment.start.position.X, segment.start.position.Y, segment.start.position.Z, segment.end.position.X, segment.end.position.Y, segment.end.position.Z, found);
			}
			if (found) { // si hemos logrado encontrarlo, le hacemos merge de modo que reseteamos el tiempo y travel
				p->segment->timestamp = segment.timestamp;
				p->segment->traveled = segment.traveled;
			}
		}
		p = p->next;
	}

	return found;
}

inline void printPoint (HPoint3D point) {
	if (DEBUG) printf ("(%0.2f, %0.2f, %0.2f)", point.X, point.Y, point.Z);
}

inline void printParallelogram (Parallelogram3D par1) {
	HPoint3D* myArray1[4] = {&(par1.p1.position), &(par1.p2.position), &(par1.p3.position), &(par1.p4.position)};
	int i;

	if (DEBUG) printf ("Parallelogram %p\n", &par1);
	if (DEBUG) printf ("=====================\n");
	if (DEBUG) printf ("[");
	for (i = 0; i < 4; i ++) {
		printPoint (*myArray1[i]);
		if (DEBUG) printf (", ");
	}
	if (DEBUG) printf ("]\n\n");
}

inline int areTheSameParallelogram (Parallelogram3D par1, Parallelogram3D par2) {
	HPoint3D* myArray1[4] = {&(par1.p1.position), &(par1.p2.position), &(par1.p3.position), &(par1.p4.position)};
	HPoint3D* myArray2[4] = {&(par2.p1.position), &(par2.p2.position), &(par2.p3.position), &(par2.p4.position)};
	int i, j, found;
	float value;
	int equalPointsCounter = 0;

	//printParallelogram (par1);
	//printParallelogram (par2);

	for (i = 0; i < 4; i ++) {
		j = 0;
		found = FALSE;
		while ((j < 4) && (!found)) {
			value = abs(distanceBetweenPoints (*myArray1[i], *myArray2[j]));
			if (value < 20.) {
				equalPointsCounter ++;
				found = TRUE;
			}
			j ++;
		}
	}

	return (equalPointsCounter == 4);
}

inline void point2angle () {
	HPoint3D p;
	float pan, tilt;

	if (myMaxSaliency->type == 1)
		p = myMaxSaliency->parallelogram.centroid;
	else if (myMaxSaliency->type == 2)
		p = myMaxSaliency->face.center.position;
	else if (myMaxSaliency->type == 3) {
		p = myMaxSaliency->arrow.end;
		if (DEBUG) printf ("point2angle: an arrow objetive on [%0.2f, %0.2f] has been established\n", p.X, p.Y);
	}

	p.X = p.X - robotCamera.position.X;
	p.Y = p.Y - robotCamera.position.Y;
	p.Z = 0. - robotCamera.position.Z;
	p.H = 1.;

	if (p.X == 0.) pan = 0.;
	else pan = atan(p.Y/p.X);

	if (p.X == 0.) tilt = 0.;
	else tilt = atan (p.Z/p.X); // La Z siempre saldrá negativa, porque estará más alta que el punto

	myMaxSaliency->longitude = pan * 180.0/PI;
	myMaxSaliency->latitude = tilt * 180.0/PI;
	if (DEBUG) printf ("p = [%0.2f, %0.2f, %0.2f], pan = %0.2f, tilt = %0.2f, New coordinates: [%0.2f, %0.2f]\n", p.X, p.Y, p.Z, pan, tilt, myMaxSaliency->latitude, myMaxSaliency->longitude);
}

inline void getPolygonCentroid (Parallelogram3D* parallelogram) {
	HPoint3D vertexList[5]; // Remember: Counter Clockwise Winding in order to get the centroid
  int i, j;
  double sum = 0.;
  double area = 0.;
	HPoint3D centroid;

	vertexList[0] = parallelogram->p2.position;
	vertexList[1] = parallelogram->p1.position;
	vertexList[2] = parallelogram->p3.position;
	vertexList[3] = parallelogram->p4.position;
	vertexList[4] = vertexList[0];

  centroid.X = 0.;
  centroid.Y = 0.;
  centroid.Z = 0.;
	centroid.H = 1.;

  for (i = 0; i < 5; i++) {
		j = (i+1)%5;
    area = vertexList[i].X * vertexList[j].Y - vertexList[i].Y * vertexList[j].X;
    sum += area;
    centroid.X += (vertexList[i].X + vertexList[j].X) * area;
    centroid.Y += (vertexList[i].Y + vertexList[j].Y) * area;
  }
  sum *= 3.;
  centroid.X /= sum;
  centroid.Y /= sum;

	parallelogram->centroid = centroid;
	if (DEBUG) printf ("Centroid = [%0.2f, %0.2f, %0.2f]\n", centroid.X, centroid.Y, centroid.Z);
}

inline void fillParallelogramElement (Parallelogram3D parallelogram) {
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));
	if (myActualElement == NULL) if (DEBUG) printf ("Parallelogram malloc failed\n");
	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;

	// Si estamos aquí es porque acabamos de ver un paralelogramo, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima

	myActualElement->type = 1; // elemento paralelogramo
	myActualElement->parallelogram = parallelogram;
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline int areTheSameArrow (Arrow3D arrow1, Arrow3D arrow2) {
	return (distanceBetweenPoints (arrow1.end, arrow2.end) < MAX_GAP_ARROWS);
}

inline int areTheSameFace (Face3D face1, Face3D face2) {
	return (distanceBetweenPoints (face1.center.position, face2.center.position) < MAX_GAP_FACES);
}

inline void isMergedElementAndInsert () {
	struct elementStruct *r;
	int found = 0;

	if (myActualElement != NULL) {
		r = myElements;
		if (r != NULL) { // BUSCAMOS UN SIMILAR AL QUE INTENTAMOS INSERTAR
			while ((r->next != NULL)&&(!found)) { // Llegamos hasta el último de la cola, si no lo encontramos antes
				if ((r->type == myActualElement->type) && 
						(((r->type == 1) && (areTheSameParallelogram (r->parallelogram, myActualElement->parallelogram))) ||
						((r->type == 3) && (areTheSameArrow (r->arrow, myActualElement->arrow))) ||
						((r->type == 2) && (areTheSameFace (r->face, myActualElement->face))))) {
					r->saliency -= SALIENCY_DECREMENT;
					r->liveliness += LIVELINESS_INCREMENT;
					found = 1; // ya existía de antes
					if (DEBUG) printf ("Element has been merged with another similar one!\n");
				}	else {
					r = r->next;
				}
			}
		}


		if ((myElements == NULL) || ((myElements != NULL) && (!found))) { // Inserción: no ha sido encontrado en los existentes
			if (myElements==NULL) { // List is null
				myElements = myActualElement;
				myActualElement->next = NULL;
			} else if (myElements != NULL) {
				myActualElement->next = myElements;
				myElements = myActualElement;
			}
			elementsStructCounter ++;
			if (DEBUG) printf ("Element type %i has been inserted correctly as %i!\n", myActualElement->type, elementsStructCounter);
		}
	} else
		if (DEBUG) printf ("Error: element is NULL\n");
}

inline void insertParallelogramOnAttentionSystem (Parallelogram3D parallelogram) {
	getPolygonCentroid (&parallelogram); // después podremos calcular el foco de atención
	fillParallelogramElement (parallelogram);
	isMergedElementAndInsert ();
}

inline void storeSegment3D (Segment3D segment) {
	int isMerge = 0, pos;
	int areTheSame;

	if (segmentMagnitude(segment)>MIN_TAM_SEG) {
		isMerge = mergeSegment (segment, &areTheSame);
		if (!isMerge) { // check insertion position
			//if (DEBUG) printf ("Storing new segments in memory\n");
			if (numSegments < MAX_LINES_IN_MEMORY)	{
				pos = numSegments;
				numSegments ++;
			} else {
				pos = incNumSegments;
				incNumSegments++;
				if (incNumSegments == MAX_LINES_IN_MEMORY) {
					incNumSegments=0;
				}
			}
			groundSegments3D[pos].start = segment.start;
			groundSegments3D[pos].end = segment.end;
			groundSegments3D[pos].idColor = segment.idColor;
			groundSegments3D[pos].isValid = 1;
			groundSegments3D[pos].timestamp = segment.timestamp;
			groundSegments3D[pos].traveled = segment.traveled;
		}
	}
}

inline void	createSegment3D (HPoint3D startPoint, HPoint3D endPoint) {
	Segment3D segment;

	segment.start.position.X = startPoint.X;
	segment.start.position.Y = startPoint.Y;
	segment.start.position.Z = startPoint.Z;
	segment.start.position.H = 1.;
	segment.end.position.X = endPoint.X;
	segment.end.position.Y = endPoint.Y;
	segment.end.position.Z = endPoint.Z;
	segment.end.position.H = 1.;
	segment.idColor = idRandColor; // color aleatorio

	segment.timestamp = actualInstant;
	segment.traveled = 0.; // desde que lo vemos tiene un recorrido el robot de 0 sobre él

	storeSegment3D (segment);
}

inline void segmentProjection (Segment2D segment) {
	myCamera = &robotCamera;

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1.;

	myStartPoint2D.y = segment.start.x;
	myStartPoint2D.x = 240.-1.-segment.start.y;
	myStartPoint2D.h = 1.;
	myEndPoint2D.y = segment.end.x;
	myEndPoint2D.x = 240.-1.-segment.end.y;
	myEndPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myStartPoint3D, myStartPoint2D, *myCamera);
	backproject(&myEndPoint3D, myEndPoint2D, *myCamera);

	linePlaneIntersection (myStartPoint3D, cameraPos3D);
	intersectionStartPoint = intersectionPoint;
	linePlaneIntersection (myEndPoint3D, cameraPos3D);
	intersectionEndPoint = intersectionPoint;

	createSegment3D (intersectionStartPoint, intersectionEndPoint);
}

inline void pixel2Optical (HPoint2D *p) {
	float aux;

	aux = p->x;
	p->x = 240-1-p->y;
	p->y = aux;		
}

inline void getCameraWorldPos () {
	float actualPan, actualTilt;
	gsl_matrix *robotRT, *baseCuelloRT, *alturaEjeTiltRT, *ejeTiltRT, *camaraRT, *temp1, *temp2, *temp3, *temp4, *foaRel, *foaAbs, *myComodin, *myComodin2;

	actualPan = *mypan_angle;
	actualPan=actualPan*PI/180.0;
	actualTilt=(*mytilt_angle)*PI/180.0;

	//if (DEBUG) printf ("sliderPantiltBaseX = %0.2f\n, panAngle = %0.2f, tiltAngle = %0.2f", sliderPANTILT_BASE_X, panAngle, tiltAngle);

  robotRT = gsl_matrix_calloc(4,4);
  baseCuelloRT = gsl_matrix_calloc(4,4);
  alturaEjeTiltRT = gsl_matrix_calloc(4,4);
  ejeTiltRT = gsl_matrix_calloc(4,4);
  camaraRT = gsl_matrix_calloc(4,4);
  temp1 = gsl_matrix_calloc(4,4);
  temp2 = gsl_matrix_calloc(4,4);
  temp3 = gsl_matrix_calloc(4,4);
  temp4 = gsl_matrix_calloc(4,4);
  myComodin = gsl_matrix_calloc(4,4);
  myComodin2 = gsl_matrix_calloc(4,4);
	foaRel = gsl_matrix_calloc(4,1);
	foaAbs = gsl_matrix_calloc(4,1);

	// 1º - El robot está transladado respecto al (0, 0) absoluto, y rotado respecto al eje Z
	gsl_matrix_set(robotRT,0,0,cos(myencoders[2]));
	gsl_matrix_set(robotRT,0,1,-sin(myencoders[2]));
	gsl_matrix_set(robotRT,0,2,0.);
	gsl_matrix_set(robotRT,0,3,myencoders[0]);
	gsl_matrix_set(robotRT,1,0,sin(myencoders[2]));
	gsl_matrix_set(robotRT,1,1,cos(myencoders[2]));
	gsl_matrix_set(robotRT,1,2,0.);
	gsl_matrix_set(robotRT,1,3,myencoders[1]);
	gsl_matrix_set(robotRT,2,0,0.);
	gsl_matrix_set(robotRT,2,1,0.);
	gsl_matrix_set(robotRT,2,2,1.);
	gsl_matrix_set(robotRT,2,3,0.); // Z de la base robot la considero 0
	gsl_matrix_set(robotRT,3,0,0.);
	gsl_matrix_set(robotRT,3,1,0.);
	gsl_matrix_set(robotRT,3,2,0.);
	gsl_matrix_set(robotRT,3,3,1.0);

	// 2º - La base del cuello está transladado en Z y en X respecto a la base del robot (considerada en el suelo)
	gsl_matrix_set(baseCuelloRT,0,0,cos(0.));
	gsl_matrix_set(baseCuelloRT,0,1,sin(0.));
	gsl_matrix_set(baseCuelloRT,0,2,0.);
	gsl_matrix_set(baseCuelloRT,0,3,sliderPANTILT_BASE_X);
	gsl_matrix_set(baseCuelloRT,1,0,-sin(0.));
	gsl_matrix_set(baseCuelloRT,1,1,cos(0.));
	gsl_matrix_set(baseCuelloRT,1,2,0.);
	gsl_matrix_set(baseCuelloRT,1,3,PANTILT_BASE_Y);
	gsl_matrix_set(baseCuelloRT,2,0,0.);
	gsl_matrix_set(baseCuelloRT,2,1,0.);
	gsl_matrix_set(baseCuelloRT,2,2,1.);
	gsl_matrix_set(baseCuelloRT,2,3,sliderPANTILT_BASE_HEIGHT); // desde el suelo a la base
	gsl_matrix_set(baseCuelloRT,3,0,0.);
	gsl_matrix_set(baseCuelloRT,3,1,0.);
	gsl_matrix_set(baseCuelloRT,3,2,0.);
	gsl_matrix_set(baseCuelloRT,3,3,1.0);

	// 3º - El eje de tilt está transladado en Z respecto a la base del cuello, y rotado respecto al eje Z
	gsl_matrix_set(alturaEjeTiltRT,0,0,cos(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,0,1,-sin(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,0,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,0,3,0.);
	gsl_matrix_set(alturaEjeTiltRT,1,0,sin(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,1,1,cos(actualPan));
	gsl_matrix_set(alturaEjeTiltRT,1,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,1,3,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,0,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,1,0.);
	gsl_matrix_set(alturaEjeTiltRT,2,2,1.);
	gsl_matrix_set(alturaEjeTiltRT,2,3,sliderTILT_HEIGHT);
	gsl_matrix_set(alturaEjeTiltRT,3,0,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,1,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,2,0.);
	gsl_matrix_set(alturaEjeTiltRT,3,3,1.0);

	// 4º - El eje de tilt está además rotado respecto al eje Y
	gsl_matrix_set(ejeTiltRT,0,0,cos(actualTilt));
	gsl_matrix_set(ejeTiltRT,0,1,0.);
	gsl_matrix_set(ejeTiltRT,0,2,-sin(actualTilt));
	gsl_matrix_set(ejeTiltRT,0,3,0.);
	gsl_matrix_set(ejeTiltRT,1,0,0.);
	gsl_matrix_set(ejeTiltRT,1,1,1.);
	gsl_matrix_set(ejeTiltRT,1,2,0.);
	gsl_matrix_set(ejeTiltRT,1,3,0.);
	gsl_matrix_set(ejeTiltRT,2,0,sin(actualTilt));
	gsl_matrix_set(ejeTiltRT,2,1,0.);
	gsl_matrix_set(ejeTiltRT,2,2,cos(actualTilt));
	gsl_matrix_set(ejeTiltRT,2,3,0.);
	gsl_matrix_set(ejeTiltRT,3,0,0.);
	gsl_matrix_set(ejeTiltRT,3,1,0.);
	gsl_matrix_set(ejeTiltRT,3,2,0.);
	gsl_matrix_set(ejeTiltRT,3,3,1.0);

	// El centro óptico de la cámara está transladado en X y en Z respecto al eje tilt
	gsl_matrix_set(camaraRT,0,0,cos(0.));
	gsl_matrix_set(camaraRT,0,1,sin(0.));
	gsl_matrix_set(camaraRT,0,2,0.);
	gsl_matrix_set(camaraRT,0,3,sliderISIGHT_OPTICAL_CENTER);
	gsl_matrix_set(camaraRT,1,0,-sin(0.));
	gsl_matrix_set(camaraRT,1,1,cos(0.));
	gsl_matrix_set(camaraRT,1,2,0.);
	gsl_matrix_set(camaraRT,1,3,0.);
	gsl_matrix_set(camaraRT,2,0,0.);
	gsl_matrix_set(camaraRT,2,1,0.);
	gsl_matrix_set(camaraRT,2,2,1.);
	gsl_matrix_set(camaraRT,2,3,sliderCAMERA_TILT_HEIGHT);
	gsl_matrix_set(camaraRT,3,0,0.);
	gsl_matrix_set(camaraRT,3,1,0.);
	gsl_matrix_set(camaraRT,3,2,0.);
	gsl_matrix_set(camaraRT,3,3,1.0);

	gsl_linalg_matmult (robotRT, baseCuelloRT, temp1);
	gsl_linalg_matmult (temp1, alturaEjeTiltRT, temp2);
	gsl_linalg_matmult (temp2, ejeTiltRT, temp3);
	gsl_linalg_matmult (temp3, camaraRT, temp4);

//	gsl_linalg_matmult (robotRT, myComodin, temp1);
//	gsl_linalg_matmult (temp1, myComodin2, temp2);
	gsl_matrix_set(foaRel,0,0,3000.0);
	gsl_matrix_set(foaRel,1,0,0.0);
	gsl_matrix_set(foaRel,2,0,0.0);
	gsl_matrix_set(foaRel,3,0,1.0);

	gsl_linalg_matmult(temp4,foaRel,foaAbs);

	robotCamera.position.X = gsl_matrix_get (temp4, 0, 3);
	robotCamera.position.Y = gsl_matrix_get (temp4, 1, 3);
	robotCamera.position.Z = gsl_matrix_get (temp4, 2, 3);
  robotCamera.foa.X=(float)gsl_matrix_get(foaAbs,0,0);
  robotCamera.foa.Y=(float)gsl_matrix_get(foaAbs,1,0);
  robotCamera.foa.Z=(float)gsl_matrix_get(foaAbs,2,0);

	update_camera_matrix (&robotCamera);

	//printCameraInformation (&robotCamera);

	// Liberación de memoria de matrices
  gsl_matrix_free(robotRT);
  gsl_matrix_free(baseCuelloRT);
  gsl_matrix_free(alturaEjeTiltRT);
  gsl_matrix_free(ejeTiltRT);
  gsl_matrix_free(camaraRT);
  gsl_matrix_free(temp1);
  gsl_matrix_free(temp2);
  gsl_matrix_free(temp3);
  gsl_matrix_free(temp4);
  gsl_matrix_free(foaRel);
  gsl_matrix_free(foaAbs);
  gsl_matrix_free(myComodin);
  gsl_matrix_free(myComodin2);
}

inline void moveInitialTilt () {
  speed_y = 16*(VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(tiltAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT))));
  speed_x = 16*(VEL_MAX_PAN-((POS_MAX_PAN-DEG_TO_ENCOD*(abs(panAngle)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN))));

	if ((*mylatitude_speed) < VEL_MIN_TILT)
		*mylatitude_speed = VEL_MIN_TILT*4;
	else *mylatitude_speed = speed_y;
	if ((*mylongitude_speed) < VEL_MIN_PAN)
		*mylongitude_speed = VEL_MIN_PAN*4;
	else *mylongitude_speed = speed_x;

	// TODO
	*mylatitude = -29.;//tiltAngle;
	*mylongitude = 0.;//-50.; //panAngle;//-50.;
	if (((*mypan_angle) < ((*mylongitude) + 1.)) && ((*mypan_angle) > ((*mylongitude) - 1.)) && ((*mytilt_angle) < ((*mylatitude) + 1.)) && ((*mytilt_angle) > ((*mylatitude) - 1.))) {
		completedMovement = TRUE;
		pantiltStill = TRUE;
	}	else
		completedMovement = FALSE;
}

inline void makePrediction (IplImage *img) {
	CvPoint pt1, pt2;
	HPoint2D gooda, goodb;
	HPoint2D p1, p2;
	struct Segment2y3DList *my2y3DSeg, *last2y3DSegment;
	struct CacheList *p;

	p = cache;

	last2y3DSegment = seg2y3Dlist;
	my2y3DSeg = last2y3DSegment;

	while (last2y3DSegment != NULL) {
		last2y3DSegment = my2y3DSeg->next;
		free (my2y3DSeg);
		my2y3DSeg = last2y3DSegment;
	}

	seg2y3Dlist = NULL; // inicializamos puntero inicial y final

	last2y3DSegment = seg2y3Dlist;

	while (p != NULL) {
		if (p->segment->isValid == 1) {
		  project(p->segment->start.position, &p1, robotCamera); // pasamos de 3D a 2D
		  project(p->segment->end.position, &p2, robotCamera);

			if(displayline(p1,p2,&gooda,&goodb,robotCamera)==1) { // es proyectable, está en el campo de visión
				//Pasamos de coordenadas opticas a pixels
				p->segment->isWellPredicted = FALSE; // los marcamos como mal predichos, de momento, luego corroboraremos...

				pt1.x=(int)gooda.y;
				pt1.y=SIFNTSC_ROWS-1-(int)gooda.x;

				pt2.x=(int)goodb.y;
				pt2.y=SIFNTSC_ROWS-1-(int)goodb.x;

				if (showPredictions == TRUE)
					cvLine(img, pt1, pt2, CV_RGB(0,0,255), 3, 8, 0); // las pintamos en rojo todas en un principio

				my2y3DSeg = NULL;
				my2y3DSeg = (struct Segment2y3DList*) malloc(sizeof(struct Segment2y3DList));
				my2y3DSeg->segment2D.start.x = pt1.x;
				my2y3DSeg->segment2D.start.y = pt1.y;
				my2y3DSeg->segment2D.start.h = 1.;
				my2y3DSeg->segment2D.end.x = pt2.x;
				my2y3DSeg->segment2D.end.y = pt2.y;
				my2y3DSeg->segment2D.end.h = 1.;
				my2y3DSeg->segment3D = p->segment;
				my2y3DSeg->next = NULL;

				if (DEBUG) printf ("makePrediction of segment start = [%0.2f, %0.2f, %0.2f] end = [%0.2f, %0.2f, %0.2f]\n", p->segment->start.position.X, p->segment->start.position.Y, p->segment->start.position.Z, p->segment->end.position.X, p->segment->end.position.Y, p->segment->end.position.Z);

				if (last2y3DSegment != NULL) {
					last2y3DSegment->next = my2y3DSeg;
					last2y3DSegment = last2y3DSegment->next;
				} else { // list = NULL, así que inicializamos
					seg2y3Dlist = my2y3DSeg; // inicialización de list
					last2y3DSegment = seg2y3Dlist;
				}
			}
		}
		p = p->next;
	}
}

/*Dibujamos una linea desde p1 a p2 en la imagen*/
inline int projectParallelogram (IplImage* src, IplImage* src2, HPoint2D p1, HPoint2D p2, HPoint2D p3, HPoint2D p4, TPinHoleCamera camera) {
	/* it takes care of important features */
	/* before/behind the focal plane, inside/outside the image frame */

	CvPoint pts[5];

	/*Pasamos de coordenadas opticas a pixels*/
	pts[0].x=(int)p1.y;
	pts[0].y=SIFNTSC_ROWS-1-(int)p1.x;

	pts[1].x=(int)p2.y;
	pts[1].y=SIFNTSC_ROWS-1-(int)p2.x;

	pts[2].x=(int)p3.y;
	pts[2].y=SIFNTSC_ROWS-1-(int)p3.x;

	pts[3].x=(int)p4.y;
	pts[3].y=SIFNTSC_ROWS-1-(int)p4.x;

	pts[4].x=pts[0].x;
	pts[4].y=pts[0].y;

	cvFillConvexPoly(src, &pts[0], 5, CV_RGB(0.,0.,0.), 8, 0);
	cvFillConvexPoly(src2, &pts[0], 5, CV_RGB(0.,0.,0.), 8, 0);

	return 1;
}

inline void segment2Dto3D (Segment2D segment2D, Segment3D *segment3D) {
	segment3D->start.position.X = segment2D.start.x;
	segment3D->start.position.Y = segment2D.start.y;
	segment3D->start.position.Z = 0.;
	segment3D->start.position.H = 1.;

	segment3D->end.position.X = segment2D.end.x;
	segment3D->end.position.Y = segment2D.end.y;
	segment3D->end.position.Z = 0.;
	segment3D->end.position.H = 1.;
}

inline void segment3Dto2D (Segment3D segment3D, Segment2D *segment2D) {
	segment2D->start.x = segment3D.start.position.X;
	segment2D->start.y = segment3D.start.position.Y;

	segment2D->end.x = segment3D.end.position.X;
	segment2D->end.y = segment3D.end.position.Y;
}

inline int equalsSegment2D (Segment2D seg1, Segment2D seg2) {
	Segment3D seg1b, seg2b;
	int found=0;
	HPoint3Dinfo proy1, proy2;
	int ubi1, ubi2;
	float dist1, dist2;

	segment2Dto3D (seg1, &seg1b);
	segment2Dto3D (seg2, &seg2b);

	if (areTheSameSegment (seg1b, seg2b)) {
		found = 1;
	} else {
		// Check every segment in memory, with segment. Calculate intersection point between segment i and segment.start perpendicular
		ubi1 = distancePointLine (seg1b.start, seg2b, &proy1, &dist1);
		ubi2 = distancePointLine (seg1b.end, seg2b, &proy2, &dist2);

		found = overlapping (seg2b, proy1, proy2, ubi1, ubi2, dist1, dist2, 1); // If they're parallel lines -> we finish checking
	}

	return found;
}

inline int isInPrediction (Segment2D segment, IplImage *img) {
	CvPoint pt1, pt2;
	int found=0;
	struct Segment2y3DList *actualElement;

	actualElement = seg2y3Dlist; // empezamos por el principio de la lista

	while (actualElement != NULL)	{
/*		if (DEBUG) printf ("[%0.2f, %0.2f, %0.2f]-[%0.2f, %0.2f, %0.2f] vs [%0.2f, %0.2f, %0.2f]-[%0.2f, %0.2f, %0.2f]\n", mySeg3Da.start.position.X, mySeg3Da.start.position.Y, mySeg3Da.start.position.Z, mySeg3Da.end.position.X, mySeg3Da.end.position.Y, mySeg3Da.end.position.Z, mySeg3Db.start.position.X, mySeg3Db.start.position.Y, mySeg3Db.start.position.Z, mySeg3Db.end.position.X, mySeg3Db.end.position.Y, mySeg3Db.end.position.Z); */
		found = equalsSegment2D (actualElement->segment2D, segment);
		if (DEBUG) printf ("   is Found? %i\n", found);
		if (found) {
			actualElement->segment3D->timestamp = actualInstant;
			actualElement->segment3D->traveled = 0.;
			actualElement->segment3D->isWellPredicted = TRUE;

			pt1.x = actualElement->segment2D.start.x;
			pt1.y = actualElement->segment2D.start.y;

			pt2.x = actualElement->segment2D.end.x;
			pt2.y = actualElement->segment2D.end.y;

			if (showRefuted == TRUE)
				cvLine(img, pt1, pt2, CV_RGB(255,0,0), 3, 8, 0); // predicción refutada la mostramos en azul
		}
		actualElement = actualElement->next;
	}

	return found;
}

inline void updatePredictions () {
	struct Segment2y3DList *actualElement;

	actualElement = seg2y3Dlist; // empezamos por el principio de la lista

	while (actualElement != NULL)	{
		if (actualElement->segment3D->isWellPredicted == FALSE) { // no fue corroborado su segmento 3D
			actualElement->segment3D->isValid = FALSE; // a efectos es como si lo eliminaramos de memoria
		}
		actualElement = actualElement->next;
	}
}

inline void cannyFilter() {
	IplImage *src;
	IplImage *src2;
	IplImage *gray;
	IplImage *edge;
	IplImage *cedge;
	CvSeq* lines;
	CvMemStorage* storage;
	double distanceResolution, angleResolution;
	int numLines, i;
	Segment2D myActualContour;

	storage = cvCreateMemStorage(0);
	lines = 0;
	distanceResolution = 1.;
	angleResolution = (PI/180.);

	src = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	src2 = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	cedge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 3);
	gray = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	edge = cvCreateImage(cvSize(SIFNTSC_COLUMNS, SIFNTSC_ROWS), IPL_DEPTH_8U, 1);
	
	memcpy(src->imageData, image, src->width*src->height*src->nChannels); // aquí tenemos la original image -> src
	memcpy(src2->imageData, image, src2->width*src2->height*src2->nChannels); // aquí tenemos la original image -> src2

	// hago filtrado normal, para luego usar en imageAfiltered (con la visualSonar)
	cvCvtColor(src, gray, CV_RGB2GRAY); // en gray paso de src (RGB) -> gray (B/N)

	cvCanny(gray, edge, sliderThreshold, sliderThreshold*3, 3); // saco bordes gray -> edge
	// cvCanny detects borders between threshold and (3xThreshold)
	cvCvtColor(edge, cedge, CV_GRAY2RGB); // paso los bordes edge (B/N) -> cedge (RGB)

	makePrediction (src2); // almacenamos las predicciones en el array src2

	lines = cvHoughLines2 (edge, storage, CV_HOUGH_PROBABILISTIC, distanceResolution, angleResolution, HOUGH_LINE_THRESHOLD, HOUGH_MIN_DIST_SEG, MAX_GAP_BETWEEN_SEGMENTS);	// probabilistic Hough transform: more efficient in case if picture contains a few long linear segments

	if(lines->total > MAX_LINES_TO_DETECT)
		numLines = MAX_LINES_TO_DETECT;
	else
		numLines = lines->total;

	for (i = 0; i < numLines; i ++) { // ATENCIÓN: BUCLE IMPORTANTE DE DETECCIÓN DE LÍNEAS Y RETROPROYECCIÓN
		CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
		if (showBorders == TRUE)
			cvLine(src2, line[0], line[1], CV_RGB(0,255,255), 3, 8, 0); // almacenamos los bordes detectados en src2
		myActualContour.start.x = (float)(line[0].x);
		myActualContour.start.y = (float)(line[0].y);
		myActualContour.start.h = 1.;
		myActualContour.end.x = (float)(line[1].x);
		myActualContour.end.y = (float)(line[1].y);
		myActualContour.end.h = 1.;

		if (!isInPrediction (myActualContour, src2)); // vemos si está en la predicción previamente realizada
			segmentProjection (myActualContour); // si no está lo proyectamos e intentaremos fusionarlo e insertarlo
	}

	updatePredictions (); // aquellos segmentos predichos y que no han sido corroborados, se eliminan

	memcpy(imageAfiltered->image, src2->imageData, src2->width*src2->height*src2->nChannels); // pasamos el contenido de src2 -> imageAfiltered

	cvReleaseImage(&src);
	cvReleaseImage(&src2);
	cvReleaseImage(&gray);
	cvReleaseImage(&edge);
	cvReleaseImage(&cedge);
	cvReleaseMemStorage(&storage);
}

inline void preFilter () {
  int i, j, c, row, pos, columnPoint, rowPoint, posPoint;

  double H, S, V;
  float myR, myG, myB;

	int esFrontera;
	int v1, v2, v3, v4, v5, v6, v7, v8;

	/* Ground image */
	for (j=0;j<SIFNTSC_COLUMNS;j++) { // recorrido en columnas
		for (i = SIFNTSC_ROWS-1; i>=0; i --) { // recorrido en filas
			esFrontera = FALSE;
			if (mycolorA!= NULL) {
			  pos = i*SIFNTSC_COLUMNS+j; // posicion actual

				myR = (float)(unsigned int)(unsigned char)(*mycolorA)[pos*3+2];
				myG = (float)(unsigned int)(unsigned char)(*mycolorA)[pos*3+1];
				myB = (float)(unsigned int)(unsigned char)(*mycolorA)[pos*3+0];

				myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

				if ((myHSV->H*DEGTORAD > H_MIN_BLANCO) && (myHSV->H*DEGTORAD < H_MAX_BLANCO) &&
					(myHSV->S > S_MIN_BLANCO) && (myHSV->S < S_MAX_BLANCO) &&
					(myHSV->V > V_MIN_BLANCO) && (myHSV->V < V_MAX_BLANCO)) { // si soy blanco
/*
				if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
						(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
						(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) {*/
					esFrontera = TRUE; // lo damos por frontera en un principio, luego veremos

					// Calculo los demás vecinos, para ver de qué color son...
					c = j - 1;
					row = i;
					v1 = row*SIFNTSC_COLUMNS+c;

					if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
						 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
						myR = 0.;
						myG = 0.;
						myB = 0.;
					} else {
						myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v1*3+2];
						myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v1*3+1];
						myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v1*3+0];
					}

					myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

					if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
						(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
						(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

						c = j - 1;
						row = i - 1;
						v2 = row*SIFNTSC_COLUMNS+c;

						if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
							 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
							myR = 0.;
							myG = 0.;
							myB = 0.;
						} else {
							myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v2*3+2];
							myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v2*3+1];
							myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v2*3+0];
						}

						myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

						if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
							(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
							(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

							c = j;
							row = i - 1;
							v3 = row*SIFNTSC_COLUMNS+c;

							if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
								 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
								myR = 0.;
								myG = 0.;
								myB = 0.;
							} else {
								myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v3*3+2];
								myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v3*3+1];
								myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v3*3+0];
							}

							myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

							if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
								(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
								(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

								c = j + 1;
								row = i - 1;
								v4 = row*SIFNTSC_COLUMNS+c;

								if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
									 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
									myR = 0.;
									myG = 0.;
									myB = 0.;
								} else {
									myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v4*3+2];
									myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v4*3+1];
									myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v4*3+0];
								}

								myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

								if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
									(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
									(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

									c = j + 1;
									row = i;
									v5 = row*SIFNTSC_COLUMNS+c;

									if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
										 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
										myR = 0.;
										myG = 0.;
										myB = 0.;
									} else {
										myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v5*3+2];
										myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v5*3+1];
										myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v5*3+0];
									}

									myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

									if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
										(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
										(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

										c = j + 1;
										row = i + 1;
										v6 = row*SIFNTSC_COLUMNS+c;

										if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
											 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
											myR = 0.;
											myG = 0.;
											myB = 0.;
										} else {
											myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v6*3+2];
											myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v6*3+1];
											myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v6*3+0];
										}

										myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

										if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
											(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
											(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

											c = j;
											row = i + 1;
											v7 = row*SIFNTSC_COLUMNS+c;

											if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
												 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
												myR = 0.;
												myG = 0.;
												myB = 0.;
											} else {
												myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v7*3+2];
												myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v7*3+1];
												myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v7*3+0];
											}

											myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

											if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
												(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
												(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, sigo comprobando

												c = j - 1;
												row = i + 1;
												v8 = row*SIFNTSC_COLUMNS+c;

												if (!((c >= 0) && (c < SIFNTSC_COLUMNS) && 
													 (row >= 0) && (row < SIFNTSC_ROWS))) { // si no es válido ponemos sus valores a 0
													myR = 0.;
													myG = 0.;
													myB = 0.;
												} else {
													myR = (float)(unsigned int)(unsigned char)(*mycolorA)[v8*3+2];
													myG = (float)(unsigned int)(unsigned char)(*mycolorA)[v8*3+1];
													myB = (float)(unsigned int)(unsigned char)(*mycolorA)[v8*3+0];
												}

												myHSV = (struct HSV*) RGB2HSV_getHSV ((int)myR,(int)myG,(int)myB); // pasamos de RGB -> HSV

												if (!((myHSV->H*DEGTORAD > H_MIN_VERDE) && (myHSV->H*DEGTORAD < H_MAX_VERDE) &&
													(myHSV->S > S_MIN_VERDE) && (myHSV->S < S_MAX_VERDE) &&
													(myHSV->V > V_MIN_VERDE) && (myHSV->V < V_MAX_VERDE))) { // si no soy color campo, se acabó, no es pto. frontera
													esFrontera = FALSE;
												} // fin if vecinito 8
											} // fin if vecinito 7
										} // fin if vecinito 6
									} // fin if vecinito 5
								} // fin if vecinito 4
							} // fin if vecinito 3
						} // fin if vecinito 2
					} // fin if vecinito 1
				}
				if (esFrontera == TRUE) { // si NO SOY COLOR CAMPO y alguno de los vecinitos ES color campo...
				  image[pos*3] = 255.;
				  image[pos*3+1] = 255.;
				  image[pos*3+2] = 255.;
				}	else {
				  image[pos*3]=0.;
				  image[pos*3+1]=0.;
				  image[pos*3+2]=0.;
				}
			}
		}
	}
}

inline void processImage () {
	int i;

	preFilter (); // image la pondremos en blanco sobre negro, según los píxeles detectados blancos sobre verde

	cannyFilter (); // aquí hacemos todo el procesado de imagen y retroproyección de líneas en 3D

	for(i=0; i<SIFNTSC_ROWS*SIFNTSC_COLUMNS; i++) {
		if ((imageAfiltered->image[i*3] == 0.) && (imageAfiltered->image[i*3+1] == 0.) && (imageAfiltered->image[i*3+2] == 0.)) {
			imageAfiltered->image[i*3]=(*mycolorA)[i*3+2];
			imageAfiltered->image[i*3+1]=(*mycolorA)[i*3+1];
			imageAfiltered->image[i*3+2]=(*mycolorA)[i*3];
		}
	}

	// Imagen con segmentos retroproyectados sobre original
	notPrepare2draw(imageAfiltered, filteredImageRGB);

	// Imagen de la cara
	//prepare2draw(imageAgrounded, groundedImageRGB);

	// Imagen original con el foco de la cámara
	//drawCross(imageA, pixel_camA, 5, 255, 0, 0); // pinta el foco de la cámara
	//prepare2draw(imageA, originalImageRGB);
}

inline void getLastPoint (Parallelogram3D* square) {
	float t;

	t = (square->p2.position.X - square->p3.position.X)/((square->p1.position.X - square->p2.position.X)-(square->p1.position.X - square->p3.position.X));

	square->p4.position.X = square->p2.position.X + (square->p1.position.X - square->p3.position.X)*t;
	square->p4.position.Y = square->p2.position.Y + (square->p1.position.Y - square->p3.position.Y)*t;
	square->p4.position.Z = square->p2.position.Z + (square->p1.position.Z - square->p3.position.Z)*t;
}

inline int haveACommonVertex (Segment3D s1, Segment3D s2, Parallelogram3D *square){
	// caso de tener un vértice común, este quedará en square->p1, los otros dos serán square->p2 y p3 respectivamente.
	HPoint3D point[4];
	int i,j;
	point[0] = s1.start.position;
	point[1] = s1.end.position;
	point[2] = s2.start.position;
	point[3] = s2.end.position;

	j = 0;
	i = 2;

	if((point[i].X > point[j].X - COMMON_VERTEX_THRESHOLD)&&(point[i].X < point[j].X + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Y > point[j].Y - COMMON_VERTEX_THRESHOLD)&&(point[i].Y < point[j].Y + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Z > point[j].Z - COMMON_VERTEX_THRESHOLD)&&(point[i].Z < point[j].Z + COMMON_VERTEX_THRESHOLD)){
				
			square->p1.position = point[0];
			square->p2.position = point[1];
			square->p3.position = point[3];
			return 1;
	}
	j = 0;
	i = 3;
	if((point[i].X > point[j].X - COMMON_VERTEX_THRESHOLD)&&(point[i].X < point[j].X + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Y > point[j].Y - COMMON_VERTEX_THRESHOLD)&&(point[i].Y < point[j].Y + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Z > point[j].Z - COMMON_VERTEX_THRESHOLD)&&(point[i].Z < point[j].Z + COMMON_VERTEX_THRESHOLD)){
				
			square->p1.position = point[0];
			square->p2.position = point[1];
			square->p3.position = point[2];
			return 1;
	}
	j = 1;
	i = 2;
	if((point[i].X > point[j].X - COMMON_VERTEX_THRESHOLD)&&(point[i].X < point[j].X + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Y > point[j].Y - COMMON_VERTEX_THRESHOLD)&&(point[i].Y < point[j].Y + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Z > point[j].Z - COMMON_VERTEX_THRESHOLD)&&(point[i].Z < point[j].Z + COMMON_VERTEX_THRESHOLD)){
				
			square->p1.position = point[1];
			square->p2.position = point[0];
			square->p3.position = point[3];
			return 1;
	}
	j = 1;
	i = 3;
	if((point[i].X > point[j].X - COMMON_VERTEX_THRESHOLD)&&(point[i].X < point[j].X + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Y > point[j].Y - COMMON_VERTEX_THRESHOLD)&&(point[i].Y < point[j].Y + COMMON_VERTEX_THRESHOLD)
	&&(point[i].Z > point[j].Z - COMMON_VERTEX_THRESHOLD)&&(point[i].Z < point[j].Z + COMMON_VERTEX_THRESHOLD)){
				
			square->p1.position = point[1];
			square->p2.position = point[0];
			square->p3.position = point[2];
			return 1;
	}
	return 0;
}

inline float angleBetweenSegments (Parallelogram3D square) {
	// We have three points forming a triangle p2-p1-p3, where p1 is the common vertex
	// So, we want to get the p2p1-p1p3 angle
	float d21, d13, d23, cos1;
	d21 = distanceBetweenPoints (square.p2.position, square.p1.position);
	d13 = distanceBetweenPoints (square.p1.position, square.p3.position);
	d23 = distanceBetweenPoints (square.p2.position, square.p3.position);

	cos1 = SQUARE(d23) - SQUARE(d21) - SQUARE(d13);
	cos1 /= (2 * d21 * d13);

	return acos (cos1);

/*	HPoint3D A,B;
	float modA,modB;
	float scalarproduct;
	float a;

	A.X = s1.end.position.X -s1.start.position.X;
	A.Y = s1.end.position.Y -s1.start.position.Y;
	A.Z = s1.end.position.Z -s1.start.position.Z;

	B.X = s2.end.position.X -s2.start.position.X;
	B.Y = s2.end.position.Y -s2.start.position.Y;
	B.Z = s2.end.position.Z -s2.start.position.Z;

	modA = sqrt(SQUARE(A.X)+SQUARE(A.Y)+SQUARE(A.Z));
	modB = sqrt(SQUARE(B.X)+SQUARE(B.Y)+SQUARE(B.Z));

	scalarproduct = A.X*B.X + A.Y*B.Y + A.Z*B.Z;

	a = acos(scalarproduct/(modA*modB));
	
	return a;*/
}

inline void buildBase (Segment3D* s, HPoint3D p) {
	float dist1, dist2;
	HPoint3D temp;
	dist1 = distanceBetweenPoints ((s->start).position, p);
	dist2 = distanceBetweenPoints ((s->end).position, p);

	if (dist1 < dist2) { // el que suponiamos end del segment está más alejado del punto intersección...
		temp = (s->start).position; // ...con lo que procedemos a cambiar de orden los extremos, en el orden correcto
		(s->start).position = (s->end).position;
		(s->end).position = temp;
	} // en caso contrario el segmento base de la flecha está bien tomado (el extremo end es donde está realmente la flecha)
}

inline void buildSemiArrow (Segment3D s1, Segment3D s2, HPoint3D intersec, float dist1, float dist2, SemiArrow3D* mySemiArrow) {
	if (dist1 > dist2) {
		buildBase (&s1, intersec);
		mySemiArrow->base = s1;
		mySemiArrow->cross = s2;
	} else {
		buildBase (&s2, intersec);
		mySemiArrow->base = s2;
		mySemiArrow->cross = s1;
	}
	mySemiArrow->isValid = 1;	
}

inline void buildArrow (Segment3D base, Arrow3D* myArrow) {
	myArrow->start = base.start.position; // ya comprobamos en su momento que start y end son correctos, ya que será la dirección de la flecha
	myArrow->end = base.end.position;
}

inline void fillArrowElement (Arrow3D arrow) {
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));
	if (myActualElement == NULL) if (DEBUG) printf ("Arrow malloc failed\n");
	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;

	// Si estamos aquí es porque acabamos de ver un paralelogramo, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima

	myActualElement->type = 3; // elemento flecha
	myActualElement->arrow = arrow;
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline void insertArrowOnAttentionSystem (Arrow3D arrow) {
	fillArrowElement (arrow);
	isMergedElementAndInsert ();
}

inline int elementAttainable (struct elementStruct *element) {
	HPoint3D p;
	float pan, tilt;
	float isAttainable = FALSE;

	if (element->type == 1)
		p = element->parallelogram.centroid;
	else if (element->type == 2)
		p = element->face.center.position;
	else if (element->type == 3)
		p = element->arrow.end;
	else // es un elemento virtual, que supuestamente será atendible
		isAttainable = TRUE;

	if (!isAttainable) {
		p.X = p.X - robotCamera.position.X;
		p.Y = p.Y - robotCamera.position.Y;
		p.Z = 0. - robotCamera.position.Z;
		p.H = 1.;

		if (p.X != 0.) {
			pan = atan(p.Y/p.X);
			tilt = atan (p.Z/p.X); // La Z siempre saldrá negativa, porque estará más alta que el punto

			pan = pan * 180.0/PI; // pasamos a grados
			tilt = tilt * 180.0/PI;

			if ((pan >= -90) && (pan <= 90) && (tilt < -20) && (tilt > -50))
				isAttainable = TRUE;
		}
	}

	return (isAttainable);
}

inline void getMaxSaliency () {
	struct elementStruct *p, *q, *r, *prev;
	float maxSaliency = -9999.;
	p = myElements;
	q = NULL;
	r = p;

	// Recorremos la lista buscando al de mayor saliencia
	while (p != NULL) {
		if (DEBUG) printf ("getMaxSaliency: myElements[%i]. Type = %i, Pos: [%.2f, %.2f], live = %.1f, sal = %.1f p es %p\n", elementsStructCounter-1, p->type, p->longitude, p->latitude, p->liveliness, p->saliency, p);

		if ((p->saliency > maxSaliency) && (elementAttainable (p))) { // elemento atendible
			maxSaliency = p->saliency;
			q = p; // me lo guardo
			prev = r; // r tendrá el anterior al maxSaliency
		}
		r = p;
		p = p->next; // preparamos a p para la siguiente vuelta :)
	}

	if (q != NULL) { // nos aseguramos que q tenga algo, para que no casque
		myMaxSaliency = q; // q tendrá al elemento con mayor saliencia

		if (myMaxSaliency == myElements) // como es el primero, el previo al maxSaliency es Null
			myPrevMaxSaliency = NULL;
		else // maxSaliency es !Null y no es el primero, por tanto r tendrá el previo al maxSaliency
			myPrevMaxSaliency = prev;

		myMaxSaliency->saliency = MIN_SALIENCY;
		myMaxSaliency->liveliness = MAX_LIVELINESS;
		myMaxSaliency->lastInstant = actualInstant;
		if (myMaxSaliency->type != 0) // tenemos un elemento que no es virtual
			point2angle (); // obtenemos su posición en el mundo y las posiciones (pan, tilt) para el cuello mecánico

		if (DEBUG) printf ("getMaxSaliency: choosen new maxSaliency element (%i)-> myElements[%i]. Pos: [%.2f, %.2f], live = %.1f, sal = %.1f\n", myMaxSaliency->type, elementsStructCounter-1, myMaxSaliency->latitude, myMaxSaliency->longitude, myMaxSaliency->liveliness, myMaxSaliency->saliency);
	} else {
		if (DEBUG) printf ("getMaxSaliency: Fatal error - Max Saliency element NOT FOUND!\n");
		myMaxSaliency = NULL; // no tenemos candidato ahora mismo
		myPrevMaxSaliency = NULL;
	}
}

inline void hypothesizeArrows () {
	int found;
	float dist1, dist2, ang;
	Parallelogram3D square;
	SemiArrow3D mySemiArrow;
	Arrow3D myArrow;
	checkedArrow = FALSE;
	struct CacheList *p, *q;

	p = cache;

	while (p != NULL) {
		if (p->segment->isValid == 1) {
			dist1 = distanceBetweenPoints (p->segment->start.position, p->segment->end.position);
			if (DEBUG) printf ("hypothesizeArrows: dist1 = %0.2f\n", dist1);
			if ((dist1<MAX_TAM_ARROW) && (dist1>MIN_TAM_ARROW)) {
				if (DEBUG) printf ("hypothesizeArrows: dist1 = %0.2f\n", dist1);
				q = p->next;
				found = FALSE;
				while ((q != NULL) && (!found)) {
					if (q->segment->isValid == 1) {
						dist2 = distanceBetweenPoints (q->segment->start.position, q->segment->end.position);
						if (DEBUG) printf ("hypothesizeArrows: dist2 = %0.2f\n", dist2);
						if ((dist2<MAX_TAM_ARROW) && (dist2>MIN_TAM_ARROW)) {
							if (((dist1<MAX_TAM_ARROW) && (dist1>MIN_TAM_BASE) && (dist2>MIN_TAM_ARROW)) || ((dist2<MAX_TAM_ARROW) && (dist2>MIN_TAM_BASE) && (dist1>MIN_TAM_ARROW))) {
								if (DEBUG) printf ("hypothesizeArrows: Arrow LOOP, dist1 = %0.2f, dist2 = %0.2f\n", dist1, dist2);
								if (haveACommonVertex(*(p->segment),*(q->segment),&square)) {
									ang = angleBetweenSegments(square);
									if (DEBUG) printf ("hypothesizeArrows: they've a common vertex :), their angle is %0.2f\n", (ang*RADTODEG));
									if(((ang > (DEGTORAD*25))&&(ang < (DEGTORAD*65))) || ((ang > (DEGTORAD*115))&&(ang < (DEGTORAD*155)))) {
										found = TRUE;
										// square.p1 será el punto de intersección entre base y aspa, nos dirá la dirección de la futura flecha
										buildSemiArrow (*(p->segment), *(q->segment), square.p1.position, dist1, dist2, &mySemiArrow);
										buildArrow (mySemiArrow.base, &myArrow);
										insertArrowOnAttentionSystem (myArrow);
										if (DEBUG) printf ("Arrow Found!\n");
										if ((!checkedArrow) && (myMaxSaliency != NULL) && (myMaxSaliency->type == 3) && (areTheSameArrow (myMaxSaliency->arrow,myArrow))) { // si nos hemos posado aquí a revisitar la flecha, y efectivamente está
											checkedArrow = TRUE;
										}
									}
								}
							}
						}
					}
					q = q->next;
				}
			}
		}
		p = p->next;
	}
}

inline void hypothesizeParallelograms () {
	int counter, squareFound;
	float dist1, dist2, ang;
	Parallelogram3D square;
	checkedParallelogram = FALSE;
	struct CacheList *p, *q;

	p = cache;

	while (p != NULL) {
		if (p->segment->isValid == 1) {
			counter = 0; // intersection counter

			squareFound = 0;
			q = p->next;
			while ((q != NULL) && (!squareFound)) {
				if (q->segment->isValid == 1) {
					if (haveACommonVertex(*(p->segment),*(q->segment),&square)){
						dist1 = distanceBetweenPoints (p->segment->start.position, p->segment->end.position);
						dist2 = distanceBetweenPoints (q->segment->start.position, q->segment->end.position);
						ang = angleBetweenSegments(square);

						if(((dist1<MAX_TAM_SEG)&&(dist2<MAX_TAM_SEG))&&((ang > (DEGTORAD*80))&&(ang < (DEGTORAD*100)))){
							getLastPoint (&square);
							insertParallelogramOnAttentionSystem (square);
							squareFound = 1;

							if ((!checkedParallelogram) && (myMaxSaliency != NULL) && (myMaxSaliency->type == 1) && (areTheSameParallelogram (myMaxSaliency->parallelogram,square))) { // si nos hemos posado aquí a revisitar el paralelogramo, y efectivamente está
								checkedParallelogram = TRUE;
							}
						}
					}
				}
				q = q->next;
			}
		}
		p = p->next;
	}
}

inline void thinkObjetive () {
	//if (DEBUG) printf ("Trata de arrancarlo por dios!\n");
}

inline void searchObjetive () {
	float aux_x, aux_y, speed_x, speed_y;
	
	if (!completedSearch) {
		aux_y = myMaxSaliency->latitude;
		aux_x = myMaxSaliency->longitude;
		if (DEBUG) printf ("Moving pan-tilt to [%0.2f, %0.2f]...\n", aux_y, aux_x);

	  speed_x = 16*(VEL_MAX_PAN-((POS_MAX_PAN-DEG_TO_ENCOD*(abs(aux_x)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN))));
	  speed_y = 16*(VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(aux_y)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT))));

		if ((*mylongitude_speed) < VEL_MIN_PAN)
			*mylongitude_speed = VEL_MIN_PAN*8;
		else *mylongitude_speed = speed_x;
		if ((*mylatitude_speed) < VEL_MIN_TILT)
			*mylatitude_speed = VEL_MIN_TILT*8;
		else *mylatitude_speed = speed_y;

		if (aux_x > MAX_PAN_ANGLE)
			*mylongitude = MAX_PAN_ANGLE;
		else if (aux_x < MIN_PAN_ANGLE)
			*mylongitude = MIN_PAN_ANGLE;
		else
			*mylongitude = aux_x;

		if (aux_y > MAX_TILT_ANGLE)
			*mylatitude = MAX_TILT_ANGLE;
		else if (aux_y < MIN_TILT_ANGLE)
			*mylatitude = MIN_TILT_ANGLE;
		else
			*mylatitude = aux_y;

		if (((*mypan_angle) < ((*mylongitude) + 2.)) && ((*mypan_angle) > ((*mylongitude) - 2.)) && ((*mytilt_angle) < ((*mylatitude) + 2.)) && ((*mytilt_angle) > ((*mylatitude) - 2.))) {
			// TODO: cuando completamos una búsqueda, antes de analizar establecemos un color de pintado aleatorio
			idRandColor ++;
			if (idRandColor == 3) idRandColor = 0;
			completedSearch = TRUE;
		}	else {
			completedSearch = FALSE;
		}
	}
}

inline void facesHaarDetection (IplImage *img) {
  cvClearMemStorage (storageTmp);

  if( cascade && img )
  {
    //detectionTimeAux = (double) cvGetTickCount();
    facesTmp = cvHaarDetectObjects( img, cascade, storageTmp,
                                  1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                  cvSize(MIN_EXP_WIDTH, MIN_EXP_HEIGHT) );
		//--------------------------------------------------------------------------
		// A little of general culture :)
		// cvGetTickCount(void): Returns the number of ticks.
		// cvGetTickFrequency(void): Returns the number of ticks per microsecond.
		// Thus, the quotient of cvGetTickCount() and cvGetTickFrequency() will give
		// the number of microseconds starting from the platform-dependent event.
		//--------------------------------------------------------------------------

    //detectionTimeAux = (double) cvGetTickCount() - detectionTimeAux; // ticks to detect face...
    //detectionTimeAux =  detectionTimeAux/((double)cvGetTickFrequency()*1000.); // ...translated into seconds

    //pthread_mutex_lock(&iFollowFaceMutex);
    //facesClock = lastClock;
    //lastClock++;
    //pthread_mutex_unlock(&iFollowFaceMutex);
  }
}

void detectFaces () {
	static char **mycolorATmp = NULL;

	if (mycolorA != mycolorATmp) { // Source image has been changed
		img = cvMat(240, 320, CV_8UC3, *mycolorA);
		if (imgLocal != NULL)
			cvReleaseImage(&imgLocal);

		imgLocal = cvCreateImage(cvSize(320, 240), IPL_DEPTH_8U, 1);
		mycolorATmp = mycolorA;
	}
	if (imgLocal != NULL) {
		cvCvtColor(&img, imgLocal, CV_BGR2GRAY);
		facesHaarDetection (imgLocal);
	}
}

inline void faceProjection (HPoint2D point2D) {
	myCamera = &robotCamera;

	// Coordenadas en 3D de la posicion de la cámara
	cameraPos3D.X = myCamera->position.X;
	cameraPos3D.Y = myCamera->position.Y;
	cameraPos3D.Z = myCamera->position.Z;
	cameraPos3D.H = 1;

	myPoint2D.y = point2D.x;
	myPoint2D.x = 240.-1.-point2D.y;
	myPoint2D.h = 1.;

	// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
	backproject(&myPoint3D, myPoint2D, *myCamera);

	linePlaneIntersection (myPoint3D, cameraPos3D);
}

inline void fillFaceElement (Face3D face) {
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));
	if (myActualElement == NULL) if (DEBUG) printf ("Face malloc failed\n");
	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;

	// Si estamos aquí es porque acabamos de ver una cara, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima

	myActualElement->type = 2; // elemento face
	myActualElement->face = face;
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline void insertFaceOnAttentionSystem (Face3D face) {
	fillFaceElement (face);
	isMergedElementAndInsert ();
}

inline void getAndStoreFaces () {
	CvRect r;
	int i;
	Face3D face;
	face.isValid = TRUE; // las caras que sacaremos serán todas válidas
	checkedFace = FALSE;

	detectFaces (); // face haar detection -> facesTmp

	if (facesTmp->total <= 0) {
		faceCenter.x = 9999;
		faceCenter.y = 9999;
	} else {
		for( i = 0; i < (facesTmp ? facesTmp->total : 0); i++ ) {
			r = *(CvRect*)cvGetSeqElem(facesTmp, i);
			faceCenter.x = cvRound((r.x + r.width*0.5));
			faceCenter.y = cvRound((r.y + r.height*0.5));
			if (DEBUG) printf ("getAndStoreFaces: Cara detectada en [%0.2f, %0.2f]\n", faceCenter.x, faceCenter.y);
/*
			if (mycolorA!= NULL) {
			  imageAgrounded->image = (char *) *mycolorA; // asignamos directamente lo que viene de mycolorA
				drawCross(imageAgrounded, faceCenter, 20, 0, 255, 0); // pinta el foco de la cámara
				prepare2draw(imageAgrounded, groundedImageRGB);
			}*/

			faceProjection (faceCenter); // en intersectionPoint estará el punto retroproyectado en 3D
			face.center.position.X = intersectionPoint.X;
			face.center.position.Y = intersectionPoint.Y;
			face.center.position.Z = intersectionPoint.Z;
			face.center.position.H = intersectionPoint.H;
			insertFaceOnAttentionSystem (face);

			if ((!checkedFace) && (myMaxSaliency != NULL) && (myMaxSaliency->type == 2) && (areTheSameFace (myMaxSaliency->face,face))) { // si nos hemos posado aquí a revisitar la cara, y efectivamente está
				checkedFace = TRUE;
			}			
		}
		faceCenter.x = 9999;
		faceCenter.y = 9999; // para la siguiente vuelta
	}

	if (myMaxSaliency != NULL)
		if (DEBUG) printf ("getAndStoreFaces: tipo del myMaxSaliency = %i, isChecked = %i\n", myMaxSaliency->type, checkedFace);

	if ((myMaxSaliency != NULL) && (myMaxSaliency->type == 2) && (!checkedFace)) { // no hemos corroborado la supuesta flecha
		if (myMaxSaliency == myElements)
			myElements = myElements->next;
		else
			myPrevMaxSaliency->next = myMaxSaliency->next;

		free (myMaxSaliency);
		myMaxSaliency = NULL;
		elementsStructCounter --;
		getMaxSaliency ();
	}
}

inline void analizeImage () {
	if (processImageButton) {
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		// #1º# Obtención de posición exacta de la cámara de donde procede la imagen en curso
		getCameraWorldPos ();
		if (DEBUG) printf ("analizeImage: getCameraWorldPos()... done!\n");
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Get camera pos = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);

		// #2º# Obtención de bordes2D (canny) -> projectMemory2D sobre imagen actual (borrado de existentes) ->
		// -> segments2D (hough) -> groundSegments3D (merge)
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		processImage ();
		if (DEBUG) printf ("analizeImage: processImage()... done!\n");
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Processing Image = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);

		// #3º# Hipotetizar sobre groundSegments3D -> sistema de atención
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//hypothesizeParallelograms ();
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Hypothesize Paral = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);

		if (DEBUG) printf ("analizeImage: hypothesizeParallelograms()... done!\n");

		// #4º# Hipotetizar sobre groundSegments3D -> sistema de atención
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		hypothesizeArrows ();
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Hypothesize arrows = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);

		if (DEBUG) printf ("analizeImage: hypothesizeArrows()... done!\n");

		// #5º# Obtención de caras2D -> proyección a groundFaces3D (merge) -> inserción en sistema atentivo
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//getAndStoreFaces (); // Detectamos si hay objetos interesantes en la imagen actual
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Getting faces = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);
		//if (DEBUG) printf ("analizeImage: getAndStoreFaces()... done!\n");
	}

	analizedImage = TRUE;
	if (DEBUG) printf ("analizeImage: image analized correctly!\n");
}

inline float distancia (imagePoint punto1, imagePoint punto2) {
  return (sqrt (pow((punto2.x-punto1.x), 2.)+pow((punto2.y-punto1.y), 2.)));
}

inline double pasar_radianes_grados (double radianes) {
  return ((radianes * 180.)/PI);
}

inline double pasar_grados_radianes (double grados) {
  return ((grados * PI)/180.);
}

inline scenePoint coordenadas2PT (int x, int y) {
	imagePoint punto1, punto2, punto3, punto_origen;
	t_vector vector1, vector2;
	float dist, ang1, ang2;
	scenePoint c_angulares;

	punto1.x = TILT_MIN; punto1.y = RADIO_MAX;
	punto2.x = TILT_MAX; punto2.y = RADIO_MIN;
	vector1.x = punto2.x - punto1.x;
	vector1.y = punto2.y - punto1.y;

	punto3.x = x; punto3.y = y;
	punto_origen.x = CENTRO_X; punto_origen.y = CENTRO_Y;
	dist =  distancia (punto3, punto_origen);

	c_angulares.tilt = (((dist - punto1.y)*(vector1.x))/(vector1.y))+punto1.x;
	  
	vector1.x = CENTRO_X - x;
	vector1.y = CENTRO_Y - y;
	vector2.x = CENTRO_X - ANCHO_ESCENA_COMPUESTA / 2;
	vector2.y = CENTRO_Y - 0;
	ang1 = atan2 (vector1.x, vector1.y);
	ang2 = atan2 (vector2.x, vector2.y);

	c_angulares.pan = -1*(pasar_radianes_grados (ang2-ang1));

	return c_angulares;
}

inline void fillVirtualElement () {
	int signoP, signoT;
	myActualElement = (struct elementStruct*) malloc (sizeof (struct elementStruct));

	if (myActualElement == NULL) if (DEBUG) printf ("Virtual-element malloc failed\n");

	if (randomPosition == TRUE) {
	  srand(time(0));
		signoP = random()%2;
		signoT = random()%2;
		if (signoP==1)
			myActualElement->longitude = -random()%159;
		else
			myActualElement->longitude = random()%159;
/*
		if (signoT==1)
			myActualElement->latitude = -random()%31;
		else
			myActualElement->latitude = random()%31;
*/
		myActualElement->latitude = -30.; // TODO tiltAngle;

		randomPosition = FALSE;
	} else { // toca posicionarse en la siguiente hubicación sistemática
		myActualElement->longitude = longitudeScenePositions[nextLongitude];
		//myActualElement->latitude = latitudeScenePositions[nextLatitude];
		myActualElement->latitude = -30.; // TODO tiltAngle;

		if (nextLongitude == LONGITUDE_NUM_POSITIONS-1) {
			nextLongitude = 0;
/*			if (nextLatitude == 3)
				nextLatitude = 0;
			else nextLatitude ++;*/
		} else nextLongitude ++;

		//randomPosition = TRUE;
	}

	myActualElement->firstInstant = actualInstant; // instante de detección, en segundos
	myActualElement->lastInstant = myActualElement->firstInstant;
	// Si estamos aquí es porque acabamos de ver un paralelograma, y por tanto...
	myActualElement->saliency = MAX_SALIENCY; // ...cuando entre en el sistema tendrá saliencia mínima
	myActualElement->liveliness = MAX_LIVELINESS; // ...cuando entre en el sistema tendrá vida máxima
	myActualElement->type = 0; // elemento paralelogramo
	myActualElement->isVisited = FALSE;
	myActualElement->next = NULL;
}

inline void insertVirtualElement () {
	fillVirtualElement ();
	isMergedElementAndInsert ();
}

inline void updateCache () {
	int i;
	HPoint3D robotPos;
	struct CacheList *p, *q, *actualCache;

	p = cache;
	q = p;

	// Inicialmente vaciamos la anterior cache
	while (p != NULL) {
		q = p->next;
		free (p);
		p = q;
	}
	cache = NULL;
	p = cache;
	q = cache;

	// Pasamos de 2D -> 3D para la función distanceBetweenPoints
	robotPos.Z = 0.;
	robotPos.H = 1.;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) {
		robotPos.X = myencoders[0];
		robotPos.Y = myencoders[1];
		//printf ("updateCache: segment[%i].start = (%i) [0.2%f, 0.2%f, 0.2%f] y end = [0.2%f, 0.2%f, 0.2%f]. Robot = [0.2%f, 0.2%f]\n", i, groundSegments3D[i].isValid, groundSegments3D[i].start.position.X, groundSegments3D[i].start.position.Y, groundSegments3D[i].start.position.Z, groundSegments3D[i].end.position.X, groundSegments3D[i].end.position.Y, groundSegments3D[i].end.position.Z, robotPos.X, robotPos.Y);
		if ((groundSegments3D[i].isValid == 1) && ((distanceBetweenPoints (robotPos, groundSegments3D[i].start.position) < CACHE_SPACE) || (distanceBetweenPoints (robotPos, groundSegments3D[i].end.position) < CACHE_SPACE))) { // es un elemento en Cache y lo marcamos asi
			groundSegments3D[i].isInCache = TRUE;

			actualCache = NULL;
			actualCache = (struct CacheList *)malloc(sizeof(struct CacheList *));
			actualCache->segment = &(groundSegments3D[i]);
			actualCache->next = NULL;

			if (q != NULL) {
				q->next = actualCache;
				q = q->next;
			} else { // list = NULL, así que inicializamos
				cache = actualCache; // inicialización de list
				q = cache;
			}
		} else groundSegments3D[i].isInCache = FALSE;
	}

	timeToUpdateCache = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
}

inline void segmentsMaintenance () {
	struct CacheList *p;
	int isMerge;
	int areTheSame;

	p = cache;

	while (p != NULL)	{
		if (p->segment->isValid == 1) {
			isMerge = mergeSegment (*(p->segment), &areTheSame);

			if ((isMerge) && (!areTheSame)) { // si hemos logrado fusionarlo, sin que sea consigo mismo, lo anulamos
				p->segment->isValid = 0;
			}
		}
		p = p->next;
	}
	timeToMaintenance = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
}

inline void updateSegments () {
// Ahora en vez de por el tiempo en memoria lo vamos a hacer por lo que nos hemos movido desde que lo vimos...
	struct CacheList *p;

	p = cache;

	while (p != NULL) {
		if (p->segment->isValid == 1) {
			p->segment->traveled += abs(sqrt(pow(myencoders[0]-lastPosition.x,2) + pow(myencoders[1]-lastPosition.y,2)));
			if (p->segment->traveled > MAX_LIFE_INCREMENTS)
				p->segment->isValid = 0;
/*			if (segment.timestamp - actualInstant > SEGMENT_LIFE_TIME)
				p->segment->isValid = 0;
*/
		}
		p = p->next;
	}
	lastPosition.x = myencoders[0]; // al finalizar actualizamos la última posición registrada del robot a la actual
	lastPosition.y = myencoders[1];
}

inline void updateElements () {
	struct elementStruct *p, *q, *r, *virtualMax, *virtualPrevMax;
	virtualPrevMax = NULL;
	virtualMax = NULL;

	p = myElements;
	q = p;

	// 1º - Recorremos la lista eliminando todos los que se les ha acabado la vida :(
	while (p != NULL) {
		//if (DEBUG) printf ("updateElements: Search Loop\n");
		//if (DEBUG) printf ("myElements[%i]. Pos: [%.2f, %.2f], time in live = %0.2f, live = %.1f, sal = %.1f\n", elementsStructCounter, p->longitude, p->latitude, (instant - p->lastInstant), p->liveliness, p->saliency);
		if (p == myMaxSaliency) {
			virtualPrevMax = q;
			virtualMax = p;
		}

		if ((actualInstant - p->lastInstant) > TIME_TO_DEAD) { // esta antigua elemento será borrada de memoria
			if (DEBUG) printf ("updateElements: Deleting old element of memory...\n");
			if (p == myElements) {
				myElements = myElements->next;
				free (p);
				p = NULL;
				p = myElements;
			} else {
				q->next = p->next;
				free (p);
				p = NULL;
				p = q->next;
			}
			elementsStructCounter --;
		} else {
			p->liveliness -= LIVELINESS_DECREMENT;
			p->saliency += SALIENCY_INCREMENT;

			q = p;
			p = p->next; // preparamos a p para la siguiente vuelta :)
		}
	}

	p = virtualMax; // maxSaliency, y su previo
	r = virtualPrevMax;

	if ((virtualMax != NULL) && (analizedImage)) { // tenemos un elemento virtual que ha de ser eliminado en su caso
		if (DEBUG) printf ("updateElements: Virtual element analized --> Deleting...\n");

		if (virtualMax == myElements)
			myElements = myElements->next;
		else
			virtualPrevMax->next = virtualMax->next;

		free (virtualMax);
		elementsStructCounter --;
		getMaxSaliency ();

	} else if (p != NULL) { // p contendrá el valor de myMaxSaliency
		//if (DEBUG) printf ("updateElements: %p myElements[%i]. Pos: [%.2f, %.2f], type = %i, time in live = %.1f, live = %.1f, sal = %.1f\n", p, elementsStructCounter-1, p->longitude, p->latitude, p->type, (actualInstant - p->lastInstant), p->liveliness, p->saliency);
		if ((p->type != 0) && ((actualInstant - p->lastInstant) > FOLLOW_TIME)) { // si es una elemento real y se le ha pasado el tiempo de seguimiento
			if (DEBUG) printf ("updateElements: FOLLOW TIME expired\n");
			getMaxSaliency (); // cambiamos de maxSaliency

		} else if ((p->type != 0) && (p->liveliness <= LIVELINESS_TO_DEAD)) { // se ha muerto, es posible, ya que si se deja de ver... nos lo cargamos :P
			if (DEBUG) printf ("updateElements: This element has dead because of its liveliness\n");

			if (p == myElements)
				myElements = myElements->next;
			else
				r->next = p->next;

			free (p);
			elementsStructCounter --;
			getMaxSaliency ();

		} else if ((((p->type == 1) && (!checkedParallelogram)) ||
							 ((p->type == 2) && (!checkedFace)) ||
							 ((p->type == 3) && (!checkedArrow))) && (analizedImage)) { // no hemos corroborado el supueto elemento
			if (p == myElements)
				myElements = myElements->next;
			else
				r->next = p->next;

			free (p);
			elementsStructCounter --;
			getMaxSaliency ();
		}	else { // si es una elemento real, que no le ha pasado el tiempo, vamos actualizando su vida
			p->saliency = MIN_SALIENCY;
			p->liveliness += LIVELINESS_INCREMENT;
			if (p->liveliness > MAX_LIVELINESS) // para evitar saturación
				p->liveliness = MAX_LIVELINESS;
		}
	} // else getMaxSaliency (); // p = NULL

	if (elementsStructCounter != 0)
		timeToForcedSearch = elementsStructCounter * TIME_TO_FORCED_SEARCH;

	analizedImage = FALSE;
}

inline void loadCascade () {
	cascade = (CvHaarClassifierCascade*) cvLoad (cascade_name, 0, 0, 0 );
	if (!cascade) {
		fprintf(stderr, "visualSonar: Could not load classifier cascade\n");
		jdeshutdown(1);
	}
	storage = cvCreateMemStorage(0);
	storageTmp = cvCreateMemStorage(0);
}

/*****************************************************************************************************************************/
/****************************F U N C I O N E S   R E L A T I V A S   A   L A   N A V E G A C I Ó N****************************/
/*****************************************************************************************************************************/
inline float translateAngle (float angulo) {
	/* Transformamos un angulo dado en radianes en grados de tal manera k si el
	angulo introducido es 180>thetha>0 se kda tal cual y si es 180<theta<0 kda
	como -thetha*/ 
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; /* PASAMOS la entrada A GRADOS */
	auxi=angulo*100; /* Nos quedamos con 2 decimales */
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  /* Esta conversion la hacemos para quedarnos con 2 decimales */

	return salida;
}

inline int calculateForce (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return fuerza;
}

inline int sameAngleAndDistance (Tvoxel a,int distanciaPuntoMem,int anguloPuntoNuevo,int distanciaNueva) {
	int angulo_aux;
	int salida=FALSE;
	float margen = 500;
	int dist = FALSE;

	dist = distanciaPuntoMem < (distanciaNueva-margen);
	angulo_aux = (int)(atan2(a.y-myencoders[1],a.x-myencoders[0])*RADTODEG);
	salida = ((anguloPuntoNuevo <= (angulo_aux + 0.7)) && (anguloPuntoNuevo >= (angulo_aux - 0.7)));

	return salida && dist;
}

inline float transformoAngulo (float angulo) {
	// Transformamos un angulo dado en radianes en grados de tal manera que si el	angulo introducido es 180>thetha>0 se queda tal cual y si es 180<theta<0 queda	como -thetha
	float salida, auxi, auxi1;

	angulo=angulo*RADTODEG; // PASAMOS la entrada A GRADOS
	auxi=angulo*100; // Nos quedamos con 2 decimales
	auxi1=ceil(auxi);
	angulo=(auxi1/100);

	if ((angulo>180)&&(angulo<=360)) {
		salida=angulo-(2*M_PI*RADTODEG);
		if (salida>=-0.1) salida=0.00;
	}	else salida=angulo;

	if (salida>=360) salida=0.00;

	auxi=salida*100;
	auxi1=ceil(auxi);
	salida=(auxi1/100);  // Esta conversion la hacemos para quedarnos con 2 decimales

	return salida;
}

inline void guardarCoordObjetivo(TdatosResultante *res) {
	float auxi1;
	double radianes;

	(*res).direccionFuerzaAtractiva[0]=miObjetivo.x-myencoders[0];
	(*res).direccionFuerzaAtractiva[1]=miObjetivo.y-myencoders[1];

	(*res).anguloFuerzaAtractiva=atan2((*res).direccionFuerzaAtractiva[1],(*res).direccionFuerzaAtractiva[0]);
		
	(*res).moduloFuerzaAtractiva=moduloAtractiva;

	radianes=(*res).anguloFuerzaAtractiva;

	(*res).direccionFuerzaAtractiva[0]=cos(radianes)*(*res).moduloFuerzaAtractiva;
	(*res).direccionFuerzaAtractiva[1]=sin(radianes)*(*res).moduloFuerzaAtractiva;

	auxi1=transformoAngulo((*res).anguloFuerzaAtractiva);
	(*res).anguloFuerzaAtractiva=auxi1;
}

void calcVectorial(int xini, int yini, int xfin, int yfin, HPoint2D * u, HPoint2D * p) {
    /*Vector director*/
    u->x = (float) (xfin - xini);
    u->y = (float) (yfin - yini);
    u->h = 1.0;

    /*Punto*/
    p->x = (float) xini;
    p->y = (float) yini;
    p->h = 1.0;
}

inline void divideSegment (Segment3D segment) {
  HPoint3D start, end;
	HPoint2D u, p;
	struct HPoint3DList *myPoint, *lastPoint;
	float itmp;
	int i;
	float dist;

	lastPoint = segPointList;
	myPoint = lastPoint;

	while (lastPoint != NULL) {
		lastPoint = myPoint->next;
		free (myPoint);
		myPoint = lastPoint;
	}

	segPointList = NULL; // inicializamos puntero inicial y final	
	lastPoint = segPointList;
	
	start = segment.start.position;
	end = segment.end.position;

	dist = distanceBetweenPoints (start, end);

	if (dist > MIN_TAM_SEG_TO_DIVIDE) {
		calcVectorial(start.X, start.Y, end.X, end.Y, &u, &p);
    for(i=0;i<=100;i=i+25) { // dividimos el segmento en 5 partes
			itmp = ((float)i)/100.0;

			myPoint = NULL;
			myPoint = (struct HPoint3DList*)malloc(sizeof(struct HPoint3DList));
			myPoint->point.X = (float)(p.x + itmp*u.x);
			myPoint->point.Y = (float)(p.y + itmp*u.y);
			//if (DEBUG) printf ("divideSegment: storing point [%0.2f, %0.2f]...\n", myPoint->point.X, myPoint->point.Y);
			myPoint->point.Z = 0.;
			myPoint->point.H = 1;
			myPoint->next = NULL;

			if (segPointList != NULL) {
				lastPoint->next = myPoint;
				lastPoint = lastPoint->next;
			} else { // list = NULL, así que inicializamos
				segPointList = myPoint; // inicialización de list
				lastPoint = segPointList;
			}
		}
	}
}

inline int calculoFuerza (int distancia, int ang) {
  int fuerza;

  if (distancia!=0)
		if (distancia < alcancePeligroso)
			fuerza = K2/distancia; /* generamos MAS fuerza repulsiva; hay cierto peligro */
		else
			fuerza = K/distancia; /* generamos MENOS fuerza repulsiva; no se considera peligro */

  else fuerza=0;

  return fuerza;
}

inline int segmentIsInOverlappArea (Segment3D segment) {
	struct elementStruct *r;
	int isInside = FALSE;
	float dis1, dis2, dis3, dis4;

	r = myElements;

	while ((r != NULL) && (!isInside)) { // cuando encuentres una cara o flecha en la que solape, paramos de buscar
		if (r->type == 2) { // si es una cara
			dis1 = distanceBetweenPoints (segment.start.position, r->face.center.position);
			dis2 = distanceBetweenPoints (segment.end.position, r->face.center.position);

			isInside = ((dis1 < DIST_OVERLAPPING) || (dis2 < DIST_OVERLAPPING));
		} else if (r->type == 3) { // o si es una flecha
			dis1 = distanceBetweenPoints (segment.start.position, r->arrow.start);
			dis2 = distanceBetweenPoints (segment.end.position, r->arrow.start);
			dis3 = distanceBetweenPoints (segment.start.position, r->arrow.end);
			dis4 = distanceBetweenPoints (segment.end.position, r->arrow.end);

			isInside = ((dis1 < DIST_OVERLAPPING) || (dis2 < DIST_OVERLAPPING) || (dis3 < DIST_OVERLAPPING) || (dis4 < DIST_OVERLAPPING));
		}
		r = r->next;
	}

	return isInside;
}

inline void calculaResultante(TdatosResultante *res) {
	int angulo,ang_robot,diff_ang;
	int sumaX, sumaY;
	int distancia,modulo;
	float auxi, auxi1;
	double radianes;
	struct HPoint3DList *actualPoint;
	HPoint3D point;
	struct CacheList *p;

	sumaX=0;
	sumaY=0;
	p = cache;

	while (p != NULL) {
		if ((p->segment->isValid == TRUE) && (!segmentIsInOverlappArea (*(p->segment)))) {
			divideSegment (*(p->segment));
			/* calculo la distancia al robot y el angulo respecto a este */
			actualPoint = segPointList;
			while (actualPoint != NULL) {
				point.X = actualPoint->point.X;
				point.Y = actualPoint->point.Y;
				distancia = (((point.X-myencoders[0])*(point.X-myencoders[0]))+((point.Y-myencoders[1])*(point.Y-myencoders[1])));
				radianes = atan2((point.Y-myencoders[1]),(point.X-myencoders[0]));
				angulo = (int)(radianes*RADTODEG);
				ang_robot = (int)(myencoders[2]*RADTODEG);

				/* SUTILEZA PARA QUE EL ROBOT TENGA QUE GIRAR LO MENOS POSIBLE */
				if(ang_robot>=180) ang_robot=-360+ang_robot; /* =-(360-ang_robot)-->pasa a negativo */

				diff_ang=abs(angulo-ang_robot); /* calculamos el total que debería girar el robot */

				if (diff_ang>=180) diff_ang=360-diff_ang; /* mejor si gira por el camino más corto */
				/* FIN DE LA SUTILEZA */

				/* Miramos si el punto esta dentro del alcance en el que los puntos generan fuerza de repulsión. La distancia es distinta segun el angulo en el que se encuentre el punto respecto del robot */
				if (((diff_ang>45)&&(diff_ang<135)&&(distancia<alcanceLateral))||
					(((diff_ang<=45)||(diff_ang>=135))&&(distancia<alcance))) { 
					/* Primero miramos si la distancia es menor que la seguridad asignada. Si es menor giramos sin avanzar */
					if (((diff_ang>45)&&(diff_ang<135)&&(distancia<seguridadLateral))||
						((diff_ang<=45)&&(distancia<seguridad)))
	        
						(*res).avanzaEnGiro=FALSE; /* inicialmente está a TRUE */

					/* sumo al angulo 180 ya que lo que buscamos es fuerza repulsiva y queremos el angulo opuesto */
					radianes=radianes+(180.0*DEGTORAD);

					/* calculamos el modulo */
					modulo=calculoFuerza(distancia,diff_ang); /* FUERZA REPULSIVA */

					/* Añadimos las componentes a la suma de x y de y.*/
					sumaX+=cos(radianes)*modulo;
					sumaY+=sin(radianes)*modulo;	
				}
				actualPoint = actualPoint->next;
			} // fin while (actualPoint != NULL)
		}
		p = p->next;
	} // fin FOR

	(*res).direccionFuerzaRepulsiva[0]= sumaX;
	(*res).direccionFuerzaRepulsiva[1]= sumaY;

	/*if (DEBUG) printf("xRep:%d f_0.yRep:%d\n",(*res).direccionFuerzaRepulsiva[0],(*res).direccionFuerzaRepulsiva[1]);*/

	/* Calculo la fuerza resultante atribuyendo factores de proporción a la fuerza atractiva y repulsiva */
	(*res).direccionFuerzaResultante[0]=(alpha *(*res).direccionFuerzaAtractiva[0]) +(beta*(*res).direccionFuerzaRepulsiva[0]);
	(*res).direccionFuerzaResultante[1]=(alpha *(*res).direccionFuerzaAtractiva[1]) +(beta*(*res).direccionFuerzaRepulsiva[1]);

	sumaX=(*res).direccionFuerzaResultante[0];
	sumaY=(*res).direccionFuerzaResultante[1];

	(*res).moduloFuerzaResultante=sqrt((sumaX*sumaX)+(sumaY*sumaY));
	(*res).anguloFuerzaResultante=atan2(sumaY,sumaX);	  

	auxi1=transformoAngulo((*res).anguloFuerzaResultante);
	(*res).anguloFuerzaResultante=auxi1;
	/*if (DEBUG) printf("Angulo resultante:%0.2f\n",(*res).anguloFuerzaResultante);*/

	if ((sumaX==(*res).direccionFuerzaAtractiva[0])&&(sumaY==(*res).direccionFuerzaAtractiva[1])) { /* ¿? sumaX y sumaY han variado, por los factores aplicados...esto NO FUNCIONARÁ */
		/* en caso k no actuen fuerzas, solo tenemos la atractiva*/
		(*res).anguloFuerzaResultante=(*res).anguloFuerzaAtractiva;;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=auxi1/100;
	}

	if((*res).anguloFuerzaResultante==0.) {
		/* es mas facil detectar 6.28 k 2*Pi, por eso aproximamos*/
		(*res).anguloFuerzaResultante=2*M_PI;
		auxi=((*res).anguloFuerzaResultante)*100;
		auxi1=ceil(auxi);
		(*res).anguloFuerzaResultante=transformoAngulo(auxi1/100);
	}

	auxi=((*res).anguloFuerzaResultante)*100;
	auxi1=ceil(auxi);
	(*res).anguloFuerzaResultante=(auxi1/100);
}

inline int overpass () {
	return ((abs((int)(miObjetivo.x-myencoders[0]))<1100)&&(abs((int)(miObjetivo.y-myencoders[1]))<1100));
}

inline void getAnguloQueLlevo () {
	float auxi,auxi1;

	// Todo esto es para quedarnos con el ángulo que llevamos, pero sólo con 2 decimales
	resultante.anguloQueLlevo=myencoders[2];
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=auxi1/100;

	// Transformo el ángulo (sutileza por el recorrido más corto)
	auxi1=transformoAngulo(resultante.anguloQueLlevo);
	resultante.anguloQueLlevo=auxi1;

	// Y nos volvemos a quedar con 2 decimales
	auxi=resultante.anguloQueLlevo*100;
	auxi1=ceil(auxi);
	resultante.anguloQueLlevo=(auxi1/100);
}

inline void extendArrow (Arrow3D arrow, HPoint3D *myLocalObjetive) {
	float incremento;
	float deltaX, deltaY;

	incremento = 10.; // longitud a prolongar la flecha (10 x 210 mm)
//	ang = atan((arrow.end.Y - arrow.start.Y)/(arrow.end.X - arrow.start.X)); // ángulo de la flechita en el suelo (eje (X,Y))
	deltaX = arrow.end.X - arrow.start.X;
	deltaY = arrow.end.Y - arrow.start.Y;

	myLocalObjetive->X = arrow.start.X + incremento*deltaX;
	myLocalObjetive->Y = arrow.start.Y + incremento*deltaY;

	if (DEBUG) printf ("extendArrow: arrow.start = [%0.2f, %0.2f], arrow.end = [%0.2f, %0.2f], objetivo = [%0.2f, %0.2f]\n", arrow.start.X, arrow.start.Y, arrow.end.X, arrow.end.Y, myLocalObjetive->X, myLocalObjetive->Y);
}

inline void generateVirtualObjetive (HPoint3D *myLocalObjetive) {
	float dist;

	dist = 2000.; // longitud a prolongar la flecha (2 metros)

	myLocalObjetive->X = myencoders[0] + (dist * (cos(myencoders[2])));
	myLocalObjetive->Y = myencoders[1] + (dist * sin(myencoders[2]));
	if (DEBUG) printf ("virtualObjetive: robot position = [%0.2f, %0.2f], objetivo = [%0.2f, %0.2f]\n", myencoders[0], myencoders[1], myLocalObjetive->X, myLocalObjetive->Y);
}

inline void getObjetivo () {
	// busco si hay flechas en mi memoria atentiva, y tengo en cuenta la más cercana
	struct elementStruct *r;
	HPoint3D myPosition, myLocalObjetive;
	float actualDist, minDist = 9999999.;
	int found = FALSE;
	Arrow3D myArrow;

	myPosition.X = myencoders[0];
	myPosition.Y = myencoders[1];
	myPosition.Z = 0.;
	myPosition.H = 1.;

	r = myElements;

	while (r != NULL) { // cuando encuentres una cara o flecha en la que solape, paramos de buscar
		if (r->type == 3) { // si es una flecha
			found = TRUE;
			actualDist = distanceBetweenPoints (myPosition, r->arrow.start);
			if (actualDist < minDist) {
				minDist = actualDist;
				myArrow = r->arrow;
			}
		}
		r = r->next;
	} // cuando salgamos tendremos la flecha más cercana al robot, caso de haberla

	if (found) extendArrow (myArrow, &myLocalObjetive); // si hemos encontrado flecha, calculamos el objetivo según ésta
	else generateVirtualObjetive (&myLocalObjetive);

	miObjetivo.x = myLocalObjetive.X;
	miObjetivo.y = myLocalObjetive.Y;
	if (DEBUG) printf ("getObjetivo: objetivo elegido = [%0.2f, %0.2f]\n", miObjetivo.x, miObjetivo.y);
}

inline void checkRobotIsInObjetive () {
	if ((abs((int)(miObjetivo.x-myencoders[0]))<200)&&(abs((int)(miObjetivo.y-myencoders[1]))<200)) {   
		distanciaObjetivo=0;
		resultante.direccionFuerzaAtractiva[0]=0.0;
		resultante.direccionFuerzaAtractiva[1]=0.0;
	}	else distanciaObjetivo=1;
}

inline void getForcesParams () {
	xRep=resultante.direccionFuerzaRepulsiva[0]; // REPULSIVA
	yRep=resultante.direccionFuerzaRepulsiva[1];

	xAtr=resultante.direccionFuerzaAtractiva[0]; // ATRACTIVA
	yAtr=resultante.direccionFuerzaAtractiva[1];

	xRes=resultante.direccionFuerzaResultante[0]; // RESULTANTE = REPULSIVA vs. ATRACTIVA
	yRes=resultante.direccionFuerzaResultante[1];
}

inline void getActualWindow () {
	// actualizamos las coordenadas de los puntos extremos de la ventana de seguridad
	// windowA, windowB, windowC, windowD, windowE, windowF, windowG, windowH, windowI, windowJ
	windowA.x = frenteY*(cos(myencoders[2])) - huecoX*(sin(myencoders[2])) + myencoders[0];
	windowA.y = huecoX*cos(myencoders[2]) + frenteY*sin(myencoders[2]) + myencoders[1];

	windowB.x = frenteY*(cos(myencoders[2])) - (-huecoX)*(sin(myencoders[2])) + myencoders[0];
	windowB.y = (-huecoX)*cos(myencoders[2]) + frenteY*sin(myencoders[2]) + myencoders[1];

	windowC.x = ventanaY*(cos(myencoders[2])) - huecoX*(sin(myencoders[2])) + myencoders[0];
	windowC.y = huecoX*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowD.x = ventanaY*(cos(myencoders[2])) - ventanaX*(sin(myencoders[2])) + myencoders[0];
	windowD.y = ventanaX*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowE.x = ventanaY*(cos(myencoders[2])) - (-ventanaX)*(sin(myencoders[2])) + myencoders[0];
	windowE.y = (-ventanaX)*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowF.x = ventanaY*(cos(myencoders[2])) - (-huecoX)*(sin(myencoders[2])) + myencoders[0];
	windowF.y = (-huecoX)*cos(myencoders[2]) + ventanaY*sin(myencoders[2]) + myencoders[1];

	windowG.x = 0.*(cos(myencoders[2])) - huecoX*(sin(myencoders[2])) + myencoders[0];
	windowG.y = huecoX*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];

	windowH.x = 0.*(cos(myencoders[2])) - ventanaX*(sin(myencoders[2])) + myencoders[0];
	windowH.y = ventanaX*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];

	windowI.x = 0.*(cos(myencoders[2])) - (-ventanaX)*(sin(myencoders[2])) + myencoders[0];
	windowI.y = (-ventanaX)*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];

	windowJ.x = 0.*(cos(myencoders[2])) - (-huecoX)*(sin(myencoders[2])) + myencoders[0];
	windowJ.y = (-huecoX)*cos(myencoders[2]) + 0.*sin(myencoders[2]) + myencoders[1];
}

inline void buildParallelogramFront (Parallelogram3D *p1) {
	p1->p1.position.X = windowA.x;
	p1->p1.position.Y = windowA.y;
	p1->p1.position.Z = 0.;
	p1->p1.position.H = 1;

	p1->p2.position.X = windowC.x;
	p1->p2.position.Y = windowC.y;
	p1->p2.position.Z = 0.;
	p1->p2.position.H = 1;

	p1->p3.position.X = windowF.x;
	p1->p3.position.Y = windowF.y;
	p1->p3.position.Z = 0.;
	p1->p3.position.H = 1;

	p1->p4.position.X = windowB.x;
	p1->p4.position.Y = windowB.y;
	p1->p4.position.Z = 0.;
	p1->p4.position.H = 1;
}

inline void buildParallelogramWindow (Parallelogram3D *p2) {
	p2->p1.position.X = windowC.x;
	p2->p1.position.Y = windowC.y;
	p2->p1.position.Z = 0.;
	p2->p1.position.H = 1;

	p2->p2.position.X = windowG.x;
	p2->p2.position.Y = windowG.y;
	p2->p2.position.Z = 0.;
	p2->p2.position.H = 1;

	p2->p3.position.X = windowH.x;
	p2->p3.position.Y = windowH.y;
	p2->p3.position.Z = 0.;
	p2->p3.position.H = 1;

	p2->p4.position.X = windowD.x;
	p2->p4.position.Y = windowD.y;
	p2->p4.position.Z = 0.;
	p2->p4.position.H = 1;
}

inline void buildParallelogramBeside1 (Parallelogram3D *p3) {
	p3->p1.position.X = windowE.x;
	p3->p1.position.Y = windowE.y;
	p3->p1.position.Z = 0.;
	p3->p1.position.H = 1;

	p3->p2.position.X = windowI.x;
	p3->p2.position.Y = windowI.y;
	p3->p2.position.Z = 0.;
	p3->p2.position.H = 1;

	p3->p3.position.X = windowJ.x;
	p3->p3.position.Y = windowJ.y;
	p3->p3.position.Z = 0.;
	p3->p3.position.H = 1;

	p3->p4.position.X = windowF.x;
	p3->p4.position.Y = windowF.y;
	p3->p4.position.Z = 0.;
	p3->p4.position.H = 1;
}

inline void buildParallelogramBeside2 (Parallelogram3D *p4) {
	p4->p1.position.X = windowD.x;
	p4->p1.position.Y = windowD.y;
	p4->p1.position.Z = 0.;
	p4->p1.position.H = 1;

	p4->p2.position.X = windowH.x;
	p4->p2.position.Y = windowH.y;
	p4->p2.position.Z = 0.;
	p4->p2.position.H = 1;

	p4->p3.position.X = windowI.x;
	p4->p3.position.Y = windowI.y;
	p4->p3.position.Z = 0.;
	p4->p3.position.H = 1;

	p4->p4.position.X = windowE.x;
	p4->p4.position.Y = windowE.y;
	p4->p4.position.Z = 0.;
	p4->p4.position.H = 1;
}
/*
int pip (HPoint3D p, Parallelogram3D polygon) {
	int i,j;
	int num_intersec=0;
	Segment2D horiz;
	Tpoint q;
	Segment3D mysegments[4];

	mysegments[0].start = polygon.p1;
	mysegments[0].end = polygon.p2;
	mysegments[1].start = polygon.p2;
	mysegments[1].end = polygon.p3;
	mysegments[2].start = polygon.p3;
	mysegments[2].end = polygon.p4;
	mysegments[3].start = polygon.p4;
	mysegments[3].end = polygon.p1;

	point[0] = s1.start.position;
	point[1] = s1.end.position;
	point[2] = s2.start.position;
	point[3] = s2.end.position;

	// hallar el segmento horizontal formado desde el punto p
	horiz.start = 0.;
	horiz.end = p.end;

	for (i=0; i<4; i++){

	// Comprobamos si el punto esta dentro del segmento, si esta entonces
	// paramos la busqueda forzando el numero de intersecciones a 1 (impar)
	// ya que el punto pertenece al poligono

	if(point_in_segment(polygon->segments[i],p)){

	if(DEBUG)
	printf("in segment %.2f:%.2f ---> %.2f:%.2f \n",
	polygon->segments[i].orig.u,
	polygon->segments[i].orig.v,
	polygon->segments[i].end.u,
	polygon->segments[i].end.v);

	num_intersec=1;
	break;

	}

	// comprobar si el segmento intersecta con el segmento horizontal
	// formado desde el punto recibido como argumento

	if (point_may_intersec(polygon->segments[i],p)
	&&
	get_segments_intersec(polygon->segments[i], horiz, &q))

	{

	if (DEBUG){
	printf("segment %.2f:%.2f ---> %.2f:%.2f \n",
	polygon->segments[i].orig.u,
	polygon->segments[i].orig.v,
	polygon->segments[i].end.u,
	polygon->segments[i].end.v);

	printf("--> %.2f:%.2f\n",q.u,q.v);
	}

	// Los dos segmentos se intersectan si la intersección de sus rectas se 
	// encuentra a la derecha del punto p

	if (q.u > p.u){
	if(DEBUG)
	printf(" hit\n");
	num_intersec++;
	}

	if (DEBUG) printf("\n");

	}
	}

	// Si el numero de interseccion es IMPAR entonces el punto esta dentro del poligno
	return (num_intersec % 2);
}
*/

inline int isThinPlace () {
	struct CacheList *p;
	int found = FALSE;
	p = cache;
	Parallelogram3D p1, p2, p3, p4;

	buildParallelogramFront (&p1);
	buildParallelogramWindow (&p2);
	buildParallelogramBeside1 (&p3);
	buildParallelogramBeside2 (&p4);

	// TODO: hacer la función PIP
	// si no hay en ventana ningún obstaculo y si no hay en el frente, comprobamos que sí hay a los lados
	while ((p != NULL) && (!found)) { // vemos si encuentra algo que fuerce a que sea un sitio estrecho
		if ((pip (p->segment->start.position, p1)) || (pip (p->segment->end.position, p1)))
			found = TRUE;
		else if ((pip (p->segment->start.position, p2)) || (pip (p->segment->end.position, p2)))
			found = TRUE;
		else if ((pip (p->segment->start.position, p3)) || (pip (p->segment->end.position, p3)))
			found = FALSE;
		else if ((pip (p->segment->start.position, p4)) || (pip (p->segment->end.position, p4)))
			found = FALSE;

		p = p->next;
	} // seguiremos a menos que haya algo que me diga lo contrario y no sea por tanto un sitio estrecho

	return (!found); // devuelve si estamos en sitio estrecho
}

inline void commandVelocity () {
	difAngle=(resultante.anguloFuerzaResultante)-(resultante.anguloQueLlevo);

	// Sutileza para que el robot gire por el lado de menos giro
	if (difAngle>180) difAngle=difAngle-360;
	if (difAngle<-180) difAngle=360+difAngle;

	if (!resultante.avanzaEnGiro) peligro = 1;
	else peligro = 0;

	if (distanciaObjetivo==0) { // estamos en el objetivo
		if (DEBUG) printf ("commandVelocity: Estamos en el objetivo\n");
		*myv=0.0;
		*myw=0.0;
		vActual=*myv;
		wActual=*myw;
	} else { // reglas AD-HOC
//		if ((thinPlace ()) && (!overpass())) { // TODO COMPORTAMIENTO VFF VS VENTANA
//			*myv = vMax; 
//			*myw = 0.0; // a toa leche, podemos seguir recto por el sitio estrecho
//		} else { // COMPORTAMIENTO VFF PURO
			if(peligro==1) { // LO MAS URGENTE, SI HAY PELIGRO INMINENTE, GIRAMOS LENTAMENTE
				if (DEBUG) printf ("commandVelocity: Hay Peligro. Giramos lentamente\n");
				if(difAngle<0) {*myv=0.0; *myw=wNegMax/2;}
				else {*myv=0.0; *myw=wPosMax/2;}
			}	else if (abs(difAngle)<limiteTrayectoriaSimilar) {
				if (DEBUG) printf ("commandVelocity: No hay Peligro. Viento en popa\n");
				*myv=vMax; *myw=0.0; // VIENTO EN POPA
			} else if (abs(difAngle)>limiteTrayectoriaDesigual) { // GIRAMOS RAPIDAMENTE
				if (DEBUG) printf ("commandVelocity: Giramos rápidamente\n");
				if (difAngle<0) {*myv=0.0; *myw=wNegMax;}
				else {*myv=0.0; *myw=wPosMax;}
			}	else if (difAngle<0) { // MITAD, MITAD... (FIFTY, FIFTY)
				if (DEBUG) printf ("commandVelocity: Mitad, mitad... (fifty, fifty)\n");
				*myv=vMax/2; *myw=wNegMax/2;
			} else {
				*myv=vMax/2; *myw=wPosMax/2;
			}
	} // fin reglas AD-HOC
}

inline void comportamientoVFF() {
	getAnguloQueLlevo (); // Ángulo que llevo

	getObjetivo ();
	if (DEBUG) printf ("getObjetivo OK\n");
	guardarCoordObjetivo(result); // guardamos la FUERZA ATRACTIVA
	if (DEBUG) printf ("guardarCoordObjetivo OK\n");
	resultante.avanzaEnGiro=TRUE; // si no hay obstaculos por debajo de la seguridad se avanza a la vez que se gira

	calculaResultante(result); // Función importante donde calculamos las fuerzas según la memoria de segmentos
	if (DEBUG) printf ("calculaResultante OK\n");
	checkRobotIsInObjetive (); // Miro si he alcanzado el objetivo
	if (DEBUG) printf ("checkRobotIsInObjetive OK\n");
	getForcesParams (); // Obtenemos los valores de fuerzas, para su posterior pintado
	if (DEBUG) printf ("getForcesParams OK\n");
	commandVelocity (); // Comandamos v y w del robot, según la diferencia de ángulo
}

/*****************************************************************************************************************************/
/*****************************************************************************************************************************/
/*****************************************************************************************************************************/
inline void deletePreviousVirtual () {
	struct elementStruct *p, *q;
	int found = FALSE;

	p = myElements;
	q = p;

	while ((p != NULL) && (!found)) {
		if (p->type == 0) {
			if (p == myElements) { // es el primer elemento de la lista, por lo que
				myElements = myElements->next;
			} else {
				q->next = p->next;
			}	
			found = TRUE;
			free (p);
			p = NULL;
			elementsStructCounter --;
		} else {
			q = p;
			p = p->next;
		}
	}
}

inline void chooseState () {
	if ((myActualState == think) && (((myElements == NULL) || (myMaxSaliency == NULL)) || ((myMaxSaliency != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)))) {
		if (DEBUG) printf ("chooseState: 1ª opcion\n");
		if ((myMaxSaliency != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)) {
			isForcedSearch = TRUE; // para que a la siguiente vuelta tengamos el flag activado y hagamos búsqueda
			timeForced = actualInstant;
			if (DEBUG) printf ("Activamos flag timeForced\n");
		}
		if (DEBUG) printf ("FORCED SEARCH\n");

		if ((myMaxSaliency != NULL) && (myMaxSaliency->type == 0)) { // elemento virtual
			if (myElements == myMaxSaliency) {
				myElements = myElements->next;
				free (myMaxSaliency);
				elementsStructCounter --;
			}
			myMaxSaliency = NULL;
			myPrevMaxSaliency = NULL;
		} else { // lo buscamos y lo eliminamos
			deletePreviousVirtual ();
		}

		insertVirtualElement (); // generaremos un destino si no hay destino o si, aun habiendo hay una búsqueda forzada
		getMaxSaliency ();
	}

	else if (((myActualState == think) && ((myElements != NULL) && (myMaxSaliency != NULL))) || (isForcedSearch == TRUE)) {
		if (DEBUG) printf ("chooseState: 2ª opcion\n");
		myActualState = search;
		isForcedSearch = FALSE;
	}

	else if ((myActualState == search) && (completedSearch)) {
		if (DEBUG) printf ("chooseState: 3ª opcion\n");
		myActualState = analizeSearch;
		completedSearch = FALSE; // TODO: comentar para poner cuello fijo
	}

	else if (myActualState == analizeSearch) {
		if (DEBUG) printf ("chooseState: 4ª opcion\n");
		myActualState = think; // una vez que hayamos analizado correctamente la imagen en curso
	}
}

inline void runState () {
	switch (myActualState) {
		case think: if (DEBUG) printf ("runState: state think\n"); // thinkObjetive ();
		break;
		case search: if (DEBUG) printf ("runState: state search\n"); searchObjetive (); // TODO: comentar para poner cuello fijo
		break;
		case analizeSearch: if (DEBUG) printf ("runState: state analizeSearch\n"); analizeImage ();
		break;
	}
}

inline void visualSonar_iteration() {
	static char d = 0;
	speedcounter(visualSonar_id);

	usleep (1);
	pthread_mutex_lock(&main_mutex);

	actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

	//comportamientoVFF (); // TEMPORALMENTE SÓLO HACEMOS ESPIRAL

  //TODO: descomentar esto para poner el cuello fijo, dando vueltas el cacharro	
	//*myv=60; *myw=6;
	completedSearch = TRUE;

	getActualWindow ();

	if (d == 0) loadCascade ();

	if (!completedMovement) { // agachamos la cámara inicialmente
		moveInitialTilt ();
	} else {
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		updateSegments (); // en función del tiempo, actualizamos los segmentos que tenemos en memoria
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Updating Segments = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);
		if (DEBUG) printf ("updateSegments OK\n");
		if (actualInstant - timeToUpdateCache > TIME_UPDATE_CACHE) {
			//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
			updateCache ();
			//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
			//printf ("Updating Cache = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);
		}
		if (actualInstant - timeToMaintenance > TIME_MAINTENANCE) { // mantenimiento de los segmentos en memoria
			segmentsMaintenance ();
		}
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		updateElements (); // en función del tiempo, actualizaremos: vida, saliencia y quitar muertos
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Updating Elements = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);
		if (DEBUG) printf ("updateElements OK\n");
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		chooseState (); // actualizamos (en su caso) myActualState
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Choosing state = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);
		if (DEBUG) printf ("chooseState OK\n");
		//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		runState (); // ejecutamos según el estado que sea
		//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		//printf ("Running state = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);
		if (DEBUG) printf ("run OK\n");
	}

	pthread_mutex_unlock(&main_mutex);
	usleep (1);
}

/*Importar símbolos*/
inline void visualSonar_imports() {
	/* importar colorA */
	mycolorA = (char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");
	mycolorAwidth	= (int*) myimport("colorA", "width");
	mycolorAheight = (int*) myimport("colorA", "height");

	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL){
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}

	/* importamos motors de pantilt */
  mylongitude=myimport("ptmotors", "longitude");
  mylatitude=myimport ("ptmotors", "latitude");
  mylongitude_speed=myimport("ptmotors", "longitude_speed");
  mylatitude_speed=myimport("ptmotors","latitude_speed");
  
  max_pan=myimport("ptmotors", "max_longitude");
  max_tilt=myimport("ptmotors", "max_latitude");
  min_pan=myimport("ptmotors", "min_longitude");
  min_tilt=myimport("ptmotors", "min_latitude");

	ptmotorsrun=myimport("ptmotors","run");
	ptmotorsstop=myimport("ptmotors","stop");

  /* importamos encoders de pantilt */
  mypan_angle=myimport("ptencoders", "pan_angle");
  mytilt_angle=myimport("ptencoders", "tilt_angle");

  ptencodersrun=myimport("ptencoders", "run");
  ptencodersstop=myimport("ptencoders", "stop");

	/* importamos encoders y motores de la base */
	myencoders=(float *)myimport("encoders","jde_robot");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

  myv=(float *)myimport("motors","v");
  myw=(float *)myimport("motors","w");
  motorsrun=(runFn)myimport("motors","run");
  motorsstop=(stopFn)myimport("motors","stop");
}

inline void visualSonar_stop()
{
	colorAstop();

	RGB2HSV_destroyTable();

	pthread_mutex_lock(&(all[visualSonar_id].mymutex));
	put_state(visualSonar_id,slept);
	if (DEBUG) printf("visualSonar: off\n");
	pthread_mutex_unlock(&(all[visualSonar_id].mymutex));

	free(imageAfiltered);
	remove_image(filteredImageRGB);
}

inline void visualSonar_run(int father, int *brothers, arbitration fn)
{
	int i;

  pthread_mutex_lock(&(all[visualSonar_id].mymutex)); // CERROJO -- LOCK
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[visualSonar_id].children[i]=FALSE;
 
  all[visualSonar_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) visualSonar_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {visualSonar_brothers[i]=brothers[i];i++;}
    }
  visualSonar_callforarbitration=fn;
  put_state(visualSonar_id,notready);
  if (DEBUG) printf("visualSonar: on\n");
  visualSonar_imports();

	// inmediatamente a continuación vemos su posición
	lastPosition.x = myencoders[0];
	lastPosition.y = myencoders[1];

  /* Wake up drivers schemas */	
  colorArun(visualSonar_id, NULL, NULL);

  pthread_cond_signal(&(all[visualSonar_id].condition));
  pthread_mutex_unlock(&(all[visualSonar_id].mymutex)); // CERROJO -- UNLOCK
}

inline void *visualSonar_thread(void *not_used) {
	struct timeval a,b;
	long n=0; /* iteration */
	long next,bb,aa;

	for(;;)	{
		pthread_mutex_lock(&(all[visualSonar_id].mymutex));

		if (all[visualSonar_id].state==slept) {
			pthread_cond_wait(&(all[visualSonar_id].condition),&(all[visualSonar_id].mymutex));
			pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
		}
		else {
			if (all[visualSonar_id].state==notready) /* check preconditions. For now, preconditions are always satisfied*/
				put_state(visualSonar_id,ready);

			if (all[visualSonar_id].state==ready) { /* check brothers and arbitrate. For now this is the only winner */
				put_state(visualSonar_id,winner);
  			*mylongitude_speed= 0.0;
  			*mylatitude_speed= 0.0;
	      all[visualSonar_id].children[(*(int *)myimport("ptencoders","id"))]=TRUE;
	      all[visualSonar_id].children[(*(int *)myimport("ptmotors","id"))]=TRUE;
	      all[visualSonar_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      all[visualSonar_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      ptencodersrun(visualSonar_id,NULL,NULL);
	      ptmotorsrun(visualSonar_id,NULL,NULL);
	      encodersrun(visualSonar_id,NULL,NULL);
	      motorsrun(visualSonar_id,NULL,NULL);

				gettimeofday(&a,NULL);
				aa=a.tv_sec*1000000+a.tv_usec;
				n=0;
			}

			if (all[visualSonar_id].state==winner) {
				/* I'm the winner and must execute my iteration */
				pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
				/* gettimeofday(&a,NULL); */
				n++;
				visualSonar_iteration();
				gettimeofday(&b,NULL);
				bb=b.tv_sec*1000000+b.tv_usec;
				next=aa+(n+1)*(long)visualSonar_cycle*1000-bb;

				if (next>5000) {
					usleep(next-5000); /* discounts 5ms taken by calling usleep itself, on average */
				}	else {
					usleep(25000); /* If iteration takes a long time, sleep 25 ms to avoid overload */
				}
			}	else { /* just let this iteration go away. overhead time negligible */
				pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
				usleep(visualSonar_cycle*1000);
			}
		}
	}
}

inline void visualSonar_terminate()
{
  pthread_mutex_lock(&(all[visualSonar_id].mymutex));
  visualSonar_stop();  
  pthread_mutex_unlock(&(all[visualSonar_id].mymutex));
  sleep(2);
}

/*Exportar símbolos*/
inline void visualSonar_exports() {
  myexport("visualSonar","cycle",&visualSonar_cycle);
  myexport("visualSonar","resume",(void *)visualSonar_run);
  myexport("visualSonar","suspend",(void *)visualSonar_stop);
}

inline void initCameras () {
	// Valores iniciales para la cámara virtual con la que observo la escena de puntos 3D en el suelo
  virtualcam0.position.X=4000.;
  virtualcam0.position.Y=4000.;
  virtualcam0.position.Z=6000.;
  virtualcam0.position.H=1.;
  virtualcam0.foa.X=0.;
  virtualcam0.foa.Y=0.;
  virtualcam0.foa.Z=0.;
  virtualcam0.position.H=1.;
  virtualcam0.roll=0.;

	// Valores iniciales para la cámara virtual con la que observo la escena
  virtualcam1.position.X=4000.;
  virtualcam1.position.Y=4000.;
  virtualcam1.position.Z=6000.;
  virtualcam1.position.H=1.;
  virtualcam1.foa.X=0.;
  virtualcam1.foa.Y=0.;
  virtualcam1.foa.Z=0.;
  virtualcam1.position.H=1.;
  virtualcam1.roll=0.;

	// Valores para la cámara virtual con la que observo el entorno de visualSonar al robot
  virtualcam2.position.X=4000.;
  virtualcam2.position.Y=4000.;
  virtualcam2.position.Z=6000.;
  virtualcam2.position.H=1.;
  virtualcam2.foa.X=0.;
  virtualcam2.foa.Y=0.;
  virtualcam2.foa.Z=0.;
  virtualcam2.foa.H=1.;
  virtualcam2.roll=0.;

	// Valores para la cámara virtual de la esquina derecha del laboratorio (myColorA)
  roboticLabCam0.position.X=7875.000000;
  roboticLabCam0.position.Y=-514.000000;
  roboticLabCam0.position.Z=3000.000000;
  roboticLabCam0.position.H=1.;
  roboticLabCam0.foa.X=6264.000000;
  roboticLabCam0.foa.Y=785.0000000;
  roboticLabCam0.foa.Z=1950.00000;
  roboticLabCam0.foa.H=1.;
  roboticLabCam0.fdistx=405.399994;
	roboticLabCam0.fdisty=roboticLabCam0.fdistx;
	roboticLabCam0.skew=0.;
  roboticLabCam0.u0=142.600006;
  roboticLabCam0.v0=150.399994;
  roboticLabCam0.roll=3.107262;

	// Valores para la cámara virtual de la esquina derecha de enfrente del laboratorio (myColorB)
  roboticLabCam1.position.X=7875.000000;
  roboticLabCam1.position.Y=4749.000000;
  roboticLabCam1.position.Z=3000.000000;
  roboticLabCam1.position.H=1.;
  roboticLabCam1.foa.X=6264.000000;
  roboticLabCam1.foa.Y=3700.000000;
  roboticLabCam1.foa.Z=1950.00000;
  roboticLabCam1.foa.H=1.;
	roboticLabCam1.fdistx=405.399994;
	roboticLabCam1.fdisty=roboticLabCam1.fdistx;
	roboticLabCam1.skew=0.;
  roboticLabCam1.u0=142.600006;
  roboticLabCam1.v0=142.600006;
  roboticLabCam1.roll=3.007028;

	// Valores para la cámara virtual de la esquina izquierda de enfrente del laboratorio (myColorC)
  roboticLabCam2.position.X=50.000000;
  roboticLabCam2.position.Y=4471.000000;
  roboticLabCam2.position.Z=2955.000000;
  roboticLabCam2.position.H=1.;
  roboticLabCam2.foa.X=2432.399902;
  roboticLabCam2.foa.Y=2918.000000;
  roboticLabCam2.foa.Z=1300.000000;
  roboticLabCam2.foa.H=1.;
	roboticLabCam2.fdistx=405.399994;
	roboticLabCam2.fdisty=roboticLabCam2.fdistx;
	roboticLabCam2.skew=0.;
  roboticLabCam2.u0=142.600006;
  roboticLabCam2.v0=150.399994;
  roboticLabCam2.roll=3.107262;

	// Valores para la cámara virtual de la esquina izquierda del laboratorio (myColorD)
  roboticLabCam3.position.X=50.000000;
  roboticLabCam3.position.Y=100.000000;
  roboticLabCam3.position.Z=2955.000000;
  roboticLabCam3.position.H=1.;
  roboticLabCam3.foa.X=2160.000000;
  roboticLabCam3.foa.Y=1151.000000;
  roboticLabCam3.foa.Z=1550.000000;
  roboticLabCam3.foa.H=1.;
	roboticLabCam3.fdistx=405.399994;
	roboticLabCam3.fdisty=roboticLabCam3.fdistx;
	roboticLabCam3.skew=0.;
  roboticLabCam3.u0=142.600006;
  roboticLabCam3.v0=150.399994;
  roboticLabCam3.roll=2.956911;

	// Valores para la cámara REAL del ROBOT. Valores iniciales, que no se usan ya que luego los modificamos convenientemente :)
  robotCamera.position.X=116.407570;
  robotCamera.position.Y=400.009766;
  robotCamera.position.Z=449.958099;
  robotCamera.position.H=1.;
  robotCamera.foa.X=930.;
  robotCamera.foa.Y=330.;
  robotCamera.foa.Z=0.;
  robotCamera.foa.H=1.;
	robotCamera.fdistx=425.871368;
	robotCamera.fdisty=robotCamera.fdistx;
	robotCamera.skew=0.;
	robotCamera.u0=98.245613;
  robotCamera.v0=164.678360;
  robotCamera.roll=-0.019950;
	robotCamera.rows=SIFNTSC_ROWS;
	robotCamera.columns=SIFNTSC_COLUMNS;

  /*ceilLabCam.position.X=-1000.;//260.000000;
  ceilLabCam.position.Y=1760.000000;
  ceilLabCam.position.Z=2800.;//400.000000;
  ceilLabCam.position.H=1.;
  ceilLabCam.foa.X=4710.000000;
  ceilLabCam.foa.Y=1760.000000;
  ceilLabCam.foa.Z=0.000000;
  ceilLabCam.foa.H=1.;
	ceilLabCam.fdistx=408.350891;
	ceilLabCam.fdisty=ceilLabCam.fdistx;
	ceilLabCam.skew=0.;
  ceilLabCam.u0=156.;
  ceilLabCam.v0=200.233917;
  ceilLabCam.roll=(PI/2)-0.059950;*/

  ceilLabCam.position.X=4720.;
  ceilLabCam.position.Y=2185.;
  ceilLabCam.position.Z=8000.;//400.000000;
  ceilLabCam.position.H=1.;
  ceilLabCam.foa.X=4720.;//4720.;
  ceilLabCam.foa.Y=2184.9;//2185.;
  ceilLabCam.foa.Z=0.000000;
  ceilLabCam.foa.H=1.;
	ceilLabCam.fdistx=600.;//408.350891;
	ceilLabCam.fdisty=ceilLabCam.fdistx;
	ceilLabCam.skew=0.;
  ceilLabCam.u0=240.;
  ceilLabCam.v0=320.;
  ceilLabCam.roll=-0.019950;//-PI/4;
	ceilLabCam.rows=480;
	ceilLabCam.columns=640;

	update_camera_matrix (&roboticLabCam0);
	update_camera_matrix (&roboticLabCam1);
	update_camera_matrix (&roboticLabCam2);
	update_camera_matrix (&roboticLabCam3);
	update_camera_matrix (&robotCamera);
	update_camera_matrix (&ceilLabCam);
}

inline void initAttentionParams () {
	pantiltStill = FALSE;
	robotStill = TRUE;
	numFlashes = 0;
	numSegments = 0;
	incNumSegments = 0;
}

inline void initSaliencyParams () {
	myElements = NULL;
	myMaxSaliency = NULL;
	myPrevMaxSaliency = NULL;
 	myActualElement = NULL;
	seg2y3Dlist = NULL;
	segPointList = NULL;
	elementsStructCounter = 0;
	completedSearch = FALSE;
	analizedImage = FALSE;
	checkedParallelogram = FALSE;
	checkedFace = FALSE;
	checkedArrow = FALSE;
	completedTrack = FALSE;
	myActualState = think;
	timeToForcedSearch = TIME_TO_FORCED_SEARCH;
	isForcedSearch = FALSE;
	randomPosition = FALSE;
	nextLatitude = 0;
	nextLongitude = 0;
	completedMovement = FALSE;
	actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	timeForced = actualInstant;
	stopInstant = actualInstant;
	timeToUpdateCache = actualInstant;
	timeToMaintenance = actualInstant;
	timeToSaveImage = actualInstant;
	comeFromCenter = TRUE;
	last_full_movement = right;
	navigationTime = 0.;
	imageNumber = 0;
}

inline void initPanTilt () {
	tiltAngle = -30.;//-(float) atan2 (480,600)*360./(2.*PI); // grados
	panAngle = 0.;//-1.5; // grados
  speed_y = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(tiltAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));
  speed_x = VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(panAngle)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));
}

inline void initGUIParams () {
	sliderPANTILT_BASE_HEIGHT = PANTILT_BASE_HEIGHT;
	sliderISIGHT_OPTICAL_CENTER = ISIGHT_OPTICAL_CENTER;
	sliderTILT_HEIGHT = TILT_HEIGHT;
	sliderCAMERA_TILT_HEIGHT = CAMERA_TILT_HEIGHT;
	sliderPANTILT_BASE_X = PANTILT_BASE_X;
	sliderThreshold = BORDER_THRESHOLD;
  actualCameraView = 5; // empezamos con la cámara del robot
	showPredictions = TRUE;
	showBorders = TRUE;
	showRefuted = TRUE;
	processImageButton = TRUE;

	pixel_camA.x = SIFNTSC_COLUMNS/2; // parameters extracted from Redo's calibrator gui
	pixel_camA.y = SIFNTSC_ROWS/2;

	faceCenter.x = 9999;
	faceCenter.y = 9999;
}

inline void initImagesMemory () {
	image = (char*) malloc(SIFNTSC_COLUMNS * SIFNTSC_ROWS * 3);
  imageAfiltered = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
  filteredImageRGB = create_image(SIFNTSC_COLUMNS, SIFNTSC_ROWS, 3);
	system ("rm memoryImage*");

	RGB2HSV_init();
	RGB2HSV_createTable();

	myHSV = (struct HSV*)malloc(sizeof(struct HSV));
}

inline void initMemoryParams () {
	int i;

	for (i = 0; i < MAX_LINES_IN_MEMORY; i++) { // inicialización de segmentos en memoria
		groundSegments3D[i].isValid = FALSE;
		groundSegments3D[i].traveled = 0.;
		groundSegments3D[i].isWellPredicted = FALSE;
		groundSegments3D[i].isInCache = FALSE;
	}
}

inline void visualSonar_init(char *configfile) {
  pthread_mutex_lock(&(all[visualSonar_id].mymutex)); // CERROJO -- LOCK

  if (DEBUG) printf("visualSonar schema started up\n");
  visualSonar_exports();
  put_state(visualSonar_id,slept);
  pthread_create(&(all[visualSonar_id].mythread),NULL,visualSonar_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  if (DEBUG) printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  if (DEBUG) printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[visualSonar_id].mymutex)); // CERROJO -- UNLOCK

  result = &resultante;
	miObjetivo.x = 8000.; // 8 metros por delante
	miObjetivo.y = 1760.;

	initCameras ();
	initPanTilt ();
	initGUIParams ();
	initAttentionParams ();
	initSaliencyParams ();
	initMemoryParams ();
	initImagesMemory ();
}

inline static int initFloorOGL (int w, int h) {
	glClearColor(0.f, 0.8f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	glOrtho (0, w, h, 0, 0, 1);

	return 0;
}

inline static int initvisualSonarOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
	GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat local_view[] = {0.0};

	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* With this, the pioneer appears correctly, but the cubes don't */
	glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv (GL_LIGHT0, GL_POSITION, position);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable (GL_LIGHT0);
	/*glEnable (GL_LIGHTING);*/

	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

inline void initWorldCanvasSettings () {
  float r,lati,longi;

	if (!flashImage) {
	  if (foa_mode) {
	    longi=2*PI*mouse_on_visualSonarcanvas.x/360.;
	    lati=2*PI*mouse_on_visualSonarcanvas.y/360.;
	    r=500;
	    virtualcam2.position.X=radius*(virtualcam2.position.X)/radius_old;
	    virtualcam2.position.Y=radius*(virtualcam2.position.Y)/radius_old;
	    virtualcam2.position.Z=radius*(virtualcam2.position.Z)/radius_old;
	    if (centrado==0){
				//Si centrado = 0 se ha pulsado el boton de centrar. Si vale 1, no está pulsado.
				virtualcam2.foa.X=r*cos(lati)*cos(longi);
				virtualcam2.foa.Y=r*cos(lati)*sin(longi);
				virtualcam2.foa.Z=r*sin(lati);
			}
	  }
	  
	  if (cam_mode) {
			centrado = 0;

	    longi=2*PI*mouse_on_visualSonarcanvas.x/360.;
	    lati=2*PI*mouse_on_visualSonarcanvas.y/360.;
	    
	    virtualcam2.position.X=radius*cos(lati)*cos(longi);
	    virtualcam2.position.Y=radius*cos(lati)*sin(longi);
	    virtualcam2.position.Z=radius*sin(lati);
	  }
		radius_old=radius;
	}

	initvisualSonarOGL(640,480);
	  
	/* Virtual camera */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 

	/* perspective projection. intrinsic parameters + frustrum */
	gluPerspective(45.,(GLfloat)640/(GLfloat)480,1.0,50000.0);
	/* extrinsic parameters */
	if (actualCameraView == 0) // user camera view
	  gluLookAt(virtualcam2.position.X,virtualcam2.position.Y,virtualcam2.position.Z,
	          virtualcam2.foa.X,virtualcam2.foa.Y,virtualcam2.foa.Z,
	          0.,0.,1.);
	else if (actualCameraView == 1) // robotics lab camera 1
		gluLookAt(roboticLabCam0.position.X,roboticLabCam0.position.Y,roboticLabCam0.position.Z,roboticLabCam0.foa.X,roboticLabCam0.foa.Y,roboticLabCam0.foa.Z,0.,0.,1.);
	else if (actualCameraView == 2) // robotics lab camera 2
		gluLookAt(roboticLabCam1.position.X,roboticLabCam1.position.Y,roboticLabCam1.position.Z,roboticLabCam1.foa.X,roboticLabCam1.foa.Y,roboticLabCam1.foa.Z,0.,0.,1.);
	else if (actualCameraView == 3) // robotics lab camera 3
		gluLookAt(roboticLabCam2.position.X,roboticLabCam2.position.Y,roboticLabCam2.position.Z,roboticLabCam2.foa.X,roboticLabCam2.foa.Y,roboticLabCam2.foa.Z,0.,0.,1.);
	else if (actualCameraView == 4) // robotics lab camera 4
		gluLookAt(roboticLabCam3.position.X,roboticLabCam3.position.Y,roboticLabCam3.position.Z,roboticLabCam3.foa.X,roboticLabCam3.foa.Y,roboticLabCam3.foa.Z,0.,0.,1.);
	else if (actualCameraView == 5) // robotics lab ceil camera
		gluLookAt(ceilLabCam.position.X,ceilLabCam.position.Y,ceilLabCam.position.Z,ceilLabCam.foa.X,ceilLabCam.foa.Y,ceilLabCam.foa.Z,0.,0.,1.);
}

inline int loadWorldLines (FILE *myfile) {
  #define limit 256		
  char word1[limit],word2[limit],word3[limit],word4[limit],word5[limit];
  char word6[limit],word7[limit],word8[limit];
  char word[limit];
  int i=0;
  char buffer_file[limit];   

  buffer_file[0]=fgetc(myfile);
  if (feof(myfile)) return EOF;
  if (buffer_file[0]==(char)255) return EOF; 
  if (buffer_file[0]=='#') {while(fgetc(myfile)!='\n'); return 0;}
  if (buffer_file[0]==' ') {while(buffer_file[0]==' ') buffer_file[0]=fgetc(myfile);}
  if (buffer_file[0]=='\t') {while(buffer_file[0]=='\t') buffer_file[0]=fgetc(myfile);}

  /* Captures a line and then we will process it with sscanf checking that the last character is \n. We can't doit with fscanf because this function does not difference \n from blank space. */
  while((buffer_file[i]!='\n') && 
	(buffer_file[i] != (char)255) &&  
	(i<limit-1) ) {
    buffer_file[++i]=fgetc(myfile);
  }
  
  if (i >= limit-1) { 
    if (DEBUG) printf("%s...\n", buffer_file); 
    if (DEBUG) printf ("Line too long in config file!\n"); 
    exit(-1);
  }
  buffer_file[++i]='\0';


  if (sscanf(buffer_file,"%s",word)!=1) return 0; 
  /* return EOF; empty line*/
  else {
     if(strcmp(word,"worldline")==0){
				sscanf(buffer_file,"%s %s %s %s %s %s %s %s %s",word,word1,word2,word3,word4,word5,word6,word7,word8);
				myfloor[myfloor_lines*2+0].X=(float)atof(word1); myfloor[myfloor_lines*2+0].Y=(float)atof(word2); myfloor[myfloor_lines*2+0].Z=(float)atof(word3); myfloor[myfloor_lines*2+0].H=(float)atof(word4);
				myfloor[myfloor_lines*2+1].X=(float)atof(word5); myfloor[myfloor_lines*2+1].Y=(float)atof(word6); myfloor[myfloor_lines*2+1].Z=(float)atof(word7); myfloor[myfloor_lines*2+1].H=(float)atof(word8);
				myfloor_lines++;
     }
  }
  return 1;
}

// calculates intersection and checks for parallel lines.  
// also checks that the intersection point is actually on  
// the line segment p1-p2  
// return values:  0 -> lines are parallel
//                -1 -> lines aren't parallel and they don't intersect
//                 1 -> lines aren't parallel and they intersect in pt point
inline int findIntersection(HPoint2D p1, HPoint2D p2, HPoint2D p3, HPoint2D p4, HPoint2D* pt) {  
	float xD1, yD1, xD2, yD2, xD3, yD3;
	float dot, deg, len1, len2;
	float segmentLen1, segmentLen2;
	float ua, ub, div;

	// calculate differences  
	xD1 = p2.x-p1.x;  
	xD2 = p4.x-p3.x;  
	yD1 = p2.y-p1.y;  
	yD2 = p4.y-p3.y;  
	xD3 = p1.x-p3.x;  
	yD3 = p1.y-p3.y;    

	// calculate the lengths of the two lines  
	len1 = sqrt(xD1*xD1+yD1*yD1);  
	len2 = sqrt(xD2*xD2+yD2*yD2);  

	// calculate angle between the two lines.  
	dot = (xD1*xD2+yD1*yD2); // dot product  
	deg = dot/(len1*len2);  

	// if abs(angle)==1 then the lines are parallell,  
	// so no intersection is possible  
	if (abs(deg)==1) return 0;  

	// find intersection Pt between two lines  
	div=yD2*xD1-xD2*yD1;  
	ua=(xD2*yD3-yD2*xD3)/div;  
	ub=(xD1*yD3-yD1*xD3)/div;  
	pt->x=p1.x+ua*xD1;  
	pt->y=p1.y+ua*yD1;  

	// calculate the combined length of the two segments  
	// between Pt-p1 and Pt-p2  
	xD1=pt->x-p1.x;  
	xD2=pt->x-p2.x;  
	yD1=pt->y-p1.y;  
	yD2=pt->y-p2.y;  
	segmentLen1=sqrt(xD1*xD1+yD1*yD1)+sqrt(xD2*xD2+yD2*yD2);  

	// calculate the combined length of the two segments  
	// between Pt-p3 and Pt-p4  
	xD1=pt->x-p3.x;  
	xD2=pt->x-p4.x;  
	yD1=pt->y-p3.y;  
	yD2=pt->y-p4.y;  
	segmentLen2=sqrt(xD1*xD1+yD1*yD1)+sqrt(xD2*xD2+yD2*yD2);  

	// if the lengths of both sets of segments are the same as
	// the lenghts of the two lines the point is actually
	// on the line segment.

	// if the point isn’t on the line, return -1
	if(abs(len1-segmentLen1)>0.01 || abs(len2-segmentLen2)>0.01)
	return -1;

	// return the intersection is valid
	return 1;
}  

inline int areNearlyParallelLines (HPoint3Dinfo *Line1Start, HPoint3Dinfo *Line1End, HPoint3Dinfo *Line2Start, HPoint3Dinfo *Line2End) {
	// Let me take m = (y2-y1)/(x2-x1)
	int areParallel = 0;
	float m1, m2;
	float start1, start2, end1, end2;

	if (Line1End->position.X > Line1Start->position.X) {
		start1 = Line1Start->position.X;
		end1 = Line1End->position.X;
	} else {
		end1 = Line1Start->position.X;
		start1 = Line1End->position.X;
	}

	if (Line2End->position.X > Line2Start->position.X) {
		start2 = Line2Start->position.X;
		end2 = Line2End->position.X;
	} else {
		end2 = Line2Start->position.X;
		start2 = Line2End->position.X;
	}

	if ((Line1End->position.X - Line1Start->position.X) != 0.)
		m1 = ((Line1End->position.Y - Line1Start->position.Y)/(Line1End->position.X - Line1Start->position.X));
	else
		m1 = 999999.9;

	if ((Line2End->position.X - Line2Start->position.X) != 0.)
		m2 = ((Line2End->position.Y - Line2Start->position.Y)/(Line2End->position.X - Line2Start->position.X));
	else
		m2 = 999999.9;

	//if (DEBUG) printf ("[%0.2f, %0.2f] [%0.2f, %0.2f] m1 = %0.2f, m2 = %0.2f\n", Line1Start->X, Line1Start->Y, Line2Start->X, Line2Start->Y, m1, m2);

	//if (m1 == m2) {
	if ((!(m1 > 990000.9)) && (!(m2 > 990000.9))) {
		if ((((m1 > 0) && (m2 > 0)) || ((m1 < 0) && (m2 < 0))) && (abs (m1-m2) < 1.)) { // We consider parallel lines
		// both positive slopes or negative ones... and a bit threshold
			areParallel = 1;
		}
	}
	return areParallel;
}

inline void getLineEquation(int xini, int yini, int xfin, int yfin, float * A, float * B, float * C) {
	/*Line equation, producto vectorial de los puntos*/
	*A = (float)yini - (float)yfin; /*y1*z2 - z1*y2*/
	*B = (float)xfin - (float)xini; /*z1*x2 - x1*z2*/
	*C = (float)xini*(float)yfin - (float)yini*(float)xfin; /*x1*y2 - y1*x2*/
}

inline void drawGroundLines () {
  glLineWidth(1.f);
	struct CacheList *p;
	HPoint2D p1, p2, gooda, goodb;
	CvPoint pt1, pt2;
	int doBackupMemory = 0;

	if (actualInstant - timeToSaveImage > TIME_TO_SAVE_IMAGE) {
		doBackupMemory = 1;
		timeToSaveImage = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	}

	if (doBackupMemory) {
		if (memoryImage != NULL)
			cvReleaseImage(&memoryImage);
		memoryImage = cvCreateImage(cvSize(640, 480), IPL_DEPTH_8U, 3);
		cvSet(memoryImage,CV_RGB(153,204,255),NULL);
	}

	p = cache;

	while (p != NULL) {
		if (p->segment->isValid == 1) {
			if (doBackupMemory) { // cada X tiempo vamos volcando el contenido de nuestra memoria a un buffer para componer una imagen estática
				p->segment->start.position.H = 1.;
				p->segment->end.position.H = 1.;

			  project(p->segment->start.position, &p1, ceilLabCam); // pasamos de 3D a 2D
			  project(p->segment->end.position, &p2, ceilLabCam);

				if(displayline(p1,p2,&gooda,&goodb,ceilLabCam)==1) { // es proyectable, está en el campo de visión
					//Pasamos de coordenadas opticas a pixels
					pt1.x=(int)gooda.y;
					pt1.y=480-1-(int)gooda.x;

					pt2.x=(int)goodb.y;
					pt2.y=480-1-(int)goodb.x;

					if (p->segment->idColor == 0)
						cvLine(memoryImage, pt1, pt2, CV_RGB(0,0,255), 1, 8, 0);
					else if (p->segment->idColor == 1)
						cvLine(memoryImage, pt1, pt2, CV_RGB(0,128,0), 1, 8, 0);
					else
						cvLine(memoryImage, pt1, pt2, CV_RGB(255,0,0), 1, 8, 0);
				}
			}

			if (p->segment->idColor == 0)
				glColor3f(0.0f, 0.0f, 1.0f);
			else if (p->segment->idColor == 1)
				glColor3f(0.0f, 0.5f, 0.0f);
			else
				glColor3f(1.0f, 0.0f, 0.0f);

			glBegin (GL_LINES);
				glVertex3f (p->segment->start.position.X, p->segment->start.position.Y, p->segment->start.position.Z);
				glVertex3f (p->segment->end.position.X, p->segment->end.position.Y, p->segment->end.position.Z);
			glEnd ();
		}
		p = p->next;
	}

	if (doBackupMemory) {
		strcpy (imageCharNumber, "\0"); // inicializamos
		strcpy (imageName, "memoryImage\0");
		sprintf (imageCharNumber, "%d\0", imageNumber);
		strcat (imageName, imageCharNumber);
		strcat (imageName, imageFormat);
		cvSaveImage (imageName, memoryImage);

		imageNumber ++;
		if (imageNumber == 999) imageNumber = 0;
	}
}

inline void drawForces () {
	fuerzaResultante.x=myencoders[0]+xRes;
	fuerzaResultante.y=myencoders[1]+yRes;
	repulsiva.x=myencoders[0]+xRep;
	repulsiva.y=myencoders[1]+yRep;
	atractiva.x=myencoders[0]+xAtr;
	atractiva.y=myencoders[1]+yAtr;

  glLoadIdentity ();

  glLineWidth(2.f);

  glColor3f(1., 1., 0.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 200.000000);
		v3f(miObjetivo.x, miObjetivo.y, 200.000000);
  glEnd();

  glColor3f(0., 0., 1.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 200.000000);
		v3f(fuerzaResultante.x, fuerzaResultante.y, 200.000000);
  glEnd();

  glColor3f(1., 0., 0.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 200.000000);
		v3f(repulsiva.x, repulsiva.y, 200.000000);
  glEnd();

  glColor3f(0., 1., 0.);
  glBegin(GL_LINES);
		v3f(myencoders[0], myencoders[1], 200.000000);
		v3f(atractiva.x, atractiva.y, 200.000000);
  glEnd();
}

inline void drawSecWindow () {
  glLoadIdentity ();

  glLineWidth(1.f);
  glColor3f(1., 0., 1.);

	// Borde exterior de la ventana de seguridad
  glBegin(GL_LINES);
		v3f(windowA.x, windowA.y, 100.000000);
		v3f(windowB.x, windowB.y, 100.000000);
  glEnd();

  glBegin(GL_LINES);
		v3f(windowB.x, windowB.y, 100.000000);
		v3f(windowJ.x, windowJ.y, 100.000000);
  glEnd();

  glBegin(GL_LINES);
		v3f(windowJ.x, windowJ.y, 100.000000);
		v3f(windowG.x, windowG.y, 100.000000);
  glEnd();

  glBegin(GL_LINES);
		v3f(windowG.x, windowG.y, 100.000000);
		v3f(windowA.x, windowA.y, 100.000000);
  glEnd();

	// Cuadrado interior
  glBegin(GL_LINES);
		v3f(windowC.x, windowC.y, 100.000000);
		v3f(windowF.x, windowF.y, 100.000000);
  glEnd();

  glBegin(GL_LINES);
		v3f(windowE.x, windowE.y, 100.000000);
		v3f(windowI.x, windowI.y, 100.000000);
  glEnd();

  glBegin(GL_LINES);
		v3f(windowD.x, windowD.y, 100.000000);
		v3f(windowH.x, windowH.y, 100.000000);
  glEnd();
}

inline void DrawCircle(float cx, float cy, float r, int num_segments) { 
	int ii;
	float x, y, theta;

	glBegin(GL_LINE_LOOP); 
	for(ii = 0; ii < num_segments; ii++) { 
		theta = 2.0f * 3.1415926f * ((float)ii) / ((float)num_segments);//get the current angle 

		x = r * cosf(theta);//calculate the x component 
		y = r * sinf(theta);//calculate the y component 

		glVertex3f((x + cx), (y + cy), 0); // output vertex 

	} 
	glEnd(); 
}

inline void drawParallelogram (Parallelogram3D parallelogram) {
	glShadeModel(GL_FLAT);
	glColor3f(1.0f, 0.0f, 0.0f);

	glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
		glVertex3f (parallelogram.p2.position.X, parallelogram.p2.position.Y, parallelogram.p2.position.Z);
		glVertex3f (parallelogram.p1.position.X, parallelogram.p1.position.Y, parallelogram.p1.position.Z);
		glVertex3f (parallelogram.p3.position.X, parallelogram.p3.position.Y, parallelogram.p3.position.Z);
		glVertex3f (parallelogram.p4.position.X, parallelogram.p4.position.Y, parallelogram.p4.position.Z);
	glEnd ();
}

inline void drawFace (Face3D face) {
	glColor3f(0.0f, 0.0f, 1.0f);
  glLineWidth(2.f);

	DrawCircle (face.center.position.X, face.center.position.Y, 70., 15);
}

inline void drawArrow (Arrow3D arrow) {
	float ang, ang1, ang2, dist, dist2, angarrow;

	dist = 210.; // longitud línea base
	dist2 = 100.; // longitud líneas de aspas
	angarrow = 45.*PI/180.; // ángulo de las aspas respecto a la base

	ang1 = -atan((arrow.end.Y - arrow.start.Y)/(arrow.end.X - arrow.start.X)); // ángulo de la flechita en el suelo (eje (X,Y))
	ang2 = -atan((arrow.start.Y - arrow.end.Y)/(arrow.start.X - arrow.end.X)); // ángulo de la flechita en el suelo (eje (X,Y))

	if (ang1 < ang2) ang = ang1;
	else ang = ang2;

	glColor3f(0.5f, 1.0f, 0.0f);
  glLineWidth(3.f);

	glBegin(GL_LINES);
		glVertex3f(arrow.start.X, arrow.start.Y, 0.); // línea base, punto start
		glVertex3f(arrow.end.X, arrow.end.Y, 0.); // y punto end
	glEnd();

	DrawCircle (arrow.end.X, arrow.end.Y, 15., 5);

	/*
	glBegin(GL_LINES);
		glVertex3f(arrow.end.X, arrow.end.Y, 0.); // línea aspa1, punto end
		glVertex3f((arrow.end.X + (dist2 * (-cos(ang - angarrow)))), (arrow.end.Y + (dist2 * (-sin(ang - angarrow)))), 0.); // línea aspa1, punto start
	glEnd();
	glBegin(GL_LINES);
		glVertex3f(arrow.end.X, arrow.end.Y, 0.); // línea aspa2, punto end
		glVertex3f((arrow.end.X + (dist2 * (-cos(ang + angarrow)))), (arrow.end.Y + (dist2 * (-sin(ang + angarrow)))), 0.); // línea aspa2, punto start
	glEnd();
	*/
}

inline void drawMemory () {
	struct elementStruct *r;

	r = myElements;

	while (r != NULL) {
		if (r->type == 1) {// dibujamos un paralelogramo
			//if (DEBUG) printf ("Drawing a parallelogram...\n");
			drawParallelogram (r->parallelogram);
		} else if (r->type == 2) {// dibujamos una cara
			//if (DEBUG) printf ("Drawing a face...\n");
			drawFace (r->face);
		} else if (r->type == 3) {// dibujamos una flecha
			//if (DEBUG) printf ("Drawing an arrow...\n");
			drawArrow (r->arrow);
		}
		r = r->next;
	}
}

inline static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;
	float dxPioneer, dyPioneer, dzPioneer, longiPioneer, latiPioneer, rPioneer;

	pthread_mutex_lock(&gl_mutex);

//whatsTheTimeBefore = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	initWorldCanvasSettings ();

  /** Robot Frame of Reference **/
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  if (myencoders!=NULL){
     mypioneer.posx=myencoders[0];
     mypioneer.posy=myencoders[1];
     mypioneer.posz=0.;
     mypioneer.foax=myencoders[0];
     mypioneer.foay=myencoders[1];
     mypioneer.foaz=10.;
     mypioneer.roll=myencoders[2]*RADTODEG;
  }
  else{
     mypioneer.posx=0.;
     mypioneer.posy=0.;
     mypioneer.posz=0.;
     mypioneer.foax=0.;
     mypioneer.foay=0.;
     mypioneer.foaz=10.;
     mypioneer.roll=0.;
  }
  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
  dxPioneer = (mypioneer.foax-mypioneer.posx);
  dyPioneer = (mypioneer.foay-mypioneer.posy);
  dzPioneer = (mypioneer.foaz-mypioneer.posz);
  longiPioneer = (float)atan2(dyPioneer,dxPioneer)*360./(2.*PI);
  glRotatef (longiPioneer,0.,0.,1.);
  rPioneer = sqrt(dxPioneer*dxPioneer+dyPioneer*dyPioneer+dzPioneer*dzPioneer);
  if (rPioneer<0.00001) latiPioneer=0.;
  else latiPioneer=acos(dzPioneer/rPioneer)*360./(2.*PI);
  glRotatef(latiPioneer,0.,1.,0.);
  glRotatef(mypioneer.roll,0.,0.,1.);

	glEnable (GL_LIGHTING); // LUCES Y.... ACCION!!
	glPushMatrix();
	glTranslatef(1.,0.,0.);
	// the body it is not centered. With this translation we center it
	glScalef (100., 100., 100.);
	loadModel();
	glPopMatrix();
	glDisable (GL_LIGHTING); // FUERA LUCES, VOLVEMOS A LA VIDA REAL

	glLoadIdentity ();
	//drawMyLines ();
	drawFloor ();
	drawRobocupLines ();
	drawGroundLines ();
	drawMemory ();
	drawSecWindow ();
	drawForces ();

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

//whatsTheTimeAfter = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
//printf ("Drawing Scene = %f\n", whatsTheTimeAfter-whatsTheTimeBefore);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}


inline void visualSonar_guidisplay()
{
	pthread_mutex_lock(&main_mutex);
	gdk_threads_enter();
	GtkImage *filImg = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));
	GtkToggleButton *myTempButton;

	myTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "showPredictionsButton");
	if ((gtk_toggle_button_get_active (myTempButton)) == TRUE)
		showPredictions = TRUE;
	else
		showPredictions = FALSE;

	myTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "showBordersButton");
	if ((gtk_toggle_button_get_active (myTempButton)) == TRUE)
		showBorders = TRUE;
	else
		showBorders = FALSE;

	myTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "showRefutedButton");
	if ((gtk_toggle_button_get_active (myTempButton)) == TRUE)
		showRefuted = TRUE;
	else
		showRefuted = FALSE;

	myTempButton = (GtkToggleButton *)glade_xml_get_widget(xml, "processImageButton");
	if ((gtk_toggle_button_get_active (myTempButton)) == TRUE)
		processImageButton = TRUE;
	else
		processImageButton = FALSE;

	sliderPANTILT_BASE_HEIGHT = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"));
	sliderISIGHT_OPTICAL_CENTER = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderOPTICAL_CENTER"));
	sliderTILT_HEIGHT = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderTILT_HEIGHT"));
	sliderCAMERA_TILT_HEIGHT = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"));
	sliderPANTILT_BASE_X = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"));
	sliderThreshold = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderThreshold"));
	//panAngle = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderPAN_ANGLE"));
	//tiltAngle = gtk_range_get_value((GtkRange *)glade_xml_get_widget(xml, "sliderTILT_ANGLE"));

	gtk_widget_queue_draw(GTK_WIDGET(filImg));
	expose_event(visualSonarCanvas, NULL, NULL);

	gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();
	pthread_mutex_unlock(&main_mutex);
}

inline void visualSonar_hide() {
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(visualSonar_guidisplay);
	all[visualSonar_id].guistate=off;
}

/*Callback of window closed*/
inline void on_delete_window (GtkWidget *widget,GdkEvent *event,gpointer user_data)
{
   gdk_threads_leave();
   visualSonar_hide();
   gdk_threads_enter();
}

inline void visualSonar_show()
{
	int loadedgui=0;
//	static pthread_mutex_t visualSonar_gui_mutex;
	GtkToggleButton *camera1Button, *camera2Button, *camera3Button, *camera4Button, *ceilCameraButton, *userCameraButton;
	GtkWidget *widget1;

//	pthread_mutex_lock(&visualSonar_gui_mutex);
	if (!loadedgui){
		loadglade ld_fn;
		loadedgui=1;
//		pthread_mutex_unlock(&visualSonar_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

		xml = ld_fn ("visualSonar.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical visualSonar on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));
		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");

			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
		}

    visualSonarCanvas = glade_xml_get_widget(xml, "visualSonarCanvas");

    gtk_widget_unrealize(visualSonarCanvas);
    /* Set OpenGL-capability to the widget. */
    if (gtk_widget_set_gl_capability (visualSonarCanvas,glconfig,NULL,TRUE,GDK_GL_RGBA_TYPE)==FALSE)
    {
        if (DEBUG) printf ("No Gl capability\n");
        jdeshutdown(1);
    }
    gtk_widget_realize(visualSonarCanvas);

		gtk_widget_set_child_visible (GTK_WIDGET(visualSonarCanvas), TRUE);

		gtk_widget_add_events ( visualSonarCanvas,
		                        GDK_BUTTON1_MOTION_MASK    |
		                        GDK_BUTTON2_MOTION_MASK    |
		                        GDK_BUTTON3_MOTION_MASK    |
		                        GDK_BUTTON_PRESS_MASK      |
		                        GDK_BUTTON_RELEASE_MASK    |
		                        GDK_VISIBILITY_NOTIFY_MASK);	

		widget1=(GtkWidget *)glade_xml_get_widget(xml, "visualSonarCanvas");

		g_signal_connect (G_OBJECT (widget1), "button_press_event", G_CALLBACK (button_press_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "motion_notify_event", G_CALLBACK (motion_notify_event), NULL);
		g_signal_connect (G_OBJECT (widget1), "scroll-event", G_CALLBACK (scroll_event), visualSonarCanvas);

		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		//glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		camera1Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera1Button");
		g_signal_connect (G_OBJECT (camera1Button), "pressed", G_CALLBACK (camera1Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera1Button), "released", G_CALLBACK (camera1Button_released), NULL);

		camera2Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera2Button");
		g_signal_connect (G_OBJECT (camera2Button), "pressed", G_CALLBACK (camera2Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera2Button), "released", G_CALLBACK (camera2Button_released), NULL);

		camera3Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera3Button");
		g_signal_connect (G_OBJECT (camera3Button), "pressed", G_CALLBACK (camera3Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera3Button), "released", G_CALLBACK (camera3Button_released), NULL);

		camera4Button = (GtkToggleButton *)glade_xml_get_widget(xml, "camera4Button");
		g_signal_connect (G_OBJECT (camera4Button), "pressed", G_CALLBACK (camera4Button_pressed), NULL);
		g_signal_connect (G_OBJECT (camera4Button), "released", G_CALLBACK (camera4Button_released), NULL);

		ceilCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "ceilCameraButton");
		g_signal_connect (G_OBJECT (ceilCameraButton), "pressed", G_CALLBACK (ceilCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (ceilCameraButton), "released", G_CALLBACK (ceilCameraButton_released), NULL);

		userCameraButton = (GtkToggleButton *)glade_xml_get_widget(xml, "userCameraButton");
		g_signal_connect (G_OBJECT (userCameraButton), "pressed", G_CALLBACK (userCameraButton_pressed), NULL);
		g_signal_connect (G_OBJECT (userCameraButton), "released", G_CALLBACK (userCameraButton_released), NULL);

		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderPAN_ANGLE"),panAngle);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderTILT_ANGLE"),tiltAngle);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderPANTILT_BASE_HEIGHT"),sliderPANTILT_BASE_HEIGHT);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderOPTICAL_CENTER"),sliderISIGHT_OPTICAL_CENTER);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderTILT_HEIGHT"),sliderTILT_HEIGHT);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderCAMERA_TILT_HEIGHT"),sliderCAMERA_TILT_HEIGHT);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderPANTILT_BASE_X"),sliderPANTILT_BASE_X);
		gtk_range_set_value((GtkRange *)glade_xml_get_widget(xml, "sliderThreshold"),sliderThreshold);

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
//		pthread_mutex_unlock(&visualSonar_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}

	gdk_threads_enter();
	// Initialization of the image buffers
	myregister_displaycallback(visualSonar_guidisplay);

	GdkPixbuf *filteredImageBuf;

	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));

	filteredImageBuf = gdk_pixbuf_new_from_data((const guchar *)filteredImageRGB->image,
					GDK_COLORSPACE_RGB,0,8,
					filteredImageRGB->width,filteredImageRGB->height,
					filteredImageRGB->width*3,NULL,NULL);

	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);

	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	gdk_threads_leave();
}
